package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.ConsoleUnlockSchematic;
import net.tardis.mod.schematics.ExteriorUnlockSchematic;
import net.tardis.mod.schematics.InteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.types.ConsoleSchematicType;
import net.tardis.mod.schematics.types.ExteriorSchematicType;
import net.tardis.mod.schematics.types.InteriorSchematicType;

public class TardisSchematicGen implements IDataProvider {

    private DataGenerator generator;

    public TardisSchematicGen(DataGenerator generator){
        this.generator = generator;
    }

    @Override
    public void act(DirectoryCache cache) throws IOException {

        //Generate interiors
    	if (ConsoleRoom.getRegistry().isEmpty())
            ConsoleRoom.registerCoreConsoleRooms();

        for(ConsoleRoom room : ConsoleRoom.getRegistry().values()){
            this.generateInterior(cache, room);
        }

        //Exteriors
        for(AbstractExterior ext : ExteriorRegistry.EXTERIOR_REGISTRY.get().getValues()){
            this.generateExterior(cache, ext);
        }

        //Consoles
        for(Console console : ConsoleRegistry.CONSOLE_REGISTRY.get().getValues()){
            this.generateConsole(cache, console);
        }

    }

    public void generateInterior(DirectoryCache cache, ConsoleRoom interior) throws IOException{
        InteriorUnlockSchematic schematic = new InteriorUnlockSchematic();
        schematic.setConsoleRoom(interior.getRegistryName());
        schematic.setTranslation(interior.getDisplayName().getString());
        schematic.setId(interior.getRegistryName());
        this.generateSchematic(cache, schematic);
    }

    public void generateExterior(DirectoryCache cache, AbstractExterior exterior) throws IOException {
        ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic(SchematicTypes.EXTERIOR.get());
        schematic.setExterior(exterior.getRegistryName());
        schematic.setId(exterior.getRegistryName());
        schematic.setTranslation(exterior.getTranslationKey());
        this.generateSchematic(cache, schematic);
    }

    public void generateConsole(DirectoryCache cache, Console console) throws IOException{
        ConsoleUnlockSchematic consoleSchematic = new ConsoleUnlockSchematic(SchematicTypes.CONSOLE.get());
        consoleSchematic.setConsole(console.getRegistryName());
        consoleSchematic.setId(console.getRegistryName());
        consoleSchematic.setTranslation(console.getDisplayName().getString());
        this.generateSchematic(cache, consoleSchematic);
    }

    public void generateSchematic(DirectoryCache cache, Schematic s) throws IOException{
        IDataProvider.save(DataGen.GSON, cache, createSchematic(s), getPath(this.generator.getOutputFolder(), s));
    }

    public JsonObject createSchematic(Schematic schematic){
       return schematic.getType().serialize(schematic);
    }

    public static Path getPath(Path base, Schematic schematic){

        if(schematic.getType() instanceof ConsoleSchematicType){
            return getPath(base, "consoles", schematic.getId());
        }
        if(schematic.getType() instanceof ExteriorSchematicType){
            return getPath(base, "exteriors", schematic.getId());
        }
        if(schematic.getType() instanceof InteriorSchematicType)
            return getPath(base, "interiors", schematic.getId());
        return base;
    }

    public static Path getPath(Path base, String folder, ResourceLocation key){
        return base.resolve("data/" + key.getNamespace() + "/unlock_schematics/" + folder + "/" + key.getPath() + ".json");
    }

    @Override
    public String getName() {
        return "TARDIS Schematic generator";
    }
}
