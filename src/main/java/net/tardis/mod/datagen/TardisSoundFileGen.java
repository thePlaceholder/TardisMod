package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.Util;
import net.tardis.mod.Tardis;
import net.tardis.mod.datagen.objects.SoundBuilder;
import net.tardis.mod.datagen.objects.SoundEventBuilder;
import net.tardis.mod.sounds.TSounds;
/** Created by 50ap5ud5 on 29 March 2021
 * <br> A Data generator that allows us to automatically add new objects to the sounds.json without needing manual work*/
public class TardisSoundFileGen implements IDataProvider {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    private JsonObject root = new JsonObject();
    
    public TardisSoundFileGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {
        
        final Path path = this.generator.getOutputFolder();
        final Path outputPath = getPath(path, Tardis.MODID);
        
        this.addSound(TSounds.ALARM_LOW.get(), "tardis/alarm_low");
        this.addSound(TSounds.AMBIENT_CREAKS.get(), SoundCategory.AMBIENT, "ambient/creaks_loop");
        this.addSound(TSounds.BESSIE_DRIVE.get(), "entity/transport/bessie_drive");
        this.addSound(TSounds.BESSIE_HORN.get(), "entity/transport/bessie_horn");
        this.addSound(TSounds.CANT_START.get(), "tardis/tardis_cant_start");
        this.addSound(TSounds.CAR_LOCK.get(), "tardis/car_lock");
        this.addSound(TSounds.COMMUNICATOR_BEEP.get(), "tardis/console/communicator_beep");
        this.addSound(TSounds.COMMUNICATOR_PHONE_PICKUP.get(), "tardis/console/communicator_phone_pickup");
        this.addSound(TSounds.COMMUNICATOR_RING.get(), "tardis/console/communicator_ring");
        this.addSound(TSounds.COMMUNICATOR_STEAM.get(), "tardis/console/communicator_steam");
        this.addSound(TSounds.DALEK_ARM.get(), "entity/dalek/dalek_arm");
        this.addSound(TSounds.DALEK_DEATH.get(), "entity/dalek/dalek_death");
        this.addSound(TSounds.DALEK_EXTERMINATE.get(), "entity/dalek/dalek_exterminate");
        this.addSound(TSounds.DALEK_FIRE.get(), "entity/dalek/dalek_laser");
        this.addSoundWithSubtitle(TSounds.DALEK_FIRE_EXTENDED.get(), getTranslationKey(TSounds.DALEK_FIRE.get()), "entity/dalek/dalek_laser_extended");
        this.addSound(TSounds.DALEK_HOVER.get(), "entity/dalek/dalek_hover");
        this.addSound(TSounds.DALEK_MOVES.get(), "entity/dalek/dalek_moves");
        this.addSound(TSounds.DALEK_SW_AIM.get(), "entity/dalek/special_weapon/dalek_sw_aim");
        this.addSound(TSounds.DALEK_SW_FIRE.get(), "entity/dalek/special_weapon/dalek_sw_fire");
        this.addSound(TSounds.DALEK_SW_HIT_EXPLODE.get(), "entity/dalek/special_weapon/dalek_sw_hit_explode");
        this.addSound(TSounds.DIMENSION.get(), "tardis/console/dimension_one", "tardis/console/dimension_two", "tardis/console/dimension_three");
        this.addSound(TSounds.DIRECTION.get(), "tardis/console/direction_one", "tardis/console/direction_two", "tardis/console/direction_three");
        this.addSound(TSounds.DOOR_CLOSE.get(), "tardis/door_close");
        this.addSound(TSounds.DOOR_KNOCK.get(), "tardis/door_knock");
        this.addSound(TSounds.DOOR_LOCK.get(), "tardis/door_lock");
        this.addSound(TSounds.DOOR_OPEN.get(), "tardis/door_open");
        this.addSound(TSounds.DOOR_UNLOCK.get(), "tardis/door_unlock");
        this.addSound(TSounds.ELECTRIC_ARC.get(), "ambient/electric_arc");
        this.addSound(TSounds.ELECTRIC_SPARK.get(), "ambient/electric_spark_a", "ambient/electric_spark_b", "ambient/electric_spark_c");
        this.addSound(TSounds.EYE_MONITOR_INTERACT.get(), "tardis/monitor/eye_monitor_interact");
        this.addSound(TSounds.GADGET_MENU_OPEN.get(), "item/gadget_menu_open");
        this.addSoundWithSubtitle(TSounds.GENERIC_ONE.get(), "subtitle.tardis.generic_control", "tardis/console/generic_one");
        this.addSoundWithSubtitle(TSounds.GENERIC_ONE.get(), "subtitle.tardis.generic_control", "tardis/console/generic_one");
        this.addSoundWithSubtitle(TSounds.GENERIC_TWO.get(), "subtitle.tardis.generic_control", "tardis/console/generic_two");
        this.addSoundWithSubtitle(TSounds.GENERIC_THREE.get(), "subtitle.tardis.generic_control", "tardis/console/generic_three");
        this.addSound(TSounds.HANDBRAKE_ENGAGE.get(), "tardis/console/handbrake_engage");
        this.addSound(TSounds.HANDBRAKE_RELEASE.get(), "tardis/console/handbrake_release");
        this.addSoundWithSubtitle(TSounds.JUNK_LAND.get(), getTranslationKey(TSounds.TARDIS_LAND.get()), "tardis/junk_land");
        this.addSoundWithSubtitle(TSounds.JUNK_TAKEOFF.get(), getTranslationKey(TSounds.TARDIS_TAKEOFF.get()), "tardis/junk_takeoff");
        this.addSoundWithSubtitle(TSounds.LAND_TV.get(), getTranslationKey(TSounds.TARDIS_LAND.get()), "tardis/tardis_land_tv");
        this.addSoundWithSubtitle(TSounds.LANDING_TYPE_DOWN.get(), "subtitle.tardis.control_landing_type", "tardis/console/landing_type_down");
        this.addSoundWithSubtitle(TSounds.LANDING_TYPE_UP.get(), "subtitle.tardis.control_landing_type", "tardis/console/landing_type_up");
        this.addSound(TSounds.LASER_GUN_FIRE.get(), "item/laser_gun_fire");
        this.addSoundWithSubtitle(TSounds.MASTER_LAND.get(), getTranslationKey(TSounds.TARDIS_LAND.get()), "tardis/master_land");
        this.addSoundWithSubtitle(TSounds.MASTER_TAKEOFF.get(), getTranslationKey(TSounds.TARDIS_TAKEOFF.get()), "tardis/master_takeoff");
        this.addSound(TSounds.PAPER_DROP.get(), "blocks/paper_drop");
        this.addSound(TSounds.POWER_DOWN.get(), "tardis/power_down");
        this.addSound(TSounds.RANDOMISER.get(), "tardis/console/randomiser");
        this.addSound(TSounds.REACHED_DESTINATION.get(), "tardis/reached_destination");
        this.addSound(TSounds.REFUEL_START.get(), "tardis/console/refuel_start");
        this.addSound(TSounds.REFUEL_STOP.get(), "tardis/console/refuel_stop");
        this.addSound(TSounds.REMOTE_ACCEPT.get(), "item/remote_accept");
        this.addSound(TSounds.ROTOR_END.get(), "tardis/rotor_end");
        this.addSound(TSounds.ROTOR_START.get(), "tardis/rotor_start");
        this.addSound(TSounds.ROTOR_TICK.get(), "tardis/rotor_tick");
        this.addSound(TSounds.SCREEN_BEEP_SINGLE.get(), "tardis/console/screen_beep_single");
        this.addSound(TSounds.SHIELD_HUM.get(), true, "shared/shield_hum");
        this.addSound(TSounds.SINGLE_CLOISTER.get(), "tardis/console/single_cloister");
        this.addSound(TSounds.SNAP.get(), "tardis/snap");
        this.addSound(TSounds.SONIC_BROKEN.get(), "sonic/sonic_broken");
        this.addSound(TSounds.SONIC_FAIL.get(), "sonic/sonic_fail");
        this.addSound(TSounds.SONIC_GENERIC.get(), "sonic/sonic_generic");
        this.addSound(TSounds.SONIC_MODE_CHANGE.get(), "sonic/sonic_mode_change");
        this.addSound(TSounds.SONIC_TUNING.get(), "sonic/sonic_tuning");
        this.addSound(TSounds.SPACE_AMBIENT_LOOP.get(), true, 0.1F, "ambient/space_ambient_loop");
        this.addSound(TSounds.STABILIZER_OFF.get(), "tardis/console/stabilizer_off");
        this.addSound(TSounds.STABILIZER_ON.get(), "tardis/console/stabilizer_on");
        this.addSound(TSounds.STEAM_HISS.get(), "ambient/steam_hiss_001", "ambient/steam_hiss_002", "ambient/steam_hiss_003", "ambient/steam_hiss_004");
        this.addSound(TSounds.STEAMPUNK_MONITOR_INTERACT.get(), "tardis/monitor/steampunk_monitor_interact");
        this.addSound(TSounds.SUBSYSTEMS_OFF.get(), "tardis/subsystems_off");
        this.addSound(TSounds.SUBSYSTEMS_ON.get(), "tardis/subsystems_on");
        this.addSoundWithSubtitle(TSounds.TAKEOFF_TV.get(), getTranslationKey(TSounds.TARDIS_TAKEOFF.get()), "tardis/tardis_takeoff_tv");
        this.addSound(TSounds.TARDIS_FIRST_ENTRANCE.get(), true, "tardis/tardis_first_entrance");
        this.addSound(TSounds.TARDIS_FLY_LOOP.get(), "tardis/tardis_fly_loop");
        this.addSoundWithSubtitle(TSounds.FLY_LOOP_TV.get(), getTranslationKey(TSounds.TARDIS_FLY_LOOP.get()), "tardis/fly_loop_tv");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_63.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_63");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_70.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_70");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_80.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_80");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_COPPER.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_copper");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_CORAL.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_coral");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_TOYOTA.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_toyota");
        this.addSoundWithSubtitle(TSounds.TARDIS_HUM_TV.get(), "subtitle.tardis.tardis_hum", true, "hums/tardis_hum_tv");
        this.addSound(TSounds.TARDIS_LAND.get(), "tardis/tardis_land");
        this.addSound(TSounds.TARDIS_LAND_NOTIFICATION.get(), "tardis/tardis_land_notification");
        this.addSound(TSounds.TARDIS_POWER_UP.get(), "tardis/tardis_power_up");
        this.addSound(TSounds.TARDIS_SHUT_DOWN.get(), "tardis/tardis_shut_down");
        this.addSound(TSounds.TARDIS_TAKEOFF.get(), "tardis/tardis_takeoff");
        this.addSound(TSounds.TELEPATHIC_CIRCUIT.get(), "tardis/console/telepathic_circuit");
        this.addSound(TSounds.THROTTLE.get(), "tardis/console/throttle");
        this.addSound(TSounds.VM_BUTTON.get(), "vm/vm_button");
        this.addSound(TSounds.VM_TELEPORT.get(), "vm/vm_teleport");
        this.addSoundWithSubtitle(TSounds.VM_TELEPORT_DEST.get(), getTranslationKey(TSounds.VM_TELEPORT.get()), "vm/vm_teleport_dest");
        this.addSound(TSounds.VORTEX_AMBIENT_LOOP.get(), true, 1F, "ambient/vortex_ambient_loop");
        this.addSound(TSounds.WATCH_MALFUNCTION.get(), "item/watch_malfunction");
        this.addSound(TSounds.WATCH_TICK.get(), "item/watch_tick");
        this.addSoundWithSubtitle(TSounds.WOOD_DOOR_CLOSE.get(), getTranslationKey(TSounds.DOOR_CLOSE.get()), "tardis/wood_door_close");
        this.addSoundWithSubtitle(TSounds.WOOD_DOOR_OPEN.get(), getTranslationKey(TSounds.DOOR_OPEN.get()),"tardis/wood_door_open");
        
        //Save the json object to a file after we have added everything
        IDataProvider.save(GSON, cache, this.root, outputPath);
        
    }
    
    /**
     * Adds a sound object with multiple sound files that have the default settings
     * @param soundEvent - the {@linkplain SoundEvent} to be added to the sounds.json file
     * @param soundPaths - Define which sound files can be played for this SoundEvent. 
     * <br> This is the path of where the ogg file is stored.
     * <br> The Tardis Modid is already set here
     * <br> E.g. If a sound is in assets/tardis/sounds/tardis/tardis_takeoff.ogg,
     * <br> the path is tardis/tardis_takeoff because the Tardis modid is already passed in
     */
    public void addSound(SoundEvent soundEvent, String... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withSubtitle();
        for (String resourcePath : soundPaths) {
            builder.withSound(new SoundBuilder(resourcePath));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    /**
     * Adds a sound object with a specific subtitle.
     * <br> Used when we want to reuse a specific subtitle
     * @param soundEvent
     * @param subtitle
     * @param soundPaths
     */
    public void addSoundWithSubtitle(SoundEvent soundEvent, String subtitle, String... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withSpecificSubtitle(subtitle);
        for (String resourcePath : soundPaths) {
            builder.withSound(new SoundBuilder(resourcePath));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    public void addSound(SoundEvent soundEvent, boolean stream, String... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withSubtitle();
        for (String resourcePath : soundPaths) {
            builder.withSound(stream ? new SoundBuilder(resourcePath).stream() : new SoundBuilder(resourcePath));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    public void addSound(SoundEvent soundEvent, boolean stream, float volume, String... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withSubtitle();
        for (String resourcePath : soundPaths) {
            builder.withSound(stream ? new SoundBuilder(resourcePath).stream().volume(volume) : new SoundBuilder(resourcePath).volume(volume));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    /**
     * Add a sound object with streaming enabled and specific subtitle
     * @param soundEvent
     * @param subtitle
     * @param stream
     * @param soundPaths
     */
    public void addSoundWithSubtitle(SoundEvent soundEvent, String subtitle, boolean stream, String... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withSpecificSubtitle(subtitle);
        for (String resourcePath : soundPaths) {
        	builder.withSound(stream ? new SoundBuilder(resourcePath).stream() : new SoundBuilder(resourcePath));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    /**
     * Adds a sound object with multiple sound files that have the default settings
     * @param soundEvent - the {@linkplain SoundEvent} to be added to the sounds.json file
     * @param category - the {@linkplain SoundCategory}
     * @param soundPaths - Define which sound files can be played for this SoundEvent. 
     * <br> This is the path of where the ogg file is stored.
     * <br> The Tardis Modid is already set here
     * <br> E.g. If a sound is in assets/tardis/sounds/tardis/tardis_takeoff.ogg,
     * <br> the path is tardis/tardis_takeoff because the Tardis modid is already passed in
     */
    public void addSound(SoundEvent soundEvent, SoundCategory category, String... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withCategory(category).withSubtitle();
        for (String resourcePath : soundPaths) {
            builder.withSound(new SoundBuilder(resourcePath));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    /**
     * Adds a sound object with multiple sound files that have the default settings
     * @param soundEvent - the {@linkplain SoundEvent} to be added to the sounds.json file
     * @param category - the {@linkplain SoundCategory}
     * @param soundPaths - Define which sound files can be played for this SoundEvent. 
     * <br> This is the path of where the ogg file is stored.
     * <br> E.g. If a sound is in assets/tardis/sounds/tardis/tardis_takeoff.ogg,
     * <br> the path is tardis:tardis/tardis_takeoff
     */
    public void addSound(SoundEvent soundEvent, SoundCategory category, ResourceLocation... soundPaths) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withCategory(category).withSubtitle();
        for (ResourceLocation resourcePath : soundPaths) {
            builder.withSound(new SoundBuilder(resourcePath));
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    /**
     * Adds a sound object with multiple sound objects that have can have variable settings.
     * @param soundEvent - the {@linkplain SoundEvent} to be added to the sounds.json file
     * @param category - the {@linkplain SoundCategory}
     * @param soundBuilders - An array of {@linkplain SoundBuilder}(s) to add to this sound object
     */
    public void addSound(SoundEvent soundEvent, SoundCategory category, SoundBuilder... soundBuilders) {
        SoundEventBuilder builder = new SoundEventBuilder(soundEvent).withCategory(category).withSubtitle();
        for (SoundBuilder soundBuilder : soundBuilders) {
            builder.withSound(soundBuilder);
        }
        this.root.add(getSoundName(soundEvent), builder.toJson());
    }
    
    
    public static Path getPath(Path path, String modid) {
        return path.resolve("assets/" + modid + "/sounds" + ".json");
    }
    
    public String getSoundName(SoundEvent sound) {
        return sound.getRegistryName().getPath();
    }
    
    public String getTranslationKey(SoundEvent sound) {
    	String subtitleTranslationKey = "";
		if (subtitleTranslationKey.isEmpty() || subtitleTranslationKey == null) {
			subtitleTranslationKey = Util.makeTranslationKey("subtitle", sound.getRegistryName());
		}
		return subtitleTranslationKey;
	}

    @Override
    public String getName() {
        return "TARDIS Sound File Generator";
    }
}
