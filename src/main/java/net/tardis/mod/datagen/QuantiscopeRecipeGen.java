package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.recipe.WeldRecipe;
/**
 * Generate recipes and repair recipes for Quantiscope
 */
public class QuantiscopeRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public QuantiscopeRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        
        //Subsystems
        createNormalRecipe(path, cache, TItems.CHAMELEON_CIRCUIT.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.RED_DYE), Ingredient.fromItems(Items.GREEN_DYE), Ingredient.fromItems(Items.BLUE_DYE)));
        createRepairRecipe(path, cache, TItems.CHAMELEON_CIRCUIT.get(), Lists.newArrayList(Ingredient.fromItems(Items.ENDER_PEARL)));
        
        createNormalRecipe(path, cache, TItems.DEMAT_CIRCUIT.get(), Lists.newArrayList(Ingredient.fromItems(Items.ENDER_PEARL), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(Items.IRON_INGOT), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createRepairRecipe(path, cache, TItems.DEMAT_CIRCUIT.get(), Lists.newArrayList(Ingredient.fromItems(Items.ENDER_PEARL)));
        
        createNormalRecipe(path, cache, TItems.FLUID_LINK.get(), Lists.newArrayList(Ingredient.fromItems(Items.IRON_INGOT), Ingredient.fromItems(Items.IRON_INGOT), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TItems.MERCURY_BOTTLE.get())));
        createRepairRecipe(path, cache, TItems.FLUID_LINK.get(), Lists.newArrayList(Ingredient.fromItems(TItems.MERCURY_BOTTLE.get())));
        
        createNormalRecipe(path, cache, TItems.INTERSTITIAL_ANTENNA.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.GLASS_PANE), Ingredient.fromItems(Items.IRON_INGOT), Ingredient.fromItems(Items.REDSTONE_BLOCK)));
        createRepairRecipe(path, cache, TItems.INTERSTITIAL_ANTENNA.get(), Lists.newArrayList(Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(TItems.CIRCUITS.get())));
        
        createNormalRecipe(path, cache, TItems.NAV_COM.get(), Lists.newArrayList(Ingredient.fromItems(Items.CLOCK), Ingredient.fromItems(Items.COMPASS), Ingredient.fromItems(Items.ENDER_EYE), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createRepairRecipe(path, cache, TItems.NAV_COM.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(TItems.CIRCUITS.get())));
        
        createNormalRecipe(path, cache, TItems.SHEILD_GENERATOR.get(), Lists.newArrayList(Ingredient.fromItems(Items.DIAMOND), Ingredient.fromItems(Items.GOLD_INGOT), Ingredient.fromItems(Items.COMPARATOR), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TItems.CIRCUITS.get())));
        createRepairRecipe(path, cache, TItems.SHEILD_GENERATOR.get(), Lists.newArrayList(Ingredient.fromItems(Items.GOLD_INGOT), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.REDSTONE_BLOCK)));
        
        createNormalRecipe(path, cache, TItems.STABILIZERS.get(), Lists.newArrayList(Ingredient.fromItems(Items.OBSIDIAN), Ingredient.fromItems(Items.IRON_BLOCK), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.IRON_BLOCK), Ingredient.fromItems(Items.OBSIDIAN)));
        createRepairRecipe(path, cache, TItems.STABILIZERS.get(), Lists.newArrayList(Ingredient.fromItems(Items.COMPASS), Ingredient.fromItems(TItems.CIRCUITS.get())));
        
        createNormalRecipe(path, cache, TItems.TEMPORAL_GRACE.get(), Lists.newArrayList(Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.SHIELD), Ingredient.fromItems(Items.GOLDEN_APPLE), Ingredient.fromItems(Items.REDSTONE_BLOCK)));
        createRepairRecipe(path, cache, TItems.TEMPORAL_GRACE.get(), Lists.newArrayList(Ingredient.fromItems(Items.IRON_INGOT), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.GOLD_NUGGET)));
        
        //Upgrades
        createNormalRecipe(path, cache, TItems.ATRIUM_UPGRADE.get(), Lists.newArrayList(Ingredient.fromItems(TItems.BLANK_UPGRADE.get()), Ingredient.fromItems(Items.ENDER_PEARL), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.GOLD_INGOT)));
        createNormalRecipe(path, cache, TItems.ELECTRO_CONVERT_UPGRADE.get(), Lists.newArrayList(Ingredient.fromItems(TItems.BLANK_UPGRADE.get()), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.PRISMARINE_CRYSTALS), Ingredient.fromItems(Items.BUCKET)));
        createNormalRecipe(path, cache, TItems.KEY_FOB_UPGRADE.get(), Lists.newArrayList(Ingredient.fromItems(TItems.BLANK_UPGRADE.get()), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.IRON_DOOR), Ingredient.fromItems(Items.TRIPWIRE_HOOK), Ingredient.fromItems(Items.GOLD_INGOT)));
        createNormalRecipe(path, cache, TItems.TIME_LINK_UPGRADE.get(), Lists.newArrayList(Ingredient.fromItems(TItems.BLANK_UPGRADE.get()), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.ENDER_EYE), Ingredient.fromItems(TItems.CIRCUITS.get())));
        createNormalRecipe(path, cache, TItems.ZERO_ROOM_UPGRADE.get(), Lists.newArrayList(Ingredient.fromItems(TItems.BLANK_UPGRADE.get()), Ingredient.fromItems(Items.GHAST_TEAR), Ingredient.fromItems(Items.BLAZE_ROD), Ingredient.fromItems(TItems.CIRCUITS.get())));
        
        //Items
        createNormalRecipe(path, cache, TBlocks.ars_egg.get().asItem(), Lists.newArrayList(Ingredient.fromItems(Items.EGG), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createNormalRecipe(path, cache, TItems.ARS_TABLET.get(), Lists.newArrayList(Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(TBlocks.ars_egg.get().asItem()), Ingredient.fromItems(Items.IRON_TRAPDOOR)));
        
        createNormalRecipe(path, cache, TItems.ARTRON_BATTERY.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createNormalRecipe(path, cache, TItems.ARTRON_BATTERY_MED.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(Items.REDSTONE_BLOCK), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createNormalRecipe(path, cache, TItems.ARTRON_BATTERY_HIGH.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(TItems.ARTRON_BATTERY_MED.get()), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        
        createNormalRecipe(path, cache, TItems.ARTRON_CAPACITOR.get(), Lists.newArrayList(Ingredient.fromItems(TItems.MERCURY_BOTTLE.get()), Ingredient.fromItems(Items.GOLD_INGOT), Ingredient.fromItems(Items.REDSTONE_BLOCK), Ingredient.fromItems(TItems.MERCURY_BOTTLE.get()), Ingredient.fromItems(Items.GOLD_INGOT)));
        createNormalRecipe(path, cache, TItems.ARTRON_CAPACITOR_MEDIUM.get(), Lists.newArrayList(Ingredient.fromItems(TItems.MERCURY_BOTTLE.get()), Ingredient.fromItems(Items.REDSTONE_BLOCK), Ingredient.fromItems(TItems.ARTRON_CAPACITOR.get()), Ingredient.fromItems(Items.BLAZE_POWDER), Ingredient.fromItems(Items.BLAZE_POWDER)));
        createNormalRecipe(path, cache, TItems.ARTRON_CAPACITOR_HIGH.get(), Lists.newArrayList(Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.DIAMOND), Ingredient.fromItems(TItems.ARTRON_CAPACITOR_MEDIUM.get()), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TItems.MERCURY_BOTTLE.get())));
        
        createNormalRecipe(path, cache, TItems.CIRCUITS.get(), Lists.newArrayList(Ingredient.fromItems(TItems.CIRCUIT_PASTE.get()), Ingredient.fromItems(Items.GOLD_NUGGET), Ingredient.fromItems(Items.GOLD_NUGGET), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createNormalRecipe(path, cache, TItems.DATA_CRYSTAL.get(), Lists.newArrayList(Ingredient.fromItems(Items.GOLD_NUGGET), Ingredient.fromItems(Items.GOLD_NUGGET), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem())));
        createNormalRecipe(path, cache, TItems.MONITOR_REMOTE.get(), Lists.newArrayList(Ingredient.fromItems(Items.STONE_BUTTON), Ingredient.fromItems(Items.STONE_BUTTON), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.IRON_INGOT)));
        createNormalRecipe(path, cache, TItems.POCKET_WATCH.get(), Lists.newArrayList(Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(Items.CLOCK)));
        
        createNormalRecipe(path, cache, TBlocks.roundel_tap.get().asItem(), Lists.newArrayList(Ingredient.fromItems(TItems.ARTRON_CAPACITOR.get()), Ingredient.fromItems(Items.IRON_INGOT), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(Items.REDSTONE)));
        
        createNormalRecipe(path, cache, TItems.SONIC_ACTIVATOR.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.STONE_BUTTON)));
        createNormalRecipe(path, cache, TItems.SONIC_EMITTER.get(), Lists.newArrayList(Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.IRON_NUGGET)));
        createNormalRecipe(path, cache, TItems.SONIC_EMITTER.get(), Lists.newArrayList(Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.IRON_NUGGET)));
        createNormalRecipe(path, cache, TItems.SONIC_HANDLE.get(), Lists.newArrayList(Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.IRON_NUGGET), Ingredient.fromItems(Items.IRON_NUGGET)));
        createNormalRecipe(path, cache, TItems.SONIC.get(), Lists.newArrayList(Ingredient.fromItems(TItems.SONIC_EMITTER.get()), Ingredient.fromItems(TItems.SONIC_ACTIVATOR.get()), Ingredient.fromItems(TItems.SONIC_HANDLE.get()), Ingredient.fromItems(TItems.SONIC_END.get())));
        
        createNormalRecipe(path, cache, TItems.DIAGNOSTIC_TOOL.get(), Lists.newArrayList(Ingredient.fromItems(TItems.ARTRON_BATTERY.get()), Ingredient.fromItems(TBlocks.xion_crystal.get().asItem()), Ingredient.fromItems(Items.GLASS), Ingredient.fromItems(TItems.CIRCUITS.get())));
        createNormalRecipe(path, cache, TItems.TARDIS_BACKDOOR.get(), Lists.newArrayList(Ingredient.fromItems(TItems.INT_DOOR.get()), Ingredient.fromItems(Items.GOLD_NUGGET), Ingredient.fromItems(Items.REDSTONE), Ingredient.fromItems(TItems.CIRCUITS.get()), Ingredient.fromItems(Items.ENDER_PEARL)));
    }

    @Override
    public String getName() {
        return "TARDIS Quantiscope Recipe Generator";
    }
    
    public static Path getPath(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/recipes/quantiscope/" + key.getPath() + ".json");
    }
    
    public static Path getPathRepair(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/recipes/quantiscope/" + key.getPath() + "_repair" + ".json");
    }

    /**
     * Create a non-repair recipe. Make sure it is a unique recipe
     * @param output
     * @param ingredients - items that are used to craft the output item. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createNormalRecipe(Path base, DirectoryCache cache, Item output, List<Ingredient> ingredients) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(output, ingredients), getPath(base, output));
    }
    
    public void createNormalRecipe(Path base, DirectoryCache cache, Item output, List<Ingredient> ingredients, int progressTicks) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithProcessTicks(output, ingredients, progressTicks), getPath(base, output));
    }
    
    /**
     * Create a repair recipe. Make sure it is a unique recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - items that are used to repair itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createRepairRecipe(Path base, DirectoryCache cache, Item itemToRepair, List<Ingredient> repairItems) throws IOException {
        IDataProvider.save(GSON, cache, this.createRepairRecipe(itemToRepair, repairItems), getPathRepair(base, itemToRepair));
    }
    
    /**
     * Creates a non-repair recipe. Make sure it is a unique recipe
     * @param output - this is the item that is output at the end
     * @param ingredients - the items that are used to craft output. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRecipe(Item output, List<Ingredient> ingredients) {
        WeldRecipe recipe = new WeldRecipe(false, Optional.empty(), ingredients, Optional.of(new WeldRecipe.RecipeResult(output)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }
    
    public JsonObject createRecipeWithProcessTicks(Item output, List<Ingredient> ingredients, int progressTicks) {
    	WeldRecipe recipe = new WeldRecipe(false, Optional.empty(), Optional.of(progressTicks), ingredients, Optional.of(new WeldRecipe.RecipeResult(output)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }
    
    /**
     * Creates a repair recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - the items that are used to repair the itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRepairRecipe(Item itemToRepair, List<Ingredient> repairItems) {
    	WeldRecipe recipe = new WeldRecipe(true, Optional.of(itemToRepair), Optional.of(200), repairItems, Optional.of(new WeldRecipe.RecipeResult(itemToRepair)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }
    
    public JsonObject createRepairRecipeWithProgress(Item itemToRepair, List<Ingredient> repairItems, int progressTicks) {
    	WeldRecipe recipe = new WeldRecipe(true, Optional.of(itemToRepair), Optional.of(progressTicks), repairItems, Optional.of(new WeldRecipe.RecipeResult(itemToRepair)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }

}
