package net.tardis.mod.datagen;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;

import java.io.IOException;
import java.nio.file.Path;

public abstract class BaseDataProvider implements IDataProvider {

    protected DataGenerator generator;
    private String name;

    public BaseDataProvider(DataGenerator gen){
        this.generator = gen;
    }

    @Override
    public void act(DirectoryCache cache) throws IOException {
        this.act(cache, this.getBasePath());
    }

    public abstract void act(DirectoryCache cache, Path base) throws IOException;

    public Path getBasePath(){
        return this.generator.getOutputFolder();
    }

    @Override
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
}
