package net.tardis.mod.compat.jei;

import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.client.gui.GuiUtils;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.recipe.WeldRecipe;

public class WeldRecipeCategory implements IRecipeCategory<WeldRecipe> {

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "quantiscope_weld");
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/weld_jei.png");
	
	private IDrawable background;
	private IDrawable icon;
	private IDrawable arrow;
	
	public WeldRecipeCategory(IGuiHelper gui) {
		background = gui.createDrawable(TEXTURE, 12, 5, 153, 64);
		icon = gui.createDrawableIngredient(new ItemStack(TBlocks.quantiscope_brass.get()));
		arrow = gui.drawableBuilder(TEXTURE, 178, 0, 22, 16)
				.buildAnimated(200, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public ResourceLocation getUid() {
		return NAME;
	}

	@Override
	public Class<WeldRecipe> getRecipeClass() {
		return WeldRecipe.class;
	}

	@Override
	public String getTitle() {
		return TardisConstants.Translations.QUANTISCOPE_JEI_TITLE.getString();
	}

	@Override
	public IDrawable getBackground() {
		return this.background;
	}

	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIngredients(WeldRecipe recipe, IIngredients ingredients) {
		
		ingredients.setInputIngredients(recipe.getIngredients());
		ingredients.setOutput(VanillaTypes.ITEM, new ItemStack(recipe.getResult().get().getOutput()));
	}

	@Override
	public void draw(WeldRecipe recipe, MatrixStack matrixStack, double mouseX, double mouseY) {
		IRecipeCategory.super.draw(recipe, matrixStack, mouseX, mouseY);
		arrow.draw(matrixStack, 78, 25);
		int maxProgress = recipe.getProcessingTicks().orElse(200);
		List<ITextComponent> progressText = TextHelper.createProgressBarTooltip(0, maxProgress, true);
		int mouseXPos = (int)mouseX;
        int mouseYPos = (int)mouseY;
        Minecraft mc = Minecraft.getInstance();
		if (Helper.isInBounds(mouseXPos, mouseYPos, 75, 25, 100, 40))
            GuiUtils.drawHoveringText(matrixStack, progressText, mouseXPos, mouseYPos, 200, 50, 100, mc.fontRenderer);
        
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, WeldRecipe recipe, IIngredients ingredients) {
		
		ItemStack[] item = new ItemStack[5];
//		List<ItemStack> stacksInIngredient = Arrays.asList(recipe.getRequiredIngredients().getMatchingStacks());
//		for(int i = 0; i < item.length; ++i) {
//			if(i < stacksInIngredient.size())
//				item[i] = stacksInIngredient.get(i);
//			else item[i] = ItemStack.EMPTY;
//		}
		List<Ingredient> ingredientList = recipe.getRequiredIngredients();
		
		//Inputs
		
		JEIHelper.addInputSlot(recipeLayout, 0, 40, 3, JEIHelper.getValidIngredientFromList(0, ingredientList).getMatchingStacks());
		JEIHelper.addInputSlot(recipeLayout, 1, 19, 3, JEIHelper.getValidIngredientFromList(1, ingredientList).getMatchingStacks());
		JEIHelper.addInputSlot(recipeLayout, 2, 6, 23, JEIHelper.getValidIngredientFromList(2, ingredientList).getMatchingStacks());
		JEIHelper.addInputSlot(recipeLayout, 3, 19, 43, JEIHelper.getValidIngredientFromList(3, ingredientList).getMatchingStacks());
		JEIHelper.addInputSlot(recipeLayout, 4, 40, 43, JEIHelper.getValidIngredientFromList(4, ingredientList).getMatchingStacks());
		
		//Repair
		if(recipe.isRepair()) {
			ItemStack repair = new ItemStack(recipe.getResult().get().getOutput());
			repair.setDamage(repair.getMaxDamage() - 1);
			JEIHelper.addInputSlot(recipeLayout, 5, 54, 23, repair);
		}
		
		recipeLayout.getItemStacks().init(6, false, 110, 23);
		recipeLayout.getItemStacks().set(6, new ItemStack(recipe.getResult().get().getOutput()));
	}
}
