package net.tardis.mod.traits;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * 
 * @author Spectre
 *
 * Extend this if you need the trait to deal with an entity's death, see @code {@link #onPlayerKilledEntity(PlayerEntity, ConsoleTile, LivingEntity)}
 */
public abstract class AbstractEntityDeathTrait extends TardisTrait{

	public AbstractEntityDeathTrait(TardisTraitType type) {
		super(type);
	}
	
	/**
	 * Called when one of the players being tracked by this TARDIS's emotional handler kills an entity
	 * @param player - One of the players which is being tracked by this TARDIS's emotional handler
	 * @param tardis - ConsoleTile this trait is attached to
	 * @param victim - The entity the owner killed
	 */
	public abstract void onPlayerKilledEntity(PlayerEntity player, ConsoleTile tardis, LivingEntity victim);

}
