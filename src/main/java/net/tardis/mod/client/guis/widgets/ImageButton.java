package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class ImageButton extends Widget{

	ResourceLocation texture;
	Runnable pressable;
	int u, v;
	
	public ImageButton(int xIn, int yIn, int u, int v, int widthIn, int heightIn, ITextComponent title, ResourceLocation texture) {
		super(xIn, yIn, widthIn, heightIn, title);
		this.u = u;
		this.v = v;
		this.texture = texture;
	}
	
	public ImageButton(int xIn, int yIn, int u, int v, int widthIn, int heightIn, ResourceLocation texture) {
		super(xIn, yIn, widthIn, heightIn, new TranslationTextComponent(""));
		this.u = u;
		this.v = v;
		this.texture = texture;
	}

	@Override
	public void renderWidget(MatrixStack matrixStack, int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_) {
		Minecraft.getInstance().textureManager.bindTexture(texture);
		int offset  = this.isHovered ? height : 0;
		this.blit(matrixStack, x, y, u, v + offset, width, height + offset);
	}
	
	public void setPressable(Runnable press) {
		this.pressable = press;
	}

	@Override
	public void onClick(double p_onClick_1_, double p_onClick_3_) {
		super.onClick(p_onClick_1_, p_onClick_3_);
		if(this.pressable != null)
			this.pressable.run();
			
	}
}
