package net.tardis.mod.client.guis.ars;

import net.minecraft.client.Minecraft;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateARSTablet;

public class ARSPieceButton extends TextButton {

    private ARSPiece piece;

    public ARSPieceButton(int x, int y, ARSPiece piece) {
        super(x, y, piece.isDataPack() ? piece.getDisplayName().getKey() : piece.getDisplayName().getString(), null);
        this.piece = piece;
    }

    @Override
    public void onPress() {
        Network.sendToServer(new UpdateARSTablet(piece.getRegistryName()));
        Minecraft.getInstance().displayGuiScreen(null);
    }
}
