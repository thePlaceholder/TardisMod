package net.tardis.mod.client.guis.vm;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.lwjgl.glfw.GLFW;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.guis.INeedTardisNames;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMDistressMessage;

public class VortexMDistressScreen extends VortexMFunctionScreen implements INeedTardisNames{

    private final TranslationTextComponent title = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.title");
    private final TranslationTextComponent selectTardis = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.select_tardis");
    private final TranslationTextComponent messageDesc = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.message");
    private final TranslationTextComponent userFound = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.tardis_found");
    private final TranslationTextComponent userNotFound = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.tardis_invalid");
    private final TranslationTextComponent sentText = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.send_signal");
    private final TranslationTextComponent checkNameText = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "distress.check_name");
    private TextFieldWidget userInput;
    private TextFieldWidget messageBox;
    private Button send;
    private Button back;
    private Button increment;
    private Button decrement;
    private Button checkName;
    private int index = 0;
    private Map<ResourceLocation, String> names = Maps.newHashMap();
    private ArrayList<Entry<ResourceLocation, String>> nameList = Lists.newArrayList();
    private Entry<ResourceLocation, String> selectedTardis;
    private boolean isValid; //Used for rendering error messages on gui
    private boolean hasCheckedName; //Second flag to check if we should render error message

    public VortexMDistressScreen(ITextComponent title) {
        super(title);
    }

    public VortexMDistressScreen() {
    }

    @Override
    public void init() {
        super.init();
        String next = ">";
        String previous = "<";
        String sendButton = sentText.getString();
        String checkNames = checkNameText.getString();
        final int btnH = 20;

        userInput = new TextFieldWidget(this.font, this.getMinX() + 62, this.getMaxY() + 55, 150, this.font.FONT_HEIGHT + 5, new TranslationTextComponent(""));
        messageBox = new TextFieldWidget(this.font, this.getMinX() + 75, this.getMaxY() + 100, 120, this.font.FONT_HEIGHT + 5, new TranslationTextComponent(""));
        send = new Button(this.getMinX() + 100, this.getMaxY() + 120, this.font.getStringWidth(sendButton) + 10, btnH, new TranslationTextComponent(sendButton), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                //Check if text box input has valid player.
                checkInputName();
                if (isValid) {
                    Network.sendToServer(new VMDistressMessage(selectedTardis.getKey(), messageBox.getText()));
                    closeScreen();
                }
            }
        });
        back = new TextButton(this.getMaxX() + 5, this.getMaxY() + 5, TardisConstants.Translations.GUI_BACK, new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                ClientHelper.openGUI(TardisConstants.Gui.VORTEX_MAIN, null);
            }
        });
        increment = new Button(this.getMinX() + 215, this.getMaxY() + 53, 20, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(next), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                modIndex(1);
            }
        });
        decrement = new Button(this.getMinX() + 35, this.getMaxY() + 53, 20, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(previous), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                modIndex(-1);
            }
        });
        checkName = new Button(this.getMinX() + 165, this.getMaxY() + 125, 65, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(checkNames), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                checkInputName();
            }
        });

        this.buttons.clear();
        this.addButton(userInput);
        this.addButton(send);
        this.addButton(increment);
        this.addButton(decrement);
        this.addButton(back);
        this.addButton(messageBox);
        userInput.setFocused2(true);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, title.getString(), this.getMinX() + 135, this.getMaxY() + 20, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, selectTardis.getString(), this.getMinX() + 135, this.getMaxY() + 40, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, messageDesc.getString(), this.getMinX() + 135, this.getMaxY() + 85, 0xFFFFFF);
        if (this.hasCheckedName) {
            if (!this.isValid && !this.userInput.isFocused()) {
            	String user = userInput.getText();
            	String output = userInput.getText() == null ? "Null" : user;
            	StringTextComponent text = new StringTextComponent(userNotFound.getString() + " " + output);
            	this.font.func_238418_a_(text, this.getMinX() + 10, this.getMaxY() + 145, 300, 0xffcc00);
            } else if (this.isValid && !this.userInput.getText().isEmpty() && !this.userInput.isFocused()) {
            	this.font.func_238418_a_(userFound, this.getMinX() + 95, this.getMaxY() + 145, 200, 0x00FF00);
            }
        }
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void closeScreen() {
        super.closeScreen();
    }

    @Override
    public boolean isPauseScreen() {
        return true;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
        if (this.userInput.isFocused()) {
            if (keyCode == GLFW.GLFW_KEY_TAB) {
                modIndex(1);
            }
            super.keyPressed(keyCode, scanCode, bitmaskModifier);
            return true;
        }
        return super.keyPressed(keyCode, scanCode, bitmaskModifier);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int mouseZ) {
        if (!this.checkName.mouseClicked(mouseX, mouseY, mouseZ))
            this.hasCheckedName = false;
        return super.mouseClicked(mouseX, mouseY, mouseZ);
    }

    @Override
    public void renderBackground(MatrixStack matrixStack) {
        super.renderBackground(matrixStack);
    }

    @Override
    public int texWidth() {
        return super.texWidth();
    }

    @Override
    public int texHeight() {
        return super.texHeight();
    }

    @Override
    public int getMinY() {
        return super.getMinY();
    }

    @Override
    public int getMinX() {
        return super.getMinX();
    }

    @Override
    public int getMaxX() {
        return super.getMaxX();
    }

    @Override
    public int getMaxY() {
        return super.getMaxY();
    }

    public void modIndex(int amount) {
        int temp = index + amount;
        if (temp < nameList.size() && temp > 0) {
            this.index = temp;
        }
        if (temp > nameList.size() - 1) {
            this.index = 0;
        } else if (temp < 0) {
            this.index = nameList.size() - 1;
        }
        if (!this.nameList.isEmpty()) {
        	this.selectedTardis = this.nameList.get(index);
            this.userInput.setText(this.selectedTardis != null ? selectedTardis.getValue() : "");
            this.userInput.setCursorPositionZero();
        }
        
    }

    /**
     * Used to check if textbox contents match a valid timeship
     *
     * @implSpec Only use this for the Check name button
     */
    public void checkInputName() {

        String name = this.userInput.getText();
        for (Entry<ResourceLocation, String> entry : this.names.entrySet()) {
        	if (entry.getValue().contentEquals(name)) {
        		this.isValid = true;
        		this.selectedTardis = entry;
        		break; //stop searching for valid timeships once we find one
        	}
        	else {
        		this.isValid = false;
        		this.selectedTardis = entry;
        		continue; //continue searching until we find a matching one
            }
        }
        this.hasCheckedName = true;
    }

	@Override
	public void setNamesFromServer(Map<ResourceLocation, String> nameMap) {
		this.names.clear();
		this.names.putAll(nameMap);
		this.names.entrySet().forEach(entry -> {
			this.nameList.add(entry);
		});
	}

}
