package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.renderer.*;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Quaternion;
import net.tardis.mod.client.models.exteriors.DisguiseDoorModel;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.helper.BlockPosHelper;
import net.tardis.mod.helper.Helper;
import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.blocks.exteriors.ExteriorBlock;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.tileentities.exteriors.DisguiseExteriorTile;

public class DisguiseExteriorTileRenderer extends ExteriorRenderer<DisguiseExteriorTile> {


    public static final ResourceLocation TEXTURE = Helper.createRL("textures/exteriors/disguise.png");
    public static final DisguiseDoorModel MODEL = new DisguiseDoorModel();

    public DisguiseExteriorTileRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void render(DisguiseExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        
        Disguise disguise = tile.disguise;
        if(disguise != null) {
            matrixStackIn.push();
            Minecraft.getInstance().getBlockRendererDispatcher().renderModel(disguise.getTopState(), tile.getPos(), tile.getWorld(), matrixStackIn, bufferIn.getBuffer(RenderTypeLookup.getChunkRenderType(disguise.getTopState())), false, tile.getWorld().rand, tile.getModelData());
            matrixStackIn.pop();
            
            matrixStackIn.push();
            matrixStackIn.translate(0, -1, 0); //Have to translate this one down as it'll render at the top state position by default
            Minecraft.getInstance().getBlockRendererDispatcher().renderModel(disguise.getBottomState(), tile.getPos().down(), tile.getWorld(), matrixStackIn, bufferIn.getBuffer(RenderTypeLookup.getChunkRenderType(disguise.getBottomState())), false, tile.getWorld().rand, tile.getModelData());
            matrixStackIn.pop();
            
        }
        
        matrixStackIn.pop();


        if(tile != null && tile.getBotiWorld() != null && tile.getOpen() != EnumDoorState.CLOSED){
            PortalInfo info = new PortalInfo();
            info.setWorldShell(tile.getBotiWorld());
            info.setPosition(tile.getPos());

            final Direction dir = WorldHelper.getFacingForState(tile.getWorld().getBlockState(tile.getPos()));
            info.setTranslate(matrix -> {

                Quaternion rot = Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(dir));
                if(dir == Direction.EAST || dir == Direction.WEST)
                    rot.multiply(Vector3f.YP.rotationDegrees(180));
                matrix.rotate(rot);
                matrix.translate(0, -0.5, 0);
            });

            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(tile.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, -0.5, -0.5);
            });

            info.setRenderPortal((matrix, buf) -> {
                MODEL.renderBones(tile, 1.0F, matrix, buf.getBuffer(RenderType.getEntitySolid(TEXTURE)), combinedLightIn, combinedOverlayIn, 1.0F);
                buf.finish();
            });



            BOTIRenderer.addPortal(info);
        }
        
    }
    
    @Override
    public void renderExterior(DisguiseExteriorTile tile, float partialTicks, MatrixStack matrixStackIn,
            IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
        
    }

}
