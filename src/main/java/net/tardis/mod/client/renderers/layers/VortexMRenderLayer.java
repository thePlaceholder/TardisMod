package net.tardis.mod.client.renderers.layers;


import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.HandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.models.misc.VortexMItemLayerModel;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;


public class VortexMRenderLayer extends LayerRenderer<AbstractClientPlayerEntity,PlayerModel<AbstractClientPlayerEntity>>{
	
	private final IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> renderPlayer;
	private ItemStack stack = new ItemStack(TItems.VORTEX_MANIP.get());
	public static VortexMItemLayerModel MODEL = new VortexMItemLayerModel(1F);
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/item/vm_entity.png");
	
	public VortexMRenderLayer(
			IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> entityRendererIn) {
		super(entityRendererIn);
		this.renderPlayer = entityRendererIn;
	}
    /** Detects if this player's game profile skin type is slim or regular. 
     * Yes, use of OnlyIn is dumb, but this is needed for this client side use. We should get rid of this in the future*/
	@OnlyIn(Dist.CLIENT)
	public static boolean hasSmallArms(AbstractClientPlayerEntity player) {
		return player.getSkinType().equals("slim");
	}
	

	@Override
	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn,
	        AbstractClientPlayerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTicks,
	        float ageInTicks, float netHeadYaw, float headPitch) {
		if (entityIn.inventory.hasItemStack(stack)) {
			ItemStack heldStack = PlayerHelper.getHeldOrNearestStack(entityIn, stack);
			this.renderVMOnOppositeArm(entityIn, heldStack, limbSwing , limbSwingAmount, partialTicks , entityIn.getPrimaryHand(), matrixStackIn, bufferIn, packedLightIn);
		}
	}
	
	private void renderVMOnOppositeArm(AbstractClientPlayerEntity entityIn, ItemStack stack, float limbSwing, 
			float limbSwingAmount, float partialTicks, HandSide primarySide,
			MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, 
			int combinedLightIn) {
		boolean slim = hasSmallArms(entityIn);
		boolean isPrimaryRightHand = primarySide == HandSide.RIGHT;
		matrixStackIn.push();
		if (slim) {
			matrixStackIn.scale(0.825F, 1F, 1F); //Squeeze sides to make it fit alex arm if the player's skin is alex
			matrixStackIn.translate(isPrimaryRightHand ? 0.05 : -0.05, 0, 0); //Shift the model to right if primary hand is right arm, and to the left if the primary hand is the left hand
		}
		
		if (!isPrimaryRightHand){ //If primary hand is left, render this model on the right hand
			//Rotate in radians
			MODEL.getVMRoot().rotateAngleZ = (-(float)Math.PI / 1F); //Flip the model by 180 degrees so it faces the correct way from a user stand point
			MODEL.getArmRoot().setRotationPoint(3, 2, 0); //Set the pivot point to the right arm if the primary hand is left
		}
		else {
			MODEL.getVMRoot().rotateAngleZ = 0; //Reset the rotation of the model to 0
			MODEL.getArmRoot().setRotationPoint(5, 2, 0); //Reset pivot point to left arm if primary hand is right
		}
		this.translateHand(isPrimaryRightHand ? HandSide.LEFT : HandSide.RIGHT, matrixStackIn);
		MODEL.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(TEXTURE)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
			MODEL.getLid().rotateAngleY = cap.getOpen() ? -45 : 0;
		});
		matrixStackIn.pop();
	}
	
	public void translateHand(HandSide sideIn, MatrixStack matrixStackIn) {
	      this.getArmForSide(sideIn).translateRotate(matrixStackIn);
    }
	
	public ModelRenderer getArmForSide(HandSide side) {
	    return side == HandSide.LEFT ? this.getEntityModel().bipedLeftArm : this.getEntityModel().bipedRightArm;
	}

}
