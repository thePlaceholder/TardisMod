package net.tardis.mod.client.renderers.layers;


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.ClientHelper;


public class LaserHurtRenderLayer<E extends LivingEntity, M extends EntityModel<E>> extends LayerRenderer<E,M>{
	
	private final IEntityRenderer<E, M> renderEntity;
	public static final ResourceLocation TEXTURE = new ResourceLocation("textures/entity/creeper/creeper_armor.png");
	
	public LaserHurtRenderLayer(
			IEntityRenderer<E, M> entityRendererIn) {
		super(entityRendererIn);
		this.renderEntity = entityRendererIn;
	}	

	@Override
	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn,
	        E entityIn, float limbSwing, float limbSwingAmount, float partialTicks,
	        float ageInTicks, float netHeadYaw, float headPitch) {
		if (ClientHelper.ENTITIES_HURT_BY_LASER.contains(entityIn)) {
			if (entityIn.hurtTime != 0) {
			    EntityModel<E> model = this.renderEntity.getEntityModel();
			    if (model != null) {
			    	matrixStackIn.push();
			    	float swirl = (float)entityIn.ticksExisted + partialTicks;
			    	IVertexBuilder ivertexbuilder = bufferIn.getBuffer(RenderType.getEnergySwirl(this.getTexture(), 0, swirl * 0.01F));
			    	boolean isPlayer = false;
				    if (entityIn instanceof PlayerEntity) {
				    	isPlayer = true;
				    }
			    	float scale = isPlayer ? 1.1F : 1.05F;
				    matrixStackIn.scale(scale, scale, scale);
				    model.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTicks);
				    this.getEntityModel().copyModelAttributesTo(model);
				    model.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
				    model.render(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 0.5F, 0.5F, 0.5F, 1.0F);
				    matrixStackIn.pop();
			    }
			}
			else {
			    ClientHelper.ENTITIES_HURT_BY_LASER.remove(entityIn);
			}
		}
		
	}
	
	public ResourceLocation getTexture() {
	    return TEXTURE;
	}
	
	

}
