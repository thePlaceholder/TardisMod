package net.tardis.mod.client.models.entity;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;

public class SpacesuitModel extends TBaseArmorModel<LivingEntity>{
	
	private final ModelRenderer bipedHead;
	private final ModelRenderer ring;
	private final ModelRenderer bipedBody;
	private final ModelRenderer belt;
	private final ModelRenderer tank;
	private final ModelRenderer collar;
	private final ModelRenderer bipedLeftArm;
	private final ModelRenderer leftshoulder;
	private final ModelRenderer patch;
	private final ModelRenderer bipedRightArm;
	private final ModelRenderer rightshoulder;
	private final ModelRenderer bipedLeftLeg;
	private final ModelRenderer bipedRightLeg;
	private EquipmentSlotType slot = EquipmentSlotType.HEAD;

	public SpacesuitModel(float modelSize, EquipmentSlotType slot) {
		super(modelSize);
		this.slot = slot;
		textureWidth = 128;
		textureHeight = 128;

		bipedHead = new ModelRenderer(this);
		bipedHead.setRotationPoint(0.0F, -1.0F, 0.0F);
		bipedHead.setTextureOffset(0, 0).addBox(-5.0F, -8.0F, -5.0F, 10.0F, 8.0F, 10.0F, 0.0F, false);
		bipedHead.setTextureOffset(50, 78).addBox(0.5F, -5.25F, 4.25F, 1.0F, 3.0F, 2.0F, 0.0F, false);
		bipedHead.setTextureOffset(44, 78).addBox(-1.5F, -5.25F, 4.25F, 1.0F, 3.0F, 2.0F, 0.0F, false);

		ring = new ModelRenderer(this);
		ring.setRotationPoint(0.5F, 0.5F, 0.25F);
		bipedHead.addChild(ring);
		setRotationAngle(ring, 0.2618F, 0.0F, 0.0F);
		ring.setTextureOffset(0, 18).addBox(-6.0F, -2.5F, -5.0F, 11.0F, 1.0F, 11.0F, 0.0F, false);

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedBody.setTextureOffset(44, 18).addBox(-5.0F, 0.0F, -2.5F, 10.0F, 12.0F, 5.0F, 0.0F, false);
		bipedBody.setTextureOffset(42, 69).addBox(-2.5F, 1.5F, -3.5F, 5.0F, 5.0F, 1.0F, 0.0F, false);

		belt = new ModelRenderer(this);
		belt.setRotationPoint(0.0F, 12.0F, 0.0F);
		bipedBody.addChild(belt);
		belt.setTextureOffset(10, 78).addBox(-5.5F, -3.5F, -3.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
		belt.setTextureOffset(0, 78).addBox(2.5F, -3.5F, -3.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
		belt.setTextureOffset(64, 69).addBox(2.5F, -3.5F, 1.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);
		belt.setTextureOffset(54, 69).addBox(-5.25F, -3.5F, 1.0F, 3.0F, 4.0F, 2.0F, 0.0F, false);

		tank = new ModelRenderer(this);
		tank.setRotationPoint(0.5F, 4.0F, 0.25F);
		bipedBody.addChild(tank);
		tank.setTextureOffset(24, 69).addBox(-3.0F, -2.604F, 1.1838F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		collar = new ModelRenderer(this);
		collar.setRotationPoint(0.5F, -0.5F, 0.25F);
		bipedBody.addChild(collar);
		setRotationAngle(collar, 0.2618F, 0.0F, 0.0F);
		collar.setTextureOffset(40, 0).addBox(-6.0F, -1.5341F, -4.2588F, 11.0F, 3.0F, 11.0F, 0.0F, false);
		collar.setTextureOffset(20, 52).addBox(-4.0F, -0.3093F, 0.4483F, 7.0F, 4.0F, 6.0F, 0.0F, false);

		bipedLeftArm = new ModelRenderer(this);
		bipedLeftArm.setRotationPoint(5.0F, 2.0F, 0.0F);
		bipedLeftArm.setTextureOffset(28, 78).addBox(0.0F, 3.0F, 2.0F, 3.0F, 4.0F, 1.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(20, 34).addBox(-1.0F, -2.0F, -2.5F, 5.0F, 13.0F, 5.0F, 0.0F, false);

		leftshoulder = new ModelRenderer(this);
		leftshoulder.setRotationPoint(1.2813F, -0.5F, 0.0F);
		bipedLeftArm.addChild(leftshoulder);
		setRotationAngle(leftshoulder, 0.0F, 0.0F, 0.3491F);
		leftshoulder.setTextureOffset(46, 52).addBox(-2.5554F, -2.5639F, -3.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

		patch = new ModelRenderer(this);
		patch.setRotationPoint(4.5F, 3.0F, 0.0F);
		bipedLeftArm.addChild(patch);
		setRotationAngle(patch, -0.7854F, 0.0F, 0.0F);
		patch.setTextureOffset(20, 78).addBox(-1.25F, -1.0F, -0.9393F, 1.0F, 3.0F, 3.0F, 0.0F, false);

		bipedRightArm = new ModelRenderer(this);
		bipedRightArm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		bipedRightArm.setTextureOffset(36, 78).addBox(-3.0F, 3.0F, 2.0F, 3.0F, 4.0F, 1.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(0, 34).addBox(-4.0F, -2.0F, -2.5F, 5.0F, 13.0F, 5.0F, 0.0F, false);

		rightshoulder = new ModelRenderer(this);
		rightshoulder.setRotationPoint(-1.2813F, -0.5F, 0.0F);
		bipedRightArm.addChild(rightshoulder);
		setRotationAngle(rightshoulder, 0.0F, 0.0F, -0.3491F);
		rightshoulder.setTextureOffset(0, 69).addBox(-3.4446F, -2.5639F, -3.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

		bipedLeftLeg = new ModelRenderer(this);
		bipedLeftLeg.setRotationPoint(1.9F, 12.0F, 0.0F);
		bipedLeftLeg.setTextureOffset(0, 52).addBox(-1.9375F, 0.0F, -2.5F, 5.0F, 12.0F, 5.0F, 0.0F, false);

		bipedRightLeg = new ModelRenderer(this);
		bipedRightLeg.setRotationPoint(-1.9F, 12.0F, 0.0F);
		bipedRightLeg.setTextureOffset(40, 35).addBox(-3.125F, 0.0F, -2.5F, 5.0F, 12.0F, 5.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(LivingEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netbipedHeadYaw, float bipedHeadPitch){
		super.setRotationAngles(entity, limbSwing, limbSwingAmount, ageInTicks, netbipedHeadYaw, bipedHeadPitch);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		if (slot == EquipmentSlotType.HEAD) {
			super.bipedHead = this.bipedHead;
			super.bipedHead.render(matrixStack, buffer, packedLight, packedOverlay);
        }
        if (slot == EquipmentSlotType.CHEST) {
        	super.bipedLeftArm = bipedLeftArm; //We need to set the super class value to our value 
        	super.bipedRightArm = bipedRightArm;
        	super.bipedBody = bipedBody;
        	super.bipedBody.render(matrixStack, buffer, packedLight, packedOverlay);
        	super.bipedRightArm.render(matrixStack, buffer, packedLight, packedOverlay);
        	super.bipedLeftArm.render(matrixStack, buffer, packedLight, packedOverlay);
        }
        if (slot == EquipmentSlotType.LEGS || slot == EquipmentSlotType.FEET) {
        	super.bipedLeftLeg = this.bipedLeftLeg;
        	super.bipedRightLeg = this.bipedRightLeg;
        	super.bipedLeftLeg.render(matrixStack, buffer, packedLight, packedOverlay);
    		super.bipedRightLeg.render(matrixStack, buffer, packedLight, packedOverlay);
        }
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}