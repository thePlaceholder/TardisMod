package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.DoorEntity;
/** Generic template for InteriorDoor models.*/
public interface IInteriorDoorRenderer {
	void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay);
    void renderBones(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay);
    void renderBoti(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay);
    ResourceLocation getTexture();
 
    /** Helper method to render the door bones when BOTI is not enabled via the Client Config.
     * <br> If BOTI is enabled, only render the doors when the EnumDoorState is not CLOSED
     * <br> Only call this if your door model is facing into the BOTI Window, otherwise you don't need this custom logic*/
    void renderDoorWhenClosed(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, ModelRenderer doorBone);

    /** Overload version of {@linkplain renderDoorWhenClosed} for models with seperate bones for the left and right doors*/
    void renderDoorWhenClosed(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, ModelRenderer... doorBones);

    /** If the interior door bone will be facing inside the BOTI window when rotated.
     * <p>  This is to provide a more seamless transition between:
     * <br> Closed door state (when BOTI is not rendering)
     * <br> Open door state (when BOTI is rendering)
     * <p>  This is used for {@linkplain renderDoorWhenClosed}. 
     * @return 
     * True - The logic in {@linkplain renderDoorWhenClosed} will be executed
     * <br> False - The passed in door ModelRenderer will render as normal
     * */
    boolean doesDoorOpenIntoBotiWindow();
}
