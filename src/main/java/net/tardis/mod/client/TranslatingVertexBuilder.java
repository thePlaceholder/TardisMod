package net.tardis.mod.client;

import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.util.math.vector.Vector3d;
/** Custom vertex builder to allow for translating
 * <br> Credits: GiantLuigi4
 * <br> Made to deal with Mojang's legacy code in fluid rendering preventing it from be able to be translated by matrix stacks*/
public class TranslatingVertexBuilder implements IVertexBuilder {
	public Vector3d offset = new Vector3d(0, 0, 0);
	float scale;
	IVertexBuilder parent;
	
	public TranslatingVertexBuilder(float scale, IVertexBuilder parent, Vector3d offset) {
		this.scale = scale;
		this.parent = parent;
		this.offset = offset;
	}
	
	public TranslatingVertexBuilder(IVertexBuilder parent, Vector3d offset) {
		this(1, parent, offset);
	}
	
	public TranslatingVertexBuilder(IVertexBuilder parent) {
		this(1, parent, new Vector3d(0,0,0));
	}
	
	@Override
	public IVertexBuilder pos(double x, double y, double z) {
		parent = parent.pos((x + offset.x) * scale, (y + offset.y) * scale, (z + offset.z) * scale);
		return this;
	}
	
	@Override
	public IVertexBuilder color(int red, int green, int blue, int alpha) {
		parent = parent.color(red, green, blue, alpha);
		return this;
	}
	
	@Override
	public IVertexBuilder tex(float u, float v) {
		parent = parent.tex(u, v);
		return this;
	}
	
	@Override
	public IVertexBuilder overlay(int u, int v) {
		parent = parent.overlay(u, v);
		return this;
	}
	
	@Override
	public IVertexBuilder lightmap(int u, int v) {
		parent = parent.lightmap(u, v);
		return this;
	}
	
	@Override
	public IVertexBuilder normal(float x, float y, float z) {
		parent = parent.normal(x, y, z);
		return this;
	}
	
	@Override
	public void endVertex() {
		parent.endVertex();
	}
}