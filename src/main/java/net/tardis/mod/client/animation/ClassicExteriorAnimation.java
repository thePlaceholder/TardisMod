package net.tardis.mod.client.animation;

import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ClassicExteriorAnimation extends ExteriorAnimation {

	private float alpha = 0;

	public ClassicExteriorAnimation(ExteriorAnimationEntry entry, ExteriorTile tile) {
		super(entry, tile);
	}

	@Override
	public void tick(int timeLeft) {

		if(exterior.getMatterState() == EnumMatterState.DEMAT)
			alpha = timeLeft / (float)this.getMaxTime();
		else if(exterior.getMatterState() == EnumMatterState.REMAT)
			alpha = 1.0F - (timeLeft / (float)this.getMaxTime());
	}

	@Override
	public float getAlpha() {
		return this.alpha;
	}

	@Override
	public void startAnim(EnumMatterState state, int dematTime) {
		super.startAnim(state, dematTime);
		if(state == EnumMatterState.DEMAT)
			alpha = 1.0F;
		else alpha = 0.0F;

	}
}
