package net.tardis.mod.entity.projectiles;

import net.minecraft.block.BlockState;
import net.minecraft.block.TNTBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.misc.AbstractWeapon;

/**
 * Created by Swirtzly
 * on 06/10/2019 @ 12:17
 */
public class LaserEntity extends ThrowableEntity {

    public float damage = 10F, scale = 0.5F;
    private Vector3d color = new Vector3d(0, 1, 1);
    private DamageSource source = DamageSource.GENERIC;
    private AbstractWeapon<? extends LivingEntity> weapon;
    private float rayLength = 1F;
    
    public static final DataParameter<Float> RAY_LENGTH = EntityDataManager.createKey(LaserEntity.class, DataSerializers.FLOAT);
    public static final DataParameter<CompoundNBT> COLOR = EntityDataManager.createKey(LaserEntity.class, DataSerializers.COMPOUND_NBT);

    public LaserEntity(EntityType<? extends ThrowableEntity> type, World worldIn) {
        super(type, worldIn);
    }

    public LaserEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn) {
        super(type, x, y, z, worldIn);
    }


    public LaserEntity(EntityType<? extends ThrowableEntity> type, LivingEntity livingEntityIn, World worldIn) {
        super(type, livingEntityIn, worldIn);
    }

    public LaserEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn, LivingEntity livingEntityIn, float damage, DamageSource source) {
        super(type, x, y, z, worldIn);
        this.setShooter(livingEntityIn);
        this.damage = damage;
        this.source = source;
    }

    public LaserEntity(EntityType<? extends ThrowableEntity> type, LivingEntity livingEntityIn, World worldIn, float damage, DamageSource source) {
        super(type, livingEntityIn, worldIn);
        this.damage = damage;
        this.source = source;
    }

    public LaserEntity(World world) {
        this(TEntities.LASER.get(), world);
    }
    
    @Override
    protected void registerData() {
    	getDataManager().register(RAY_LENGTH, this.rayLength);
    	getDataManager().register(COLOR, new CompoundNBT());
    }

    @Override
    public void readAdditional(CompoundNBT compoundNBT) {
        super.readAdditional(compoundNBT);
        this.damage = compoundNBT.getFloat("damage");
        
        CompoundNBT colorData = compoundNBT.getCompound("color");
        this.getDataManager().set(COLOR, colorData);
        Vector3d colorFromData = new Vector3d(colorData.getDouble("red"), colorData.getDouble("green"), colorData.getDouble("blue"));
        this.color = colorFromData;

        this.scale = compoundNBT.getFloat("scale");
        this.rayLength = compoundNBT.getFloat("ray_length");
        this.getDataManager().set(RAY_LENGTH, rayLength);
    }

    @Override
    public void writeAdditional(CompoundNBT compoundNBT) {
        super.writeAdditional(compoundNBT);
        compoundNBT.putFloat("damage", damage);
        
        CompoundNBT colorData = this.getDataManager().get(COLOR);
        colorData = this.setColorNBT(colorData, this.color);
        compoundNBT.put("color", colorData);
        
        compoundNBT.putFloat("scale", scale);
        this.rayLength = getDataManager().get(RAY_LENGTH);
        compoundNBT.putFloat("ray_length", this.rayLength);
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected float getGravityVelocity() {
        return 0.0F;
    }

    @Override
    public void tick() {
        super.tick();
        double speed = new Vector3d(getPosX(), getPosY(), getPosZ()).distanceTo(new Vector3d(prevPosX, prevPosY, prevPosZ));
       
        if (!this.world.isRemote) {
        	if (ticksExisted > 600 || speed < 0.01D) {
        		this.remove(); //This removes the laser if it goes too far away
        	}
        }
        if (isAlive()) {
            super.tick();
        }
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result == null || !isAlive())
            return;

        //Entity Hit
        if (result.getType() == RayTraceResult.Type.ENTITY) {
            EntityRayTraceResult entityHitResult = ((EntityRayTraceResult) result);
            if (entityHitResult.getEntity() == this.getShooter() || entityHitResult == null) return;
            Entity hitEntity = entityHitResult.getEntity();
            if (weapon != null)
                weapon.onHitEntityPre(this, hitEntity);
            hitEntity.attackEntityFrom(source, weapon == null ? damage : weapon.damage());
        }
        //Block Hit
        else if (result.getType() == RayTraceResult.Type.BLOCK) {
            BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
            BlockPos pos = blockResult.getPos();
            BlockState block = world.getBlockState(pos);
            if (weapon != null)
                weapon.onHitBlockPre(this, block);
            if (block.getBlock() instanceof TNTBlock) {
                world.removeBlock(pos, false);
                TNTEntity tntEntity = new TNTEntity(world, (float) pos.getX() + 0.5F, pos.getY(), (float) pos.getZ() + 0.5F, (LivingEntity) getShooter());
                tntEntity.setFuse(0);
                world.addEntity(tntEntity);
                world.playSound(null, tntEntity.getPosX(), tntEntity.getPosY(), tntEntity.getPosZ(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
                if (world.isRemote()) {
                    world.addParticle(ParticleTypes.SMOKE, true, this.getPosX(), this.getPosY(), this.getPosZ(), 0, 0, 0);
                }
            }
        }

        if (this.weapon != null) {
            this.weapon.onHit(this, result);
        }

        if (!this.world.isRemote) {
            this.remove();
        }
    }
    
    public void setWeaponType(AbstractWeapon<? extends LivingEntity> weap) {
        this.weapon = weap;
    }

    public void setColor(Vector3d color) {
        this.color = color;
        CompoundNBT colorData = this.getDataManager().get(COLOR);
        this.getDataManager().set(COLOR, this.setColorNBT(colorData, this.color));
    }

    public Vector3d getColor() {
    	CompoundNBT colorData = this.getDataManager().get(COLOR);
    	Vector3d colorFromData = new Vector3d(colorData.getDouble("red"), colorData.getDouble("green"), colorData.getDouble("blue"));
        this.color = colorFromData;
    	return color;
    }
    
    public CompoundNBT setColorNBT(CompoundNBT input, Vector3d color) {
    	input.putDouble("red", color.x);
    	input.putDouble("green", color.y);
        input.putDouble("blue", color.z);
        return input;
    }

    public void setSource(DamageSource source) {
        this.source = source;
    }

    public DamageSource getSource() {
        return source;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getRayLength() {
        return this.getDataManager().get(RAY_LENGTH).floatValue();
    }
    
    public void setRayLength(float length) {
    	this.rayLength = length;
    	this.getDataManager().set(RAY_LENGTH, this.rayLength);
    }

}