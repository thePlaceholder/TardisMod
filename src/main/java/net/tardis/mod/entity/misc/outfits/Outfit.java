package net.tardis.mod.entity.misc.outfits;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Predicate;

public abstract class Outfit {

    private final Predicate<LivingEntity> shouldBeWearing;
    private final EnumMap<EquipmentSlotType, Item> slotMap = new EnumMap<>(EquipmentSlotType.class);

    public Outfit(Predicate<LivingEntity> shouldBeWearing){
        this.shouldBeWearing = shouldBeWearing;
        this.registerItems();
    }

    public abstract void registerItems();

    public void addItem(EquipmentSlotType slot, Item item){
        this.slotMap.put(slot, item);
    }

    public void tickServer(LivingEntity entity){
        if(this.shouldBeWearing.test(entity))
            equip(entity);
        else unequip(entity);
    }

    public void equip(LivingEntity entity){
        for(Map.Entry<EquipmentSlotType, Item> entry : slotMap.entrySet()){
            if(entity.getItemStackFromSlot(entry.getKey()).getItem() != entry.getValue())
                entity.setItemStackToSlot(entry.getKey(), new ItemStack(entry.getValue()));
        }
    }

    public void unequip(LivingEntity entity){
        for(Map.Entry<EquipmentSlotType, Item> slot : slotMap.entrySet()){
            if(entity.getItemStackFromSlot(slot.getKey()).getItem() == slot.getValue())
                entity.setItemStackToSlot(slot.getKey(), ItemStack.EMPTY);
        }
    }

}
