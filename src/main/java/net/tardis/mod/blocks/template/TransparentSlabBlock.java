package net.tardis.mod.blocks.template;

import net.minecraft.block.BlockState;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.SoundType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.ars.IARS;

public class TransparentSlabBlock extends SlabBlock implements IARS {

    public TransparentSlabBlock(Properties prop, SoundType sound, float hardness, float resistance) {

        super(prop.sound(sound).hardnessAndResistance(hardness, resistance).setSuffocates((state, reader, pos) -> false));
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

}
