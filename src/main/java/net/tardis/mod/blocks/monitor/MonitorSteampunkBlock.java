package net.tardis.mod.blocks.monitor;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.helper.VoxelShapeUtils;

public class MonitorSteampunkBlock extends MonitorBlock {

    public static final VoxelShape NORTH = createVoxelShape();
    public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
    public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);

    public static final VoxelShape HANGING_NORTH = createHangingVoxelShape();
    public static final VoxelShape HANGING_EAST = VoxelShapeUtils.rotate(createHangingVoxelShape(), Rotation.CLOCKWISE_90);
    public static final VoxelShape HANGING_SOUTH = VoxelShapeUtils.rotate(createHangingVoxelShape(), Rotation.CLOCKWISE_180);
    public static final VoxelShape HANGING_WEST = VoxelShapeUtils.rotate(createHangingVoxelShape(), Rotation.COUNTERCLOCKWISE_90);

    public MonitorSteampunkBlock(Properties prop, int guiID, double x, double y, double z, double width, double height) {
        super(prop, guiID, x, y, z, width, height);
    }

    public static VoxelShape createVoxelShape() {
        VoxelShape shape = VoxelShapes.create(0.03125, 0.515625, 0.171875, 0.96875, 0.578125, 0.484375);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.371875, 0.478125, 0.96875, 0.771875, 0.615625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.15625, 0.125, 0.96875, 0.96875, 0.25), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.603125, 0.528125, 0.96875, 0.915625, 0.596875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.21875, 0.371875, 0.603125, 0.7875, 0.753125, 0.753125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.28125, 0.415625, 0.74375, 0.73125, 0.709375, 0.89375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.40625, 0.478125, 0.884375, 0.60625, 0.646875, 0.909375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0375, 0.471875, 0.603125, 0.9875, 0.671875, 0.771875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.3125, 0.003125, 0.309375, 0.7, 0.103125, 0.690625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.425, 0.103125, 0.553125, 0.56875, 0.259375, 0.884375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.125, 0.103125, 0.759375, 0.8875, 0.234375, 0.853125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.825, 0.228125, 0.759375, 0.8875, 0.359375, 0.853125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.825, 0.271875, 0.665625, 0.8875, 0.528125, 0.759375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.125, 0.271875, 0.665625, 0.1875, 0.528125, 0.759375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.125, 0.228125, 0.759375, 0.1875, 0.359375, 0.853125), IBooleanFunction.OR);
        return shape;
    }

    public static VoxelShape createHangingVoxelShape() {
        VoxelShape shape = VoxelShapes.create(0.03125, 0.3125, 0.140625, 0.96875, 0.5, 0.421875);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.328125, 0.421875, 0.96875, 0.953125, 0.609375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.40625, 0.578125, 0.859375, 0.59375, 0.828125, 0.921875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.28125, 0.9375, 0.296875, 0.71875, 1.0, 0.734375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.640625, 0.03125, 0.96875, 0.765625, 0.15625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.625, 0.390625, 0.96875, 0.875, 0.421875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.390625, 0.609375, 0.96875, 0.890625, 0.796875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.28125, 0.453125, 0.609375, 0.71875, 0.953125, 0.859375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.21875, 0.328125, 0.609375, 0.78125, 0.390625, 0.734375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.171875, 0.21875, 0.96875, 0.3125, 0.34375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.046875, 0.28125, 0.96875, 0.171875, 0.40625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.5, 0.078125, 0.96875, 0.640625, 0.203125), IBooleanFunction.OR);
        return shape;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        boolean isHanging = state.get(BlockStateProperties.HANGING);
        switch (state.get(BlockStateProperties.HORIZONTAL_FACING)) {
            case EAST:
                return isHanging ? HANGING_EAST : EAST;
            case SOUTH:
                return isHanging ? HANGING_SOUTH : SOUTH;
            case WEST:
                return isHanging ? HANGING_WEST : WEST;
            default:
                return isHanging ? HANGING_NORTH : NORTH;
        }
    }

}
