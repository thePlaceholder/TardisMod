package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.helper.TextHelper;

public class ItemAccessPanelBlock extends NotSolidTileBlock {
	private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.item_access_panel.description_one"));
	private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.item_access_panel.description_two"));
    
    public ItemAccessPanelBlock(Properties properties) {
        super(properties);
        this.setShowTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

	@Override
	public void createDescriptionTooltips(ItemStack stack, IBlockReader world, List<ITextComponent> tooltip,
	        ITooltipFlag flag) {
	    tooltip.add(descriptionTooltip);
	    tooltip.add(descriptionTooltipTwo);
	}
}
