package net.tardis.mod.commands.subcommands;

import java.util.HashSet;
import java.util.Set;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.ILocationArgument;
import net.minecraft.command.arguments.Vec3Argument;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;

public class RiftCommand extends TCommand{
    
    private static int findRift(CommandSource source, ServerWorld sWorld, ILocationArgument searchPos, int chunkRadius) {
        return findRift(source, sWorld, searchPos.getBlockPos(source), chunkRadius);
    }
    
    private static int findRift(CommandSource source, ServerWorld sWorld, BlockPos searchPos, int chunkRadius) {
    	TextComponent riftStartPosText = TextHelper.createTextComponentWithTipSuggest(WorldHelper.formatBlockPosDebug(searchPos).toString(), "/tp @p " + WorldHelper.formatBlockPosDebug(searchPos).toString());
    	TextComponent riftPositionsText = new StringTextComponent("");
    	riftPositionsText.modifyStyle(style -> {
			style.setFormatting(TextFormatting.WHITE);
			style.setHoverEvent(null);
			style.setClickEvent(null);
			return style;
		});
    	Set<BlockPos> riftPositions = new HashSet<>();
    	for (int x = 0 ; x < chunkRadius; ++x) {
    		for (int z = 0 ; z < chunkRadius; ++z) {
        		ChunkPos cPos = new ChunkPos(searchPos);
        		int newChunkX = cPos.x + x;
        		int newChunkZ = cPos.z + z;
        		ChunkPos newCPos = new ChunkPos(newChunkX, newChunkZ);
        		Chunk c = sWorld.getChunk(newCPos.x, newCPos.z);
        		if (c.getCapability(Capabilities.RIFT).isPresent()) {
        			c.getCapability(Capabilities.RIFT).ifPresent(rift ->{
        	            if(rift.isRift()) {
        	            	BlockPos foundPos = new BlockPos(newCPos.getXStart(), searchPos.getY(), newCPos.getZStart());
        	            	riftPositions.add(foundPos);
        	            }
        	        });
                }
    		}
    	}
    	for (BlockPos pos : riftPositions) {
    		TextComponent riftPos = TextHelper.createTextComponentWithTip(WorldHelper.formatBlockPosDebug(pos).toString(), "/tp @p " + WorldHelper.formatBlockPosDebug(pos).toString());
        	TextComponent riftPositionString = new StringTextComponent("\n");
        	riftPositionsText.appendSibling(riftPos).appendSibling(riftPositionString); 
    	}
    	source.sendFeedback(new TranslationTextComponent("command.tardis.rift.find.success", chunkRadius, riftStartPosText, riftPositionsText), true);
        return Command.SINGLE_SUCCESS;
    }
    
    private static int getRiftEnergy(CommandSource source, ServerWorld sWorld, ILocationArgument searchPos) {
        return getRiftEnergy(source, sWorld, searchPos.getBlockPos(source));
    }
    
    private static int getRiftEnergy(CommandSource source, ServerWorld sWorld, BlockPos searchPos) {
    	TextComponent riftPosText = TextHelper.createTextComponentWithTip(WorldHelper.formatBlockPosDebug(searchPos).toString(), "/tp @p " + WorldHelper.formatBlockPosDebug(searchPos).toString());
        Chunk c = sWorld.getChunkAt(searchPos);
        if (c.getCapability(Capabilities.RIFT).isPresent()) {
            c.getCapability(Capabilities.RIFT).ifPresent(rift ->{
                if(rift.isRift()) { 
                     source.sendFeedback(new TranslationTextComponent("command.tardis.rift.get_energy.success", riftPosText, rift.getRiftEnergy()), true);    
                }
                else {
                	source.sendErrorMessage(new TranslationTextComponent("command.tardis.rift.find.error", riftPosText));
                }
            });
            return Command.SINGLE_SUCCESS;
        }
        else {
        	source.sendErrorMessage(new TranslationTextComponent("command.tardis.rift.find.error", riftPosText));
            return 0;
        }
    }
    
    private static int addRiftEnergy(CommandSource source, ServerWorld sWorld, float addAmount, ILocationArgument searchPos) {
        return addRiftEnergy(source, sWorld, addAmount, searchPos.getBlockPos(source));
    }
    
    private static int addRiftEnergy(CommandSource source, ServerWorld sWorld, float addAmount, BlockPos searchPos) {
        TextComponent riftPosText = TextHelper.createTextComponentWithTip(WorldHelper.formatBlockPosDebug(searchPos).toString(), "/tp @p " + WorldHelper.formatBlockPosDebug(searchPos).toString());
        Chunk c = sWorld.getChunkAt(searchPos);
        if (c.getCapability(Capabilities.RIFT).isPresent()) {
            c.getCapability(Capabilities.RIFT).ifPresent(rift ->{
                if(rift.isRift()) {
                    rift.addEnergy(addAmount);
                    source.sendFeedback(new TranslationTextComponent("command.tardis.rift.add_energy.success", addAmount, riftPosText, rift.getRiftEnergy()), true);    
                }
                else {
                	source.sendErrorMessage(new TranslationTextComponent("command.tardis.rift.find.error", riftPosText));
                }
            });
            return Command.SINGLE_SUCCESS;
        }
        else {
        	source.sendErrorMessage(new TranslationTextComponent("command.tardis.rift.find.error", riftPosText));
            return 0;
        }
    }
    
    private static int createOrRemoveRift(CommandSource source, ServerWorld sWorld, ILocationArgument searchPos, boolean create) {
        return createOrRemoveRift(source, sWorld, searchPos.getBlockPos(source), create);
    }
    
    private static int createOrRemoveRift(CommandSource source, ServerWorld sWorld, BlockPos searchPos, boolean create) {
    	TextComponent riftPosText = TextHelper.createTextComponentWithTip(WorldHelper.formatBlockPosDebug(searchPos).toString(), "/tp @p " + WorldHelper.formatBlockPosDebug(searchPos).toString());
        Chunk c = sWorld.getChunkAt(searchPos);
        if (c.getCapability(Capabilities.RIFT).isPresent()) {
            c.getCapability(Capabilities.RIFT).ifPresent(rift ->{
                rift.setRift(create);
                String feedbackTranslationKey = create ? "command.tardis.rift.create_rift.success" : "command.tardis.rift.remove_rift.success";
                source.sendFeedback(new TranslationTextComponent(feedbackTranslationKey, rift.getRiftEnergy(), riftPosText), true);    
            });
            return Command.SINGLE_SUCCESS;
        }
        else {
        	source.sendErrorMessage(new TranslationTextComponent("command.tardis.rift.find.error", riftPosText));
            return 0;
        }
    }
    
    
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher){
        return Commands.literal("rift").requires(context -> context.hasPermissionLevel(2))
                .then(Commands.literal("find")
                    .then(Commands.argument("start_location", Vec3Argument.vec3())
                    		.executes(context -> findRift(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "start_location"), 10)
                                 )//End execute at specified position
                    		.then(Commands.argument("chunk_radius", IntegerArgumentType.integer(1, 20))
                    		    .executes(context -> findRift(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "start_location"), IntegerArgumentType.getInteger(context, "chunk_radius"))
                                    )
                                 ) //End chunk radius integer argument
                         )//End location argument
                   )//End find argument
                .then(Commands.literal("create")
                    .then(Commands.argument("location", Vec3Argument.vec3())
                    		.executes(context -> createOrRemoveRift(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "location"), true)
                                )//End execute at specified position
                         )//End location argument
                )//End create rift argument
                .then(Commands.literal("remove")
                        .then(Commands.argument("location", Vec3Argument.vec3())
                        		.executes(context -> createOrRemoveRift(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "location"), false)
                                    )//End execute at specified position
                             )//End location argument
                )//End remove rift argument
                .then(Commands.literal("get_energy")
	                .then(Commands.argument("location", Vec3Argument.vec3())
	                		.executes(context -> getRiftEnergy(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "location"))
                                )//End execute at specified position
	                     )//End location argument
	            )//End get energy argument
                .then(Commands.literal("add_energy")
    	                .then(Commands.argument("location", Vec3Argument.vec3())
    	                		.then(Commands.argument("amount", FloatArgumentType.floatArg())
	                		      .executes(context -> addRiftEnergy(context.getSource(), context.getSource().asPlayer().getServerWorld(), FloatArgumentType.getFloat(context, "amount"), Vec3Argument.getLocation(context, "location"))
                                )//End execute at specified position
	                     )//End amount argument
    	            )//End location argument
    	        ); //End add energy
                
    }

}
