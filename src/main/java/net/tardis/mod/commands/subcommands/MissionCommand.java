package net.tardis.mod.commands.subcommands;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.ILocationArgument;
import net.minecraft.command.arguments.Vec3Argument;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.commands.argument.MissionStageArgument;
import net.tardis.mod.commands.argument.MissionTypeArgument;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.missions.stages.MissionStage;

public class MissionCommand extends TCommand{
    
    private static boolean successfulObjectiveUpdate = false;
    private static boolean successfulStageUpdate = false;
    
    private static int spawnMission(CommandSource source, MiniMissionType missionType, ServerWorld sWorld, ILocationArgument startPos) {
        return spawnMission(source, missionType, sWorld, startPos.getBlockPos(source));
    }
    
    private static int spawnMission(CommandSource source, MiniMissionType missionType, ServerWorld sWorld, BlockPos startPos) {
        if (missionType != null) {
            if (sWorld.getCapability(Capabilities.MISSION).isPresent()) {
                Structure<?> missionStructure = sWorld.getServer().getDynamicRegistries().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getOrThrow(missionType.getStructureKey()).field_236268_b_;
                BlockPos spawnPos = missionType.spawnAndFindNearestMission(sWorld, sWorld.getStructureManager(), startPos, 5000, true, sWorld.getSeed(), sWorld.getChunkProvider().getChunkGenerator().func_235957_b_().func_236197_a_(missionStructure));
                TextComponent spawnedPosText = TextHelper.createTextWithoutTooltip("/tp @p " + WorldHelper.formatBlockPosDebug(spawnPos).toString());
                if (!spawnPos.equals(BlockPos.ZERO)) {
                    source.sendFeedback(new TranslationTextComponent("command.tardis.mission.spawn.success", missionType.getRegistryName(), spawnedPosText), false);
                    return Command.SINGLE_SUCCESS;
                }
                else {
                    source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.spawn.fail", missionType.getRegistryName(), spawnedPosText));
                    return 0;
                }
            }
            else {
                source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.invalid_mission_world", sWorld.getDimensionKey().getLocation()));
                return 0;
            }
            
        }
        else {
            source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.invalid_mission"));
            return 0;    
        }
    }
    
    private static int getMissionStage(CommandSource source, ServerWorld sWorld, ILocationArgument missionPos) {
        return getMissionStage(source, sWorld, missionPos.getBlockPos(source));
    }
    
    private static int getMissionStage(CommandSource source, ServerWorld sWorld, BlockPos missionPos) {
        TextComponent spawnedPosText = TextHelper.createTextWithoutTooltip(WorldHelper.formatBlockPosDebug(missionPos).toString());
        if (sWorld.getCapability(Capabilities.MISSION).isPresent()) {
            sWorld.getCapability(Capabilities.MISSION).ifPresent(cap -> {
                MiniMission mission = cap.getMissionForPos(missionPos);
                if (mission != null) {
                    MissionStage currentStage = mission.getCurrentStage();
                    source.sendFeedback(new TranslationTextComponent("command.tardis.mission.stage_get.success", mission.getType().getRegistryName(), spawnedPosText, currentStage.getRegistryName().toString()), false);
                }
                else {
                    source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.no_mission_found_at_pos", spawnedPosText));
                }
            });
            return Command.SINGLE_SUCCESS;
        }
        else {
            source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.invalid_mission_world", sWorld.getDimensionKey().getLocation()));
            return 0;    
        }
    }
    
    private static int setMissionStage(CommandSource source, ServerWorld sWorld, ILocationArgument missionPos, MissionStage stage) {
        return setMissionStage(source, sWorld, missionPos.getBlockPos(source), stage);
    }
    
    private static int setMissionStage(CommandSource source, ServerWorld sWorld, BlockPos missionPos, MissionStage stage) {
        TextComponent spawnedPosText = TextHelper.createTextWithoutTooltip(WorldHelper.formatBlockPosDebug(missionPos).toString());
        if (sWorld.getCapability(Capabilities.MISSION).isPresent()) {
            sWorld.getCapability(Capabilities.MISSION).ifPresent(cap -> {
                MiniMission mission = cap.getMissionForPos(missionPos);
                if (mission != null) {
                    mission.setCurrentStage(stage);
                    mission.setObjective(0);
                    mission.setAwarded(false);
                    source.sendFeedback(new TranslationTextComponent("command.tardis.mission.stage_update.success", stage.getRegistryName().toString(), mission.getType().getRegistryName(), spawnedPosText), false);
                    successfulStageUpdate = true;
                }
                else {
                    successfulStageUpdate = false;
                    source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.no_mission_found_at_pos", spawnedPosText));
                }
            });
            if (successfulStageUpdate) {
                return Command.SINGLE_SUCCESS;
            }
            else {
                return 0;
            }
        }
        else {
            source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.invalid_mission_world", sWorld.getDimensionKey().getLocation()));
            return 0;    
        }
    }
    
    private static int getMissionObjective(CommandSource source, ServerWorld sWorld, ILocationArgument missionPos) {
        return getMissionObjective(source, sWorld, missionPos.getBlockPos(source));
    }
    
    private static int getMissionObjective(CommandSource source, ServerWorld sWorld, BlockPos missionPos) {
        return getMissionObjective(source, sWorld, missionPos, getCurrentMissionStage(sWorld, missionPos));
    }
    
    private static int getMissionObjective(CommandSource source, ServerWorld sWorld, BlockPos missionPos, MissionStage stage) {
        TextComponent spawnedPosText = TextHelper.createTextWithoutTooltip(WorldHelper.formatBlockPosDebug(missionPos).toString());
        if (sWorld.getCapability(Capabilities.MISSION).isPresent()) {
            sWorld.getCapability(Capabilities.MISSION).ifPresent(cap -> {
                MiniMission mission = cap.getMissionForPos(missionPos);
                if (mission != null) {
                    MissionStage stageFound = mission.getStageByKey(stage.getRegistryName());
                    if (stageFound != null) {
                        int objectiveIndex = mission.getStageByKey(stage.getRegistryName()).getCurrentObjectiveIndex();
                        source.sendFeedback(new TranslationTextComponent("command.tardis.mission.objective_get.success", mission.getType().getRegistryName(), spawnedPosText, objectiveIndex, stage.getRegistryName().toString()), false);
                    }
                    else {
                        source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.no_stage_found_for_mission", mission.getType().getRegistryName(), spawnedPosText));
                    }
                }
                else {
                    source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.no_mission_found_at_pos", spawnedPosText));
                }
            });
            return Command.SINGLE_SUCCESS;
        }
        else {
            source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.invalid_mission_world", sWorld.getDimensionKey().getLocation()));
            return 0;    
        }
    }
    
    private static int setMissionObjective(CommandSource source, ServerWorld sWorld, ILocationArgument missionPos, MissionStage stage, int objective) {
        return setMissionObjective(source, sWorld, missionPos.getBlockPos(source), stage, objective);
    }
    
    private static int setMissionObjective(CommandSource source, ServerWorld sWorld, BlockPos missionPos, MissionStage stage, int objective) {
        TextComponent spawnedPosText = TextHelper.createTextWithoutTooltip(WorldHelper.formatBlockPosDebug(missionPos).toString());
        if (sWorld.getCapability(Capabilities.MISSION).isPresent()) {
            sWorld.getCapability(Capabilities.MISSION).ifPresent(cap -> {
                MiniMission mission = cap.getMissionForPos(missionPos);
                if (mission != null) {
                    mission.setObjectiveForStage(stage.getRegistryName(), objective);
                    mission.setAwarded(false);
                    source.sendFeedback(new TranslationTextComponent("command.tardis.mission.objective_update.success", stage.getRegistryName().toString(), mission.getType().getRegistryName(), spawnedPosText, mission.getCurrentObjective()), false);
                    successfulObjectiveUpdate = true;
                }
                else {
                    successfulObjectiveUpdate = false;
                    source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.no_mission_found_at_pos", spawnedPosText));
                }
            });
            if (successfulObjectiveUpdate) {
                return Command.SINGLE_SUCCESS;
            }
            else {
                return 0;
            }
        }
        else {
            source.sendErrorMessage(new TranslationTextComponent("command.tardis.mission.invalid_mission_world", sWorld.getDimensionKey().getLocation()));
            return 0;    
        }
    }
    
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("mission")
                    .requires(context -> context.hasPermissionLevel(2))
                    .then(Commands.literal("spawn")
                            .then(Commands.argument("type", MissionTypeArgument.getMissionTypeArgument())
                                    .executes(context -> spawnMission(context.getSource() ,MissionTypeArgument.getMissionType(context, "type"), context.getSource().asPlayer().getServerWorld(), context.getSource().asPlayer().getPosition())
                                 )//End spawn at player position
                                 .then(Commands.argument("location", Vec3Argument.vec3())
                                     .executes(context -> spawnMission(context.getSource(), MissionTypeArgument.getMissionType(context, "type"), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "location"))
                                     )//End execute at specified position
                                 )//End location argument
                             )//End mission type argument
                    )//End spawn argument
                    .then(Commands.literal("get")
                            .then(Commands.literal("objective")
                                    .executes(context -> getMissionObjective(context.getSource(), context.getSource().asPlayer().getServerWorld(), context.getSource().asPlayer().getPosition(), getCurrentMissionStage(context.getSource().asPlayer().getServerWorld(), context.getSource().asPlayer().getPosition()))
                                        )
                                    .then(Commands.argument("mission_location", Vec3Argument.vec3())
                                            .executes(context -> getMissionObjective(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "mission_location"))
                                                )
                                            .then(Commands.argument("stage_type", MissionStageArgument.getMissionStageArgument()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggestIterable(getMissionStagesFromMission(context.getSource(), context.getSource().getWorld(), Vec3Argument.getLocation(context, "mission_location")), suggestionBuilder))
                                                    .executes(context -> getMissionObjective(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "mission_location").getBlockPos(context.getSource()), MissionStageArgument.getMissionStage(context, "stage_type"))
                                                            )
                                                )
                                        )
                                )//End get objective
                            .then(Commands.literal("stage")
                                    .executes(context -> getMissionStage(context.getSource(), context.getSource().asPlayer().getServerWorld(), context.getSource().asPlayer().getPosition())
                                            )
                                    .then(Commands.argument("mission_location", Vec3Argument.vec3())
                                            .executes(context -> getMissionStage(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "mission_location"))
                                                )
                                        )//End mission location
                                )//End get stage
                        )
                    .then(Commands.literal("set")
                        .then(Commands.literal("objective")
                            .then(Commands.argument("mission_location", Vec3Argument.vec3())
                                    .then(Commands.argument("stage", MissionStageArgument.getMissionStageArgument()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggestIterable(getMissionStagesFromMission(context.getSource(), context.getSource().getWorld(), Vec3Argument.getLocation(context, "mission_location")), suggestionBuilder))
                                        .then(Commands.argument("objective_level", IntegerArgumentType.integer())
                                            .executes(context -> setMissionObjective(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "mission_location"), MissionStageArgument.getMissionStage(context, "stage"), IntegerArgumentType.getInteger(context, "objective_level"))
                                            ) //End objective at player position
                                        )//End objective level argument
                                    )//End stage
                                 )//End location level argument
                            )///End objective set argument
                        .then(Commands.literal("stage")
                            .then(Commands.argument("mission_location", Vec3Argument.vec3())
                                    .then(Commands.argument("stage_type", MissionStageArgument.getMissionStageArgument()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggestIterable(getMissionStagesFromMission(context.getSource(), context.getSource().getWorld(), Vec3Argument.getLocation(context, "mission_location")), suggestionBuilder))
                                            .executes(context -> setMissionStage(context.getSource(), context.getSource().asPlayer().getServerWorld(), Vec3Argument.getLocation(context, "mission_location"), MissionStageArgument.getMissionStage(context, "stage_type"))
                                            ) //End stage at player position
                                    )//End objective level argument
                                )//End location level argument
                            )///End stage set argument
                    );
                   
    }
    
    public static Collection<ResourceLocation> getMissionStagesFromMission(CommandSource source, ServerWorld sWorld, ILocationArgument missionPos){
        return getMissionStagesFromMission(source, sWorld, missionPos.getBlockPos(source));
    }
    
    public static Collection<ResourceLocation> getMissionStagesFromMission(CommandSource source, ServerWorld sWorld, BlockPos missionPos){
        Collection<ResourceLocation> stages = Lists.newArrayList();
        sWorld.getCapability(Capabilities.MISSION).ifPresent(cap -> {
            MiniMission mission = cap.getMissionForPos(missionPos);
            for (MissionStage stage : mission.getStages().values()) {
            	if (stage.getRegistryName() != null) {
            		stages.add(stage.getRegistryName());
            	}
            }
        });
        return stages;
    }
    
    public static MissionStage getCurrentMissionStage(ServerWorld sWorld, BlockPos missionPos) {
        List<MissionStage> stages = Lists.newArrayList();
        sWorld.getCapability(Capabilities.MISSION).ifPresent(cap -> {
            MiniMission mission = cap.getMissionForPos(missionPos);
            stages.add(mission.getCurrentStage());
        });
        return stages.get(0);
    }

}
