package net.tardis.mod.misc;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
/** 
 * Wrapper around an item that will make a Broken Tardis loyal to the player that interacts with the item
 * <br> This object is datapack compatible. Picks up JSON files from data/[namespace]/tardis_likes folder.
 * <p> Format:
 * {
 * "ingredient": [
 *   {
 *    "item":"tardis:psychic_cube" //Can also use "tag" here to support any item tag
 *   }
 * ],
 * "loyalty_weight": 10 //Amount of loyalty that the Tardis will increase for that player
 * }
 * */
public class TardisLike {
    
    private static final Codec<TardisLike> CODEC = RecordCodecBuilder.create(instance ->
    	instance.group(
    	IngredientCodec.INGREDIENT_CODEC.listOf().fieldOf("ingredient").forGetter(TardisLike::getItem),
    	Codec.INT.fieldOf("loyalty_weight").forGetter(TardisLike::getLoyaltyMod)
    	).apply(instance, TardisLike::new)
    );
    
    public static final PrepopulatedCodecListener<TardisLike> DATA_LOADER = new PrepopulatedCodecListener<TardisLike>(
	        "tardis_likes",
	        CODEC,
	        Tardis.LOGGER, 
	        () -> TardisLike.registerCoreEntries());

    private int loyaltyMod = 0;
    private List<Ingredient> ingredients;

    public TardisLike(List<Ingredient> items, int amount) {
        this.loyaltyMod = amount;
        this.ingredients = items;
    }

    public List<Ingredient> getItem() {
        return ingredients;
    }

    public int getLoyaltyMod() {
        return this.loyaltyMod;
    }
    
    public boolean matches(ItemStack stack) {
    	boolean stackInIngredient = false;
    	for (Ingredient ing : this.ingredients) {
	        if (ing.test(stack)) {
	        	stackInIngredient = true;
	        	break;
	        }
	    }
        return stackInIngredient;
    }
    
    private static TardisLike register(Ingredient ingredient, int loyaltyMod, String key) {
    	return register(ingredient, loyaltyMod, Helper.createRL(key));
    }
    
    public static TardisLike register(Ingredient ingredient, int loyaltyMod, ResourceLocation registryName) {
    	TardisLike like = new TardisLike(Lists.newArrayList(ingredient), loyaltyMod);
    	DATA_LOADER.getData().put(registryName, like);
        if (registryName == null)
            throw new NullPointerException();
        return like;
    }
    
    public static Codec<TardisLike> getCodec(){
    	return CODEC;
    }
    
    public static Map<ResourceLocation, TardisLike> registerCoreEntries(){
   	    DATA_LOADER.getData().clear();
   	    Tardis.LOGGER.log(Level.INFO, "Cleared Tardis Like Registry!");
   	    register(Ingredient.fromItems(Items.COMPASS), 5, "compass");
        register(Ingredient.fromItems(Items.FILLED_MAP), 10, "filled_map");
        register(Ingredient.fromItems(Items.ENDER_PEARL), 15, "ender_pearl");
        register(Ingredient.fromItems(Items.CLOCK), 20, "clock");
        register(Ingredient.fromItems(Items.ENDER_EYE), 20, "ender_eye");
        register(Ingredient.fromItems(Items.BELL), 25, "bell");
        Tardis.LOGGER.log(Level.INFO, "Registered default Tardis Likes");
        return DATA_LOADER.getData();
   }
    
    public static Collection<TardisLike> getValues(){
        return DATA_LOADER.getData().values();
    }
}
