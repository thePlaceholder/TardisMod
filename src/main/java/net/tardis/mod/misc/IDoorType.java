package net.tardis.mod.misc;

import java.util.function.Function;
import java.util.function.Supplier;

import net.tardis.mod.client.models.interiordoors.IInteriorDoorRenderer;
import net.tardis.mod.enums.EnumDoorState;

public interface IDoorType {
    /** Get all door states for this interior door. Can be called on both sides*/
    EnumDoorState[] getValidStates();

    /** For client side rendering only. Units are in Degrees */
    double getRotationForState(EnumDoorState state);
    /** Call in FMLClientSetupEvent only */
    void setInteriorDoorModel(IInteriorDoorRenderer renderer);
    /** Only call on the logical CLIENT Side*/
    IInteriorDoorRenderer getInteriorDoorRenderer();

    public static enum EnumDoorType implements IDoorType {

        STEAM((state) -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return 55.0;
                default:
                    return 85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        TRUNK(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return -55.0;
                default:
                    return -85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        TELEPHONE(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return 55.0;
                default:
                    return 85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        POLICE_BOX(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return 90.0; //This returns the rotation value for the right door
                case BOTH:
                    return -90.0; //This returns the rotation value of the left door
                default:
                    return 0.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        MODERN_POLICE_BOX(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return 75.0; //This returns the rotation value for the right door
                case BOTH:
                    return -75.0; //This returns the rotation value of the left door
                default:
                    return 0.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        FORTUNE(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return -55.0;
                default:
                    return -85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        SAFE(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return -55.0;
                default:
                    return -85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        TT_CAPSULE(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return 45.0;
                default:
                    return 85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        CLOCK(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return -55.0;
                default:
                    return -85.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        TT_2020_CAPSULE(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return -30.0;
                default:
                    return -60.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),

        JAPAN(state -> {
            switch (state) {
                case CLOSED:
                    return 0.0;
                case ONE:
                    return -0.20;
                case BOTH:
                    return -0.50;
                default:
                    return 0.0;
            }
        }, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH),
    	
    	APERTURE(state -> {
			switch(state) {
				default: return 0.0;
				case ONE: return 50.0;
				case BOTH: return 50.0;
			}
		}, EnumDoorState.CLOSED, EnumDoorState.ONE, EnumDoorState.BOTH);


        Function<EnumDoorState, Double> func;
        EnumDoorState[] validStates;
        Supplier<Supplier<IInteriorDoorRenderer>> renderer;

        EnumDoorType(Function<EnumDoorState, Double> func, EnumDoorState... states) {
            this.validStates = states;
            this.func = func;
        }

        @Override
        public EnumDoorState[] getValidStates() {
            return this.validStates;
        }

        @Override
        public double getRotationForState(EnumDoorState state) {
            return func.apply(state);
        }

        @Override
        public void setInteriorDoorModel(IInteriorDoorRenderer renderer) {
            this.renderer = () -> () -> renderer;
        }

        @Override
        public IInteriorDoorRenderer getInteriorDoorRenderer() {
            return this.renderer.get().get();
        }

    }
}
