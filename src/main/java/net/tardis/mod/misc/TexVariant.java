package net.tardis.mod.misc;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
/**
 * Wrapper class to allow objects such as Consoles and Exteriors to have texture variants
 * @author Spectre0987
 *
 */
public class TexVariant {

    private ResourceLocation tex;
    private TranslationTextComponent translation;
    private TexVariant interiorDoor;

    public TexVariant(ResourceLocation loc, String translationKey) {
        this.tex = loc;
        this.translation = new TranslationTextComponent("texvar." + loc.getNamespace() + "." + translationKey);
    }

    public TexVariant(String fileName, String translationKey) {
        this(new ResourceLocation(Tardis.MODID, "textures/exteriors/" + fileName + ".png"), translationKey);
    }

    /**
     *
     * @return Interior door texture if one is registered, or just the main location if not
     */
    public ResourceLocation getInteriorDoorTexture() {
    	if(this.interiorDoor != null)
    	    return interiorDoor.getTexture();
    	return this.getTexture();
    }
    
    /**
     * Adds an additional TexVariant to the parent TexVariant.
     * <br> Used for Interior door textures which are different from the Exterior texture.
     * <br> Overload of {@link TexVariant#addInteriorDoorVariant(ResourceLocation)} with string parameter
     * @param fileName - File name for the texture. The rest of the path is automatically constructed to be in assets/tardis/textures/exteriors/interior
     */
    public TexVariant addInteriorDoorVariant(String fileName) {
    	TexVariant t = new TexVariant(new ResourceLocation(Tardis.MODID, "textures/exteriors/interior/" + fileName + ".png"), "");
		this.interiorDoor = t;
		return this;
    }
    /**
     * Adds an additional TexVariant to the parent TexVariant.
     * <br> Used for Interior door textures which are different from the Exterior texture.
     * @param loc - ResourceLocation of the texture, must have file extension name at end of path (Example: .png)
     */
    public TexVariant addInteriorDoorVariant(ResourceLocation loc) {
    	TexVariant t = new TexVariant(loc, "");
    	this.interiorDoor = t;
    	return this;
    }

    public ResourceLocation getTexture() {
        return this.tex;
    }

    public TranslationTextComponent getTranslation() {
        return this.translation;
    }
}
