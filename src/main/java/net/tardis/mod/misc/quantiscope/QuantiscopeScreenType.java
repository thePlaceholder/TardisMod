package net.tardis.mod.misc.quantiscope;

import net.tardis.mod.containers.QuantiscopeContainer;

public enum QuantiscopeScreenType {

    SONIC(new SonicSlotMapper()),
    WELD(null);

    private QuantiscopeSlotMapper mapper;

    QuantiscopeScreenType(QuantiscopeSlotMapper mapper){
        this.mapper = mapper;
    }

    public QuantiscopeSlotMapper getSlotMapper(){
        return this.mapper;
    }

}
