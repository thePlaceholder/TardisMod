package net.tardis.mod.contexts.gui;

import java.util.List;

import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.packets.DiagnosticMessage.ArtronUseInfo;
import net.tardis.mod.network.packets.DiagnosticMessage.ForgeEnergyInfo;
import net.tardis.mod.subsystem.SubsystemInfo;

public class GuiContextDiagnostic extends GuiContext {

	public List<SubsystemInfo> infos;
	public SpaceTimeCoord location;
	public List<ArtronUseInfo> artronInfo;
	public ForgeEnergyInfo energy;
	
	public GuiContextDiagnostic(List<SubsystemInfo> infos, SpaceTimeCoord location, List<ArtronUseInfo> artronInfo, ForgeEnergyInfo energy) {
		this.infos = infos;
		this.location = location;
		this.artronInfo = artronInfo;
		this.energy = energy;
	}
}
