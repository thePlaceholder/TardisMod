package net.tardis.mod.cap;

import net.minecraft.world.World;

public interface ITickableMission {
    void tick(World world);
}
