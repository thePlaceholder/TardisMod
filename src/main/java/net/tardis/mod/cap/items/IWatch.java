package net.tardis.mod.cap.items;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;

public interface IWatch {

    void tick(World world, Entity ent, int itemSlot);

    boolean shouldSpin(Entity ent);
    
    int getVariant();
    void setVariant(int variant);

    /** 1.17: IStorage interface removed with no replacement.*/
    @Deprecated
    public static class Storage implements IStorage<IWatch> {

        @Override
        public INBT writeNBT(Capability<IWatch> capability, IWatch instance, Direction side) {
            return null;
        }

        @Override
        public void readNBT(Capability<IWatch> capability, IWatch instance, Direction side, INBT nbt) {
        }

    }

    public static class Provider implements ICapabilityProvider {

        IWatch watch;

        public Provider(IWatch watch) {
            this.watch = watch;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.WATCH_CAPABILITY ? (LazyOptional<T>) LazyOptional.of(() -> watch) : LazyOptional.empty();
        }

    }
}
