package net.tardis.mod.tileentities.console.misc;

import net.minecraft.entity.EntitySize;
import net.minecraft.util.math.vector.Vector3d;

public class ControlOverride {

	Vector3d position;
	EntitySize size;
	
	public ControlOverride(Vector3d position, EntitySize size) {
		this.position = position;
		this.size = size;
	}

	public Vector3d getPosition() {
		return position;
	}

	public EntitySize getSize() {
		return size;
	}
}
