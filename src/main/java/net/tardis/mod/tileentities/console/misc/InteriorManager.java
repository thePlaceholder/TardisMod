package net.tardis.mod.tileentities.console.misc;

import java.util.List;
import java.util.UUID;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.ChunkPos;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.StopHumMessage;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class InteriorManager implements INBTSerializable<CompoundNBT>, ITickable{
	
	public static ResourceLocation KEY = new ResourceLocation(Tardis.MODID, "interior_manager");
	public static final int resetInteriorChangeProcessTime = 0;
	private ConsoleTile console;
	
	private int light = 0;
	private boolean alarm = false;
	private IAlarmType alarmType = AlarmType.CLOISTER;
	private int interiorCooldownTime = 0;
	private int interiorChangeProcessTime = 0;
	private MonitorOverride monitorOverride;


	public InteriorManager(ConsoleTile console) {
		this.console = console;
		console.registerDataHandler(KEY, this);
		console.registerTicker(this);
	}
	
	public int getLight() {
		return light;
	}
	
	public void setLight(int light) {
		this.light = light;
		
		if(!this.console.getWorld().isRemote()) {
			for(int x = -10; x < 10; ++x) {
				for(int z = -10; z < 10; ++z) {
					ChunkPos start = this.console.getWorld().getChunk(this.console.getPos()).getPos();
					ChunkPos pos = new ChunkPos(start.x + x, start.z + z);
					ILightCap cap = this.console.getWorld().getChunk(pos.x, pos.z).getCapability(Capabilities.LIGHT).orElse(null);
					if(cap != null) {
						cap.setLight(light);
					}
				}
			}
			
			ExteriorTile ext = console.getExteriorType().getExteriorTile(console);
			if(ext != null) {
				ext.setLightLevel(light / 15.0F);
			}
			
		}
	}
	
	public boolean canTurnOnLight() {
		return console.getArtron() > 0;
	}

	public void setAlarmOn(boolean alarm) {
		this.alarm = alarm;
		
		if(alarm)
			this.alarmType = AlarmType.CLOISTER;
		
		console.updateClient();
	}
	
	public void soundAlarm(IAlarmType type) {
		this.setAlarmOn(true);
		this.alarmType = type;
	}
	
	public boolean isAlarmOn() {
		return this.alarm;
	}
	
	public IAlarmType getAlarmType() {
		return this.alarmType;
	}
	
	/** Number of ticks that will take before the Tardis interior change process has completed and allows a player to enter it*/
	public int getInteriorProcessingTime() {
		return this.interiorChangeProcessTime;
	}

	/** Set number of ticks required before the Tardis will finish its interior changing process and allow a player inside its interior*/
	public void setInteriorProcessingTime(int ticks) {
		this.interiorChangeProcessTime = ticks;
		console.updateClient();
	}

	/** If the interior changing is still underway
	 * <p> Use for checks during rendering of special effects or processes that are ticking
	 * <br> E.g. Checking if console should be unloaded, or if the exterior should be rendering smoke particles for interior change
	 * <p> This means the exterior should be double locked*/
	public boolean isInteriorStillRegenerating() {
		return this.interiorChangeProcessTime > 0;
	}

	/** If we can change interior again*/
	public boolean canChangeInteriorAgain() {
		return this.interiorCooldownTime == 0;
	}
	
	public void setInteriorCooldownTime(int ticks) {
		this.interiorCooldownTime = ticks;
	}
	
	public int getInteriorChangeTime() {
		return this.interiorCooldownTime;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("light", light);
		tag.putBoolean("alarm", alarm);
		tag.putInt("interior_process_time", this.interiorChangeProcessTime);
		tag.putInt("interior_cooldown_time", this.interiorCooldownTime);
		
		if(this.monitorOverride != null) {
			tag.put("override", this.monitorOverride.serializeNBT());
		}
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.light = nbt.getInt("light");
		this.alarm = nbt.getBoolean("alarm");
		this.interiorChangeProcessTime = nbt.getInt("interior_process_time");
		this.interiorCooldownTime = nbt.getInt("interior_cooldown_time");
		if(nbt.contains("override"))
			this.monitorOverride = new MonitorOverride(nbt.getCompound("override"));
	}

	@Override
	public void tick(ConsoleTile console) {
		
		if(!console.getWorld().isRemote && this.getAlarmType() != null && this.isAlarmOn() && console.getWorld().getGameTime() % this.getAlarmType().getLoopType() == 0) {
			console.getWorld().playSound(null, console.getPos(), this.alarmType.getLoopSound(), SoundCategory.BLOCKS, 1F, 0.5F);
		}
		
		if(this.monitorOverride != null) {
			if(this.monitorOverride.shouldRemove(console)) {
				this.monitorOverride = null;
			}
		}

		if (!console.getWorld().isRemote() && console.shouldStartChangingInterior()) {
			if (console.canStartToChangeInterior()) {
				if (this.interiorChangeProcessTime > 0) { //If the console is undergoing interior change and the processing time has been set
					--this.interiorChangeProcessTime;
				}
				if (this.interiorChangeProcessTime == 0) {
					console.handleInteriorChangeComplete();
				}
			}
		}
		if(this.interiorCooldownTime > 0)
			--this.interiorCooldownTime;
		
		
	}
	
	public MonitorOverride getMonitorOverrides() {
		return this.monitorOverride;
	}
	
	public void setMonitorOverrides(MonitorOverride override) {
		this.monitorOverride = override;
		this.console.updateClient();
	}

}
