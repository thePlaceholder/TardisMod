package net.tardis.mod.tileentities.console.misc;

public enum SparkingLevel {

	NONE,
	SMOKE,
	SPARKS;

	public static SparkingLevel getFromIndex(int index) {
		return values()[index];
	}
}
