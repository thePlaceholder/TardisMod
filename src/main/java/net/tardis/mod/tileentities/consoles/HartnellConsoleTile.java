package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class HartnellConsoleTile extends ConsoleTile{

	public AxisAlignedBB render;
	
	public HartnellConsoleTile(TileEntityType<?> type) {
		super(type);
		render = new AxisAlignedBB(this.getPos()).grow(2);
	}
	
	public HartnellConsoleTile() {
		this(TTiles.CONSOLE_HARTNEL.get());
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).grow(3);
	}

}
