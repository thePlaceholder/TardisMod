package net.tardis.mod.tileentities.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.Tardis;

public class PanelInventory implements IItemHandlerModifiable, INBTSerializable<ListNBT>{

	private ItemStackHandler handler;
	private Direction dir;
	
	public PanelInventory(Direction dir) {
		this(dir, 9);
	}
	
	public PanelInventory(Direction dir, int size) {
		this.dir = dir;
		this.handler = new ItemStackHandler(size);
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return index < handler.getSlots() ? handler.getStackInSlot(index) : ItemStack.EMPTY;
	}

	@Override
	public ListNBT serializeNBT() {
		ListNBT tag = new ListNBT();
		for(int i = 0; i < this.handler.getSlots(); i++) {
			ItemStack stack = this.handler.getStackInSlot(i);
			tag.add(stack.serializeNBT());
		}
		return tag;
	}

	@Override
	public void deserializeNBT(ListNBT nbt) {
		int index = 0;
		for(INBT tag : nbt) {
			this.handler.setStackInSlot(index, (ItemStack.read((CompoundNBT)tag)));
			++index;
		}
	}
	
	public Direction getPanelDirection() {
		return this.dir;
	}


	public ITextComponent getName() {
		return new TranslationTextComponent("container." + Tardis.MODID + ".engine." + dir.getName2().toLowerCase());
	}

	@Override
	public int getSlots() {
		return this.handler.getSlots();
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		return this.handler.insertItem(slot, stack, simulate);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		return this.handler.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot) {
		return this.handler.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		return true;
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		this.handler.setStackInSlot(slot, stack);
	}
	
	public ItemStackHandler getHandler() {
		return this.handler;
	}
}
