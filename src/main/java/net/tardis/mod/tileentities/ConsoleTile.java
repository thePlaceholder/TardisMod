package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.Explosion;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.blocks.exteriors.ExteriorBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Translations;
import net.tardis.mod.controls.AbstractControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.flight.TardisCollideInstigate;
import net.tardis.mod.flight.TardisCollideRecieve;
import net.tardis.mod.helper.BlockPosHelper;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.ArtronCapacitorItem;
import net.tardis.mod.misc.CrashType;
import net.tardis.mod.misc.CrashTypes;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.network.packets.console.ConsoleData;
import net.tardis.mod.network.packets.console.CrashData;
import net.tardis.mod.network.packets.console.DataTypes;
import net.tardis.mod.network.packets.console.Fuel;
import net.tardis.mod.network.packets.console.NavComData;
import net.tardis.mod.network.packets.exterior.DoorData;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.registries.FlightEventRegistry;
import net.tardis.mod.registries.MissionRegistry;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.registries.SubsystemRegistry;
import net.tardis.mod.registries.TardisStatistics;
import net.tardis.mod.registries.UpgradeRegistry;
import net.tardis.mod.sounds.AbstractSoundScheme;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.subsystem.FluidLinksSubsystem;
import net.tardis.mod.subsystem.NavComSubsystem;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.tags.TardisBlockTags;
import net.tardis.mod.tileentities.console.misc.AlarmType;
import net.tardis.mod.tileentities.console.misc.ArtronUse;
import net.tardis.mod.tileentities.console.misc.ArtronUse.ArtronType;
import net.tardis.mod.tileentities.console.misc.ArtronUse.IArtronType;
import net.tardis.mod.tileentities.console.misc.ControlOverride;
import net.tardis.mod.tileentities.console.misc.DistressSignal;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.console.misc.ExteriorPropertyManager;
import net.tardis.mod.tileentities.console.misc.InteriorManager;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;
import net.tardis.mod.tileentities.console.misc.UnlockManager;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.traits.IAffectLandingTrait;
import net.tardis.mod.traits.TardisTrait;
import net.tardis.mod.upgrades.Upgrade;
import net.tardis.mod.upgrades.UpgradeEntry;
import net.tardis.mod.world.dimensions.TDimensions;

public class ConsoleTile extends TileEntity implements ITickableTileEntity{
    
    private static final AxisAlignedBB CONRTROL_HITBOX = new AxisAlignedBB(-1, 0, -1, 2, 2, 2);
    public static final int TARDIS_MAX_SPEED = 10;
    /** Ten AU a tick at max */
    public static final float BASIC_FUEL_USEAGE = 1;
    public static Random rand = new Random();
    public int prevFlightTicks = 0;
    public int flightTicks = 0;
    private int reachDestinationTick = 0;
    private EmotionHandler emotionHandler;
    private InteriorManager interiorManager;
    private List<ITickable> tickers = new ArrayList<ITickable>();
    private HashMap<ResourceLocation, INBTSerializable<CompoundNBT>> dataHandlers = new HashMap<ResourceLocation, INBTSerializable<CompoundNBT>>();
    private ArrayList<ControlEntity> controls = new ArrayList<ControlEntity>();
    private ArrayList<ControlEntry> controlEntries = new ArrayList<ControlEntry>();
    private AbstractExterior exterior;
    private AbstractSoundScheme scheme;
    private BlockPos location = BlockPos.ZERO;
    private BlockPos destination = BlockPos.ZERO;
    private RegistryKey<World> dimension;
    private RegistryKey<World> destinationDimension;
    private Direction facing = Direction.NORTH;
    public int coordIncr = 10;
    private float max_artron = 0;
    private float artron = 0;
    private float rechargeMod = 1F;
    private ConsoleRoom consoleRoom = ConsoleRoom.STEAM;
    private List<Subsystem> subsystems = new ArrayList<>();
    private List<Upgrade> upgrades = new ArrayList<>();
    private String customName = "";
    private ExteriorPropertyManager exteriorProps;
    private SpaceTimeCoord returnLocation = SpaceTimeCoord.UNIVERAL_CENTER;
    private FlightEvent currentEvent = null;
    private List<DistressSignal> distressSignal = new ArrayList<DistressSignal>();
    private ItemStack sonic = ItemStack.EMPTY;
    protected TexVariant[] variants = {};
    private int variant = 0;
    private boolean antiGravs = false;
    private UUID tardisEntityID = null;
    private TardisEntity tardisEntity = null;
    private SparkingLevel sparkLevel = SparkingLevel.NONE;
    private String landingCode = "";
    private int landTime = 0;
    private HashMap<IArtronType, ArtronUse> artronUses = Maps.newHashMap();
    private LazyOptional<ExteriorTile> exteriorHolder = LazyOptional.empty();
    private boolean isCrashing = false;
    private UnlockManager unlockManager;
    protected HashMap<Class<?>, ControlOverride> controlOverrides = Maps.newHashMap();
    private boolean hasPoweredDown = false;
    private boolean hasNavCom = false;
    private boolean isBeingTowed = false;
    private BlockPos takeoffLocation = BlockPos.ZERO;
    /** If the console has been force loaded. Internal use only*/
    private boolean hasForcedChunksToRemove = false;

    /** If this Tardis console should start changing its interior*/
    private boolean shouldStartChangingInterior = false;
    /** The ConsoleRoom that we will be changing the current to*/
    private ConsoleRoom nextRoomToChange = ConsoleRoom.STEAM;
    
    /** What ever you do do not save / sync this (If you use onLoad() the world won't load) */
    private int timeUntilControlSpawn = 10;
    
    /** Last player to interact with a control, used for loyalty */
    private PlayerEntity pilot;
    
    /** Data handlers Read from this */
    private Runnable onLoadAction;

    public ConsoleTile(TileEntityType<?> type) {
        super(type);
        this.emotionHandler = new EmotionHandler(this);
        this.interiorManager = new InteriorManager(this);
        this.exteriorProps = new ExteriorPropertyManager(this);
        this.exterior = ExteriorRegistry.STEAMPUNK.get();
        this.dimension = World.OVERWORLD;
        this.destinationDimension = World.OVERWORLD;
        this.unlockManager = new UnlockManager(this);
        this.scheme = SoundSchemeRegistry.BASIC.get();
        this.registerControlEntry(ControlRegistry.HANDBRAKE.get());
        this.registerControlEntry(ControlRegistry.THROTTLE.get());
        this.registerControlEntry(ControlRegistry.RANDOM.get());
        this.registerControlEntry(ControlRegistry.DIMENSION.get());
        this.registerControlEntry(ControlRegistry.FACING.get());
        this.registerControlEntry(ControlRegistry.X.get());
        this.registerControlEntry(ControlRegistry.Y.get());
        this.registerControlEntry(ControlRegistry.Z.get());
        this.registerControlEntry(ControlRegistry.INC_MOD.get());
        this.registerControlEntry(ControlRegistry.LAND_TYPE.get());
        this.registerControlEntry(ControlRegistry.REFUELER.get());
        this.registerControlEntry(ControlRegistry.FAST_RETURN.get());
        this.registerControlEntry(ControlRegistry.TELEPATHIC.get());
        this.registerControlEntry(ControlRegistry.STABILIZERS.get());
        this.registerControlEntry(ControlRegistry.SONIC_PORT.get());
        this.registerControlEntry(ControlRegistry.COMMUNICATOR.get());
        this.registerControlEntry(ControlRegistry.DOOR.get());
        
        for(SubsystemEntry entry : SubsystemRegistry.SUBSYSTEM_REGISTRY.get().getValues()) {
            this.subsystems.add(entry.create(this));
        }
        
        for(UpgradeEntry entry : UpgradeRegistry.UPGRADE_REGISTRY.get().getValues()) {
            this.upgrades.add(entry.create(this));
        }

        this.registerControlOverrides();
    }
    
    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        //Things registered to save
        for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> saved : this.dataHandlers.entrySet()) {
            saved.getValue().deserializeNBT(compound.getCompound(saved.getKey().toString()));
        }
        
        //Subsystems
        ListNBT subsystemList = compound.getList("subsystems", Constants.NBT.TAG_COMPOUND);
        for(INBT base : subsystemList) {
            CompoundNBT nbt = (CompoundNBT)base;
            ResourceLocation key = new ResourceLocation(nbt.getString("name"));
            this.getSubsystem(key).ifPresent(sys -> sys.deserializeNBT(nbt));
        }
        
        if(compound.contains("unlock_manager"))
            this.unlockManager.deserializeNBT(compound.getCompound("unlock_manager"));
        
        this.location = BlockPos.fromLong(compound.getLong("location"));
        this.destination = BlockPos.fromLong(compound.getLong("destination"));
        this.dimension = WorldHelper.getWorldKeyFromRL(new ResourceLocation(compound.getString("dimension")));
        this.destinationDimension = WorldHelper.getWorldKeyFromRL(new ResourceLocation(compound.getString("dest_dim")));
        this.flightTicks = compound.getInt("flight_ticks");
        this.reachDestinationTick = compound.getInt("max_flight_ticks");
        this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(compound.getString("exterior")));
        this.artron = compound.getFloat("artron");
        if(compound.contains("console_room")) {
            ConsoleRoom room = ConsoleRoom.getRegistry().get(new ResourceLocation(compound.getString("console_room")));
            if(room != null)
                this.consoleRoom = room;
        }
        if (compound.contains("next_console_room")) {
             ConsoleRoom nextRoom = ConsoleRoom.getRegistry().get(new ResourceLocation(compound.getString("next_console_room")));
             if (nextRoom != null)
                 this.nextRoomToChange = nextRoom;
        }
        
        this.customName = compound.getString("custom_name");
        this.returnLocation = SpaceTimeCoord.deserialize(compound.getCompound("return_pos"));
        this.facing = Direction.values()[compound.getInt("facing")];
        this.sonic = ItemStack.read(compound.getCompound("sonic_item"));
        ListNBT distressList = compound.getList("distress_list_initial", Constants.NBT.TAG_COMPOUND);
        this.distressSignal.clear();
        for(INBT dis : distressList) {
            this.distressSignal.add(DistressSignal.deserializeNBT((CompoundNBT)dis));
        }
        
        this.max_artron = compound.getFloat("max_artron");
        this.rechargeMod = compound.getFloat("recharge_modifier");
        this.variant = compound.getInt("texture_variant");
        this.antiGravs = compound.getBoolean("anti_gravs");
        this.hasForcedChunksToRemove = compound.getBoolean("has_forced_chunks");
        this.hasNavCom = compound.getBoolean("nav_com");
        this.shouldStartChangingInterior = compound.getBoolean("start_changing_interior");
        
        ListNBT artronUsesList = compound.getList("artron_uses", Constants.NBT.TAG_COMPOUND);
        for(INBT base : artronUsesList) {
            CompoundNBT nbt = (CompoundNBT)base;
            ArtronUse use = ArtronUse.deserialiseNBT(nbt);
            IArtronType type = use.getType();
            if (type != null) {
            	this.artronUses.put(type, use);
            }
        }
        
        if(compound.contains("tardis_entity_id"))
            this.tardisEntityID = compound.getUniqueId("tardis_entity_id");
        this.sparkLevel = SparkingLevel.getFromIndex(compound.getInt("spark_level"));
        this.landingCode = compound.getString("landing_code");
        this.landTime = compound.getInt("landing_time");
        this.takeoffLocation = BlockPos.fromLong(compound.getLong("takeoff_location"));
        if(compound.contains("sound_scheme"))
            this.scheme = SoundSchemeRegistry.SOUND_SCHEME_REGISTRY.get().getValue(new ResourceLocation(compound.getString("sound_scheme")));
        
        this.onLoadAction = () -> {
            for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> saved : this.dataHandlers.entrySet()) {
                saved.getValue().deserializeNBT(compound.getCompound(saved.getKey().toString()));
            }
        };
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> entry : this.dataHandlers.entrySet()) {
            compound.put(entry.getKey().toString(), entry.getValue().serializeNBT());
        }
        
        //Subsystems
        ListNBT subsystemList = new ListNBT();
        for(Subsystem s : this.subsystems) {
            CompoundNBT nbt = s.serializeNBT();
            nbt.putString("name", s.getEntry().getRegistryName().toString());
            subsystemList.add(nbt);
        }
        compound.put("subsystems", subsystemList);
        
        compound.put("unlock_manager", this.unlockManager.serializeNBT());

        compound.putLong("location", this.location.toLong());
        compound.putLong("destination", this.destination.toLong());
        compound.putInt("flight_ticks", this.flightTicks);
        compound.putInt("max_flight_ticks", this.reachDestinationTick);
        compound.putString("exterior", this.exterior.getRegistryName().toString());
        compound.putString("dimension", this.dimension.getLocation().toString());
        compound.putString("dest_dim", this.destinationDimension.getLocation().toString());
        compound.putFloat("artron", this.artron);
        compound.putString("console_room", this.consoleRoom.getRegistryName().toString());
        compound.putString("next_console_room", this.nextRoomToChange.getRegistryName().toString());
        compound.putString("custom_name", this.customName);
        compound.put("return_pos", this.returnLocation.serialize());
        compound.putInt("facing", this.facing.ordinal());
        compound.putLong("takeoff_location", this.takeoffLocation.toLong());

        compound.put("sonic_item", this.sonic.serializeNBT());
        
        //SOSes
        ListNBT distress = new ListNBT();
        for(DistressSignal dis : this.distressSignal)
            distress.add(dis.serializeNBT());
        compound.put("distress_list_initial", distress);
        
        compound.putFloat("max_artron", this.max_artron);
        compound.putFloat("recharge_modifier", this.rechargeMod);
        compound.putInt("texture_variant", this.variant);
        compound.putBoolean("anti_gravs", this.antiGravs);
        if(this.tardisEntityID != null)
            compound.putUniqueId("tardis_entity_id", this.tardisEntityID);
        compound.putInt("spark_level", this.sparkLevel.ordinal());
        compound.putString("landing_code", this.landingCode);
        compound.putInt("landing_time", this.landTime);
        compound.putString("sound_scheme", this.scheme.getRegistryName().toString());
        compound.putBoolean("nav_com", this.hasNavCom);
        compound.putBoolean("has_forced_chunks", this.hasForcedChunksToRemove);
        compound.putBoolean("start_changing_interior", this.shouldStartChangingInterior);
        ListNBT artronUseList = new ListNBT();
        for (Entry<IArtronType, ArtronUse> entry : this.artronUses.entrySet()) {
        	CompoundNBT nbt = entry.getValue().serialiseNBT();
            artronUseList.add(nbt);
        }
        compound.put("artron_uses", artronUseList);
        return super.write(compound);
    }

    @Override
    public void tick() {
        //Cycle through tickable objects
        for(ITickable tick : this.tickers) {
            tick.tick(this);
        }

        this.prevFlightTicks = this.flightTicks;
        if(this.isInFlight()) {
            fly();
        }
        
        this.playAmbientNoises();
        
        this.handleRefueling();
        
        if(world.getGameTime() % 200 == 0) {
            if(world.isRemote || controls.isEmpty())
                this.getOrCreateControls();
        }
        
        if(timeUntilControlSpawn > 0) {
            --timeUntilControlSpawn;
            if(timeUntilControlSpawn == 0) {
                this.getOrCreateControls();
                if(this.onLoadAction != null)
                    this.onLoadAction.run();
            }
        }
        
        //Loop for things that need to be polled semi-constantly
        if(!world.isRemote && world.getGameTime() % 40 == 0) {
            this.updateArtronValues();
            
            //Loop for sparking
            SparkingLevel spark = SparkingLevel.NONE;
            for(Subsystem s : this.subsystems) {
                if(s.getSparkState().ordinal() > spark.ordinal())
                    spark = s.getSparkState();
            }
            
            if(this.sparkLevel != spark) {
                this.sparkLevel = spark;
                this.updateClient();
            }
            
            //Force Field Drain
            this.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> {
                if(shield.canBeUsed() && shield.isActivated() && shield.isForceFieldActivated() && this.artron > 1.0F) {
                    ArtronUse use = this.getOrCreateArtronUse(ArtronType.FORCEFIELD);
                    use.setArtronUsePerTick(0.05F);
                    use.setTicksToDrain(42);
                    shield.damage((ServerPlayerEntity)this.getPilot(), 1);
                }
                else shield.setForceFieldActivated(false);
            });
            
            //Anti-gravs
            if(this.getAntiGrav()) {
                ArtronUse use = this.getOrCreateArtronUse(ArtronType.ANTIGRAVS);
                use.setArtronUsePerTick(0.03F);
                use.setTicksToDrain(42);
            }
        }
        
        //Artron Drains
        if(!world.isRemote) {
            world.getServer().enqueue(new TickDelayedTask(0, () -> {
                float oldArtron = this.artron;
                for(ArtronUse use : this.artronUses.values()) {
                    use.tick(this);
                }
                if(oldArtron != this.artron && world.getGameTime() + 4 % 20 == 0) {
                    if(this.artron <= 0.0 && !this.hasPoweredDown)
                        this.onPowerDown(true);
                    else if(artron > 0)
                        this.hasPoweredDown = false;
                }
            }));
        }
        
        if(world.isRemote &&  world.getGameTime() % 5 == 0) {
            if(this.sparkLevel != SparkingLevel.NONE) {
                
                if(this.sparkLevel != SparkingLevel.NONE) {
                    for(int i = 0; i < 30; ++i) {
                        
                        float angle = (float)Math.toRadians(rand.nextFloat() * 360.0F);
                        
                        world.addParticle(ParticleTypes.SMOKE, pos.getX() + 0.5 + Math.sin(angle), pos.getY() + 1, pos.getZ() + 0.5 + Math.cos(angle), 0, 0, 0);
                    }
                }
                if(this.sparkLevel == SparkingLevel.SPARKS) {
                    world.addParticle(ParticleTypes.LAVA, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 1, 0);
                }
            }
        }
        
        if(!world.isRemote && !this.isInFlight()) {
            this.getSubsystem(AntennaSubsystem.class).ifPresent(sys -> {
                if(sys.canBeUsed()) {
                    if(world.getGameTime() % 2880 == 0) {
                        this.findNewMission();
                    }
                }
            });
        }
        
        //Nav Com
        if(!world.isRemote && world.getGameTime() % 20 == 0) {
            this.getSubsystem(NavComSubsystem.class).ifPresent(sys -> {
                boolean oldVal = this.hasNavCom;
                this.hasNavCom = sys.canBeUsed();
                if(oldVal != this.hasNavCom)
                    Network.sendToAllInWorld(new ConsoleUpdateMessage(DataTypes.NAV_COM, new NavComData(this.hasNavCom)), (ServerWorld)world);
            });       
        }
        //This is still needed because we want to unforce the 35 other chunks we loaded when we were trying to find the interior door during right clicking the exterior block
        this.handleAutoLoadOrUnloadChunks();
        
    }
    

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        super.onDataPacket(net, pkt);
        this.read(this.world.getBlockState(this.getPos()), pkt.getNbtCompound());
        if(pkt.getNbtCompound().contains("current_event"))
            this.currentEvent = FlightEventRegistry.FLIGHT_EVENT_REGISTRY.get().getValue(new ResourceLocation(pkt.getNbtCompound().getString("current_event"))).create(this);
        else this.currentEvent = null;
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        return new SUpdateTileEntityPacket(this.getPos(), 99, this.getUpdateTag());
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT tag = this.write(new CompoundNBT());
        if(this.currentEvent != null && !this.currentEvent.isComplete())
            tag.putString("current_event", this.currentEvent.getEntry().getRegistryName().toString());
        else tag.remove("current_event");
        return tag;
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        this.read(state, tag); //Fixes tile values being reset on world load
    }

    @Override
    public void onLoad() {
        super.onLoad();
        this.timeUntilControlSpawn = 10; //Defer control entity spawn to prevent server deadlock
    }
    
    /**
     * Handle logic for when a Tardis is first created via the Broken Exterior
     */
    public void onInitialSpawn() {
        this.getEmotionHandler().onInitialSpawn();
    }

    public void updateClient() {
        if(world.isRemote) return;
        
        BlockState state = world.getBlockState(this.getPos());
        world.markAndNotifyBlock(this.getPos(), world.getChunkAt(getPos()), state, state, 2, 512);
    }
    
    /**
     * Register tickable events
     * <br> E.g. Mercury link when fluid links break, oxygen with electrolytic upgrade
     * @param ticker
     */
    public void registerTicker(ITickable ticker) {
        this.tickers.add(ticker);
    }
    
    public void registerDataHandler(ResourceLocation loc, INBTSerializable<CompoundNBT> handler) {
        this.dataHandlers.put(loc, handler);
    }
    
    public void registerControlEntry(ControlEntry entry) {
        this.controlEntries.add(entry);
    }
    

    public void getOrCreateControls() {
        this.gatherOldControls();
        if(!world.isRemote && this.controls.size() < this.controlEntries.size()) {
            this.removeControls();
            for(ControlEntry controlEntry : this.controlEntries) {
                ControlEntity entity = TEntities.CONTROL.get().create(this.world);
                AbstractControl control = controlEntry.spawn(this, entity);
                
                Vector3d offset = null;
                if(this.controlOverrides.containsKey(control.getClass()))
                    offset = this.controlOverrides.get(control.getClass()).getPosition();
                else offset = control.getPos();
                
                
                entity.setPosition(
                        this.getPos().getX() + 0.5 + offset.x,
                        this.getPos().getY() + 0.5 + offset.y,
                        this.getPos().getZ() + 0.5 + offset.z);
                entity.setControl(control);
                entity.setConsole(this);
                ((ServerWorld)world).addEntityIfNotDuplicate(entity);
                this.controls.add(entity);
            }
        }
        this.updateClient();
    }
    
    /*
     * Gets controls after a world reload
     */
    private void gatherOldControls() {
        this.controls.clear();
        for(ControlEntity control : world.getEntitiesWithinAABB(ControlEntity.class, CONRTROL_HITBOX.offset(getPos()).grow(2))) {
            if(control.isAlive()) {
                control.setConsole(this);
                this.controls.add(control);
            }
        }
    }

    public void removeControls() {
        for(ControlEntity control : world.getEntitiesWithinAABB(ControlEntity.class, CONRTROL_HITBOX.offset(this.getPos()).grow(5))) {
            control.remove();
        }
        this.controls.clear();
    }
    

    /**
     * Gets list of controls which should override the default controls
     * <br> Used by addon mods to adjust positions of controls on their consoles
     * @return
     */
    public HashMap<Class<?>, ControlOverride> getControlOverrides(){
        return this.controlOverrides;
    }
    
    /** Allows addon mods to populate the control overrides map to reposition specific controls for their console units
     * <br> This is called in the main constructor of the ConsoleTile class to reduce issues where the map is sometimes empty*/
    public void registerControlOverrides() {

    }
    
    /** Add a Control Override to the console's control override list*/
    public void addControlOverride(Class<?> controlClazz, ControlOverride override) {
        this.controlOverrides.put(controlClazz, override);
    }
    
    /**
     * Makes Tardis start takeoff sequence
     * @return
     */
    public boolean takeoff() {
        return this.takeoff(false);
    }
    
    /**Take off, removes exterior, sets up flight and starts flight loop
     * @param towed - if this Tardis is being towed
     * @return
     */
    public boolean takeoff(boolean towed) {
        if(this.isInFlight() || world.isRemote)
            return false;
        if(!this.canFly() && !towed) {
            this.world.playSound(null, this.getPos(), TSounds.CANT_START.get(), SoundCategory.BLOCKS, 1F, 1F);
            return false;
        }
        //Force load the console tile during takeoff to ensure it will stay in flight
        this.forceLoadInteriorChunk(true, false);
        
        this.isCrashing = false;
        this.isBeingTowed = towed;

        this.takeoffLocation = this.location;

        if(this.getEntity() != null)
            this.getEntity().remove();

        this.currentEvent = null;
        
        this.returnLocation = new SpaceTimeCoord(this.getCurrentDimension(), this.getCurrentLocation(), this.getTrueExteriorFacingDirection());
        
        this.getEmotionHandler().addMood(10);
        this.getEmotionHandler().addLoyalty(this.getPilot(), 1);
        
        //Forceload the chunk at the current location
        //This will be unloaded by the exterior block in ExteriorBlock#onRemoved if the exterior block is present
        ServerWorld otherWorld = world.getServer().getWorld(this.dimension);
        
        BlockPos extPos = this.location.up(); //Assume the exterior block is one block above our location
        ChunkPos chunkPos = new ChunkPos(extPos);
//        Tardis.LOGGER.debug("Does {} have forced chunks before Takeoff? Counted: {}", otherWorld.getDimensionKey().getLocation(), WorldHelper.getTickingBlockForcedChunks(otherWorld) != null ? WorldHelper.getTickingBlockForcedChunks(otherWorld).size() : "None");
        //Force load the take off position so the exterior blocks can be removed properly. We will unload them after the blocks are removed
        WorldHelper.forceChunkIfNotLoaded(otherWorld, chunkPos, extPos);
        
        
        world.getServer().enqueue(new TickDelayedTask(1, () -> {
            this.landTime = 0;
            this.reachDestinationTick = this.calcFlightTicks(false) +
                    this.getSoundScheme().getTakeoffTime();
            this.flightTicks = 1;
            this.exterior.demat(this); //Start the demat animation and remove the exterior blocks
            this.scheme.playTakeoffSounds(this);
//            System.out.println("TARDIS: Started a flight to last " + this.reachDestinationTick);
            this.getControl(RefuelerControl.class).ifPresent(refuel -> refuel.setRefueling(false));
            MinecraftForge.EVENT_BUS.post(new TardisEvent.Takeoff(this));
            
            //Force the area we loaded if the exterior block has not done so
            WorldHelper.unForceChunkIfLoaded(otherWorld, chunkPos, location);
            
        }));
        
        this.updateClient();

        //Blocks things like mercury from crashes
        if(!this.isBeingTowed){
            for (Subsystem sub : this.getSubSystems()) {
                sub.onTakeoff();
            }
            for (Upgrade up : this.getUpgrades()) {
                up.onTakeoff();
            }
        }
        
        //Shake player's screens
        if(!world.isRemote) {
            for(PlayerEntity player : world.getPlayers()) {
                player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                    cap.setShaking(this.getSoundScheme().getTakeoffTime());
                    cap.update();
                });
            }
        }
        return true;
    }
    
    public boolean takeoffTowed() {
        boolean takeoff = this.takeoff(true);
//        System.out.println("Save take off " + takeoff);
        return takeoff;
    }
    /** Starts landing sequence and if necessary, modifies the position and dimension based on current journey percentage. */
    public void initLand() {
        this.scaleDestination();
        this.land();
        this.updateClient();
    }
    
    /** Landing code, handles exiting flight and seting up the exterior */
    public void land() {
        
        
        if(!world.isRemote) {
            
            ServerWorld destWorld = world.getServer().getWorld(this.destinationDimension);
//            Tardis.LOGGER.debug("Does {} have forced chunks before landing? Counted: {}", destWorld.getDimensionKey().getLocation(), WorldHelper.getTickingBlockForcedChunks(destWorld) != null ? WorldHelper.getTickingBlockForcedChunks(destWorld).size() : "None");
            //Pre-load the likely landing chunks. It doesn't really matter where we load, as long as the destination world is loaded
            
            BlockPos likelyLandingPos = this.destination.up(); //Set this to the exterior block's likely position, which we assume one block above this
            ChunkPos likelyLandingChunkPos = new ChunkPos(likelyLandingPos);
//            System.out.println("Likely Land Pos:" + likelyLandingPos);
            WorldHelper.forceChunkIfNotLoaded(destWorld, likelyLandingChunkPos, likelyLandingPos);
            this.landTime = this.prevFlightTicks = this.flightTicks + this.getSoundScheme().getLandTime();
            //Modify the destination position based on emotions and other factors at the predicated landing spot. Delay all this until after world loaded to prevent the server locking up.
            world.getServer().enqueue(new TickDelayedTask(1, () -> {
                ServerWorld ws = destWorld;
                this.dimension = this.destinationDimension;
                
                this.playLandSound();
                //Clear the flight mini-game controls
                this.currentEvent = null;
                
                //Add Emotional- induced inaccuracy
                if(this.getEmotionHandler() != null && this.getEmotionHandler().getMood() < EnumHappyState.APATHETIC.getTreshold()) {
                    this.destination = this.randomizeCoords(this.destination, 100);
                }
                
                //Sanity check destination
                this.destination = LandingSystem.validateBlockPos(destination, ws, this, this.exterior);
                
                //World border- redirect
                this.destination = LandingSystem.redirectBehindBorder(ws, destination, this);

                //Get landing type, up or down
                ObjectWrapper<EnumLandType> landTypeWrapper = new ObjectWrapper<>(EnumLandType.DOWN);
                this.getControl(LandingTypeControl.class).ifPresent(landControl -> {
                    landTypeWrapper.setValue(landControl.getLandType());
                });
                EnumLandType landType = landTypeWrapper.getValue();

                //Find suitable location. If nothing interferes, this is where it will land
                BlockPos landSpot = LandingSystem.getLand(ws, destination, landType, this);

                //Let traits affect landing
                for(TardisTrait trait : this.getEmotionHandler().getTraits()){
                    if(trait instanceof IAffectLandingTrait){
                        landSpot = ((IAffectLandingTrait)trait).redirect(ws, landSpot);
                    }
                }
                
                //TARDIS - in - TARDIS
                SpaceTimeCoord otherTardisCoord = LandingSystem.handleTardisInTardis(ws, landSpot, this);
                if(otherTardisCoord == null){
                    //Search 30 times for a suitable landing position if not landing in a TARDIS
                    if(landSpot.equals(BlockPos.ZERO) || ws.getBlockState(landSpot.down()).isIn(TardisBlockTags.BLOCKED)) {
                        for(int i = 0; i < 30; ++i) {
                            if(landSpot.equals(BlockPos.ZERO) || ws.getBlockState(landSpot.down()).isIn(TardisBlockTags.BLOCKED) || !ws.isAreaLoaded(landSpot, 3))
                                landSpot = LandingSystem.getLand(ws, this.randomizeCoords(destination, 30), landType, this);
                            else break;
                        }
                    }
                }
                else{
                    this.destinationDimension = otherTardisCoord.getDim();
                    landSpot = otherTardisCoord.getPos();
                }

                //Be affected by tile in the world
               SpaceTimeCoord interference = LandingSystem.affectFromWorldBlocks(ws, landSpot, landType, this);
                //If this was redirected
                if(interference != null){
                    landSpot = interference.getPos();
                    this.facing = interference.getFacing();
                }
                //Try to kill if pissed at occupants
                if(this.getEmotionHandler().getMood() < EnumHappyState.DISCONTENT.getTreshold()){
                    for(PlayerEntity occupant : this.getWorld().getPlayers()){
                        //If there is a player this TARDIS wants to kill
                        if(this.getEmotionHandler().getLoyalty(occupant.getUniqueID()) < -10){
                            landSpot = LandingSystem.tryToKill(ws, landSpot, this.facing, 30);
                            break;
                        }
                    }
                }

                //If no valid destination, force the old one
                if(landSpot.equals(BlockPos.ZERO))
                    landSpot = this.destination;
                Helper.addTardisDistanceTravelledStat(pilot, this.location, landSpot);
                //Set destination
                this.location = this.destination = landSpot.toImmutable();
                
                if(this.isCrashing) {
                    //Explode
                    if(world.getServer().getGameRules().getBoolean(GameRules.MOB_GRIEFING)) {
                        destWorld.createExplosion(null, landSpot.getX() + 0.5, landSpot.getY(), landSpot.getZ() + 0.5, 3.0F, Mode.BREAK);
                    }
                }
                
                this.exterior.remat(this);
                this.getSoundScheme().playExteriorLand(this);
                
                this.getControl(ThrottleControl.class).ifPresent(throttle -> throttle.setAmount(0.0F));
                
                this.updateClient();
                
                MinecraftForge.EVENT_BUS.post(new TardisEvent.Land(this));
                BlockPos extPos = landSpot.up();
//                System.out.println("Final land pos: " + extPos);
                ChunkPos finalChunkPos = new ChunkPos(extPos);
                if(!likelyLandingChunkPos.equals(finalChunkPos) || !likelyLandingPos.equals(extPos)) { //unload the original chunks we preloaded if the final landing positions doesn't match the predicated positions
                    WorldHelper.unForceChunkIfLoaded(destWorld, likelyLandingChunkPos, likelyLandingPos);
                }
            }));
            
            //50ap5ud5: Temporarily comment this out for now to allow the exterior block to handle the chunk loading through ExteriorBlock#onBlockAdded
            //When the exterior block handles the chunkloading, this seems to fix a visual issue where the exterior is seen to still be rematting on landing
            //Remove this after we can confirm that this change will not accidentally keep the previous landing spots loaded
            /*
            //Unload the original area we loaded after the exterior has been placed
            WorldHelper.unForceChunkIfLoaded(destWorld, likelyLandingChunkPos, this.destination);
            */
            
//            Tardis.LOGGER.debug("Does {} have forced chunks after landing? Counted: {}", destWorld.getDimensionKey().getLocation(), WorldHelper.getTickingBlockForcedChunks(destWorld) != null ? WorldHelper.getTickingBlockForcedChunks(destWorld).size() : "None");
            if(!this.isBeingTowed){
            	for (Subsystem sub : this.getSubSystems()) {
                    sub.onLand();
                }
                for (Upgrade up : this.getUpgrades()) {
                    up.onLand();
                }
            }
        }
        
    }

    //Violently fall out of flight
    public void crash(CrashType type) {
        
        if(world.isRemote || this.isCrashing || this.isBeingTowed)
            return;
        
        Network.sendToAllInWorld(new ConsoleUpdateMessage(DataTypes.CRASH, new CrashData()), (ServerWorld)world);
        
        world.playSound(null, this.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 1F, 0.25F);
        this.isCrashing = true;
        this.getInteriorManager().soundAlarm(AlarmType.LOW);
        this.scaleDestination();
        this.destination = this.randomizeCoords(this.destination, type.getInaccuracy());
        this.land();
        world.getServer().enqueue(new TickDelayedTask(1, () -> {
            this.landTime = this.flightTicks = this.reachDestinationTick = 0;
        })); //Enqueue land time to be reset after the land method as the land method is already enqueued to a prevent chunk deadlock. :mojank:
        //Shimmy-Shake
        for (LivingEntity ent : this.world.getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(this.getPos()).grow(20))) {
            ent.setMotion(ent.getMotion().add(rand.nextDouble() - 0.5, rand.nextDouble(), rand.nextDouble() - 0.5));
            if (ent instanceof ServerPlayerEntity) {
                Network.sendTo(new MissControlMessage(), (ServerPlayerEntity) ent);
            }
        }
        
        if(type.shouldDoExteriorEffects()){
            world.getServer().enqueue(new TickDelayedTask(20, () -> {
                //Set the exterior to be crashed
                if(!world.isRemote) {
                    world.getServer().enqueue(new TickDelayedTask(1, () -> {
                        ExteriorTile ext = this.getExteriorType().getExteriorTile(this);
                        if(ext != null) {
                            ext.setCrashed(true);
                        }
                    }));
                }
            }));
        }
        Helper.addTardisStatistic(this.pilot, TardisStatistics.TARDIS_CRASH_COUNT);        
    }
    
    public void playCrashEffects() {
        if(world.isRemote && world.getGameTime() % 40 == 0) {
            world.addParticle(ParticleTypes.EXPLOSION, getPos().getX() + 0.5, getPos().getY() + 1, getPos().getZ(), 0, 0, 0);
        }
    }
    
    /**
     * Set amount of damage to subsystems when the exterior is attacked
     * @param damage
     */
    public void damage(float damage) {
        
        List<Subsystem> activeSys = Lists.newArrayList();

        for(Subsystem sys : this.subsystems) {
            if(sys.canBeUsed() && sys.isActivated())
                activeSys.add(sys);
        }
        
        int dam = (int)Math.ceil(damage / (float)activeSys.size());
        for(Subsystem sub : activeSys) {
            sub.damage(null, dam);
        }
    }
    
    protected void handleRefueling() {
        if(!world.isRemote && !this.isInFlight() && artron < this.max_artron)
            this.getControl(RefuelerControl.class).ifPresent(refuel -> {
                if(refuel.isRefueling()) {
                    this.getSubsystem(FluidLinksSubsystem.class).ifPresent(link -> {
                        if(link.canBeUsed()) {
                            this.artron += (0.025F * this.rechargeMod);
                            
                            if(artron > this.max_artron)
                                this.artron = this.max_artron;
                            
                            if(this.artron < 0)
                                this.artron = 0;
                            
                            if(world.getGameTime() % 20 == 0)
                                Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FUEL, new Fuel(this.artron, this.max_artron)), world.getDimensionKey(), this.getPos(), 20);
                        }
                    });
                }
            });
    }
    
    /** Fly loop, called every tick */
    public void fly() {
        if(this.isInFlight()) {
            
            this.prevFlightTicks = this.flightTicks;
            ++this.flightTicks;

            if(this.isLanding())
                this.location = this.getPositionInFlight().getPos();

            //If crashing, play crash effects
            if(this.isCrashing)
                this.playCrashEffects();
            
            //Land if reached destination and stabilized
            if(!world.isRemote && this.flightTicks >= this.reachDestinationTick && landTime <= 0){
                if (this.isBeingTowed) {
                	this.initLand();
                }
            	this.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
                    if(sys.isControlActivated()) {
                    	//Only call land if we're not being towed.
                    	//Prevents land being called twice, which could cause the Tardis' position to get recalculated
                    	if (!this.isBeingTowed) {
                    		this.initLand();
                    	}
                    }
                });
            }
            
            if(!world.isRemote && this.flightTicks > this.landTime && this.landTime > 0) {
                this.flightTicks = this.reachDestinationTick = this.landTime = 0;
                this.updateClient();
            }
            
            //Crash if it can't fly
            if(!world.isRemote && !this.canFly()) {
                crash(CrashTypes.DEFAULT);
                return;
            }
            
            //Artron usage
            if(!world.isRemote) {
                ArtronUse use = this.getOrCreateArtronUse(ArtronType.FLIGHT);
                use.setArtronUsePerTick(this.calcFuelUse());
                use.setTicksToDrain(1);

                if (this.flightTicks % 20 == 0) {
                    for (Subsystem sub : this.getSubSystems()) {
                        sub.onFlightSecond();
                    }
                    for (Upgrade up : this.getUpgrades()) {
                        up.onFlightSecond();
                    }
                }
                
                if(world.getGameTime() % 20 == 0)
                    Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FUEL, new Fuel(this.artron, this.max_artron)), world.getDimensionKey(), this.getPos(), 20);
                
            }

            if (!world.isRemote) {
                
                //If this has an event and it's time, complete it
                if(!this.isBeingTowed && currentEvent != null && this.currentEvent.getMissedTime() < this.flightTicks) {
                    currentEvent.onComplete(this);
                    
                    //Search for collisions
                    this.currentEvent = null;

                    //If Not landing
                    if(this.landTime <= 0) {
                        ObjectWrapper<Boolean> collided = new ObjectWrapper<>(false);
                        Iterator<ServerWorld> it = world.getServer().getWorlds().iterator();
                        while(it.hasNext()) {
                            ServerWorld world = it.next();
                            
                            //Stop if found one to collide with
                            if(collided.getValue())
                                break;
                            
                            TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
                                //if unstabilized and not ourselves
                                this.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
                                    if(tile != this && tile.isInFlight() && !sys.isControlActivated()) {
                                        //If not landing and not already colliding
                                        if(tile.getLandTime() == 0 && !(tile.getFlightEvent() instanceof TardisCollideInstigate) && !(tile.getFlightEvent() instanceof TardisCollideRecieve)) {
                                            if(tile.getPositionInFlight().getPos().withinDistance(this.getPositionInFlight().getPos(), TConfig.SERVER.collisionRange.get())) {
                                                this.setFlightEvent(((TardisCollideInstigate)FlightEventRegistry.COLLIDE_INSTIGATE.get().create(this)).setOtherTARDIS(tile));
                                                collided.setValue(true);
                                            }
                                        }
                                    }
                                });
                            });
                        }
                    }
                    
                    if(this.canGiveNewEvent() && this.currentEvent == null)
                        this.setFlightEvent(FlightEventRegistry.getRandomEvent(rand).create(this));
                    
                }
                
                else if(this.currentEvent == null && this.canGiveNewEvent())
                    this.setFlightEvent(FlightEventRegistry.getRandomEvent(rand).create(this));
                
            }
            
            //Shake
            if(!world.isRemote && world.getGameTime() % 3 == 0) {
                if(this.currentEvent != null && !this.currentEvent.getControls().isEmpty())
                    for(PlayerEntity player : world.getPlayers()) {
                        player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                            cap.setShaking(5);
                            cap.update();
                        });
                    }
            }
        }

        this.playFlightLoop();
        
        //Fuck TARDIS abusers
        if(!world.isRemote && this.sparkLevel != SparkingLevel.NONE && world.getGameTime() % 60 == 0) {
            if(this.getEmotionHandler().getMood() > EnumHappyState.DISCONTENT.getTreshold())
                this.getEmotionHandler().addMood(-1);
        }
                
                
    }
    
    public boolean isInFlight() {
        return this.flightTicks > 0;
    }
    
    /** If the Tardis is in the process of landing, but is still in flight. 
     * <br> The land time is usually zero or less than zero at takeoff, during flight when flight time is being updated.
     * <br> During {@link ConsoleTile#land()}, it will greater than zero, hence we check for larger than*/
    public boolean isLanding() {
        return this.isInFlight() && this.landTime > 0; //isInFlight check is redundant but we have it here in case addon mods call it when they're not supposed to.
    }
    
    /**
     * 
     * @return - True if Journey is complete, might be in flight, or not.
     */
    public boolean hasReachedDestination() {
        return this.flightTicks >= this.reachDestinationTick;
    }
    
    public int getTimeInFlight() {
        return this.flightTicks;
    }
    
    public int getLandTime() {
        return this.landTime;
    }
    
    public void setDestinationReachedTick(int max) {
        this.reachDestinationTick = max;
        if(!world.isRemote)
            this.updateClient();
    }
    
    public void setDestination(RegistryKey<World> worldKey, BlockPos pos) {
        this.destination = pos.toImmutable();
        this.destinationDimension = worldKey;
        this.markDirty();
        if(this.isInFlight())
            this.updateFlightTime();
        this.updateClient();
    }
    
    public void setDestination(SpaceTimeCoord coord) {
        this.setDestination(coord.getDim(), coord.getPos());
    }

    public boolean isCrashing() {
        return this.isCrashing;
    }

    public void setCrashing(boolean crash) {
        this.isCrashing = crash;
    }
    
    public void setConsoleRoom(ConsoleRoom room) {
        this.consoleRoom = room;
        if(room == null)
            throw(new NullPointerException());
        this.markDirty();
        this.updateClient();
    }
    
    public void playFlightLoop() {
        if(!world.isRemote) {
            if(this.flightTicks % this.scheme.getLoopTime() == 0 && this.flightTicks > this.scheme.getTakeoffTime() && this.landTime <= 0)
                this.scheme.playFlightLoop(this);
        }
    }
    
    private void playLandSound() {
        if(!world.isRemote) {
            this.scheme.playInteriorLand(this);
            this.scheme.playExteriorLand(this);
            
            for(PlayerEntity player : world.getPlayers()) {
                player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                    cap.setShaking(this.getSoundScheme().getLandTime());
                    cap.update();
                });
            }
        }
    }
    
    private void playAmbientNoises() {
        
        //Client managed Sounds
        if(world.isRemote) {
            PlayerEntity player = ClientHelper.getClientPlayer();
            //Creaks
            if(player.ticksExisted % 2400 == 0)
                ClientHelper.playMovingSound(player, TSounds.AMBIENT_CREAKS.get(), SoundCategory.AMBIENT, 0.5F, false);
            if(this.consoleRoom == ConsoleRoom.NAUTILUS) {
                if(player.ticksExisted % 600 == 0)
                    ClientHelper.playMovingSound(player, SoundEvents.AMBIENT_UNDERWATER_LOOP_ADDITIONS_ULTRA_RARE, SoundCategory.AMBIENT, 1F, false);
            }
            
            //Sparking sound
            if(this.sparkLevel == SparkingLevel.SPARKS && world.getGameTime() % 60 == 0)
                world.playSound(ClientHelper.getClientPlayer(), this.getPos(), TSounds.ELECTRIC_SPARK.get(), SoundCategory.BLOCKS, 0.3F, 1F);
        }
    }
    
    public void playSoundAtExterior(SoundEvent sound, SoundCategory cat, float vol, float pitch) {
        ExteriorTile tile = this.exterior.getExteriorTile(this);
        if(tile != null)
            tile.getWorld().playSound(null, tile.getPos(), sound, cat, vol, pitch);
    }
    
    //Getters 'n' such
    /** Get the type of exterior for this Tardis*/
    public AbstractExterior getExteriorType() {
        return this.exterior;
    }
    /** Set the Exterior type for this Tardis*/
    public void setExteriorType(AbstractExterior ext) {
        this.exterior = ext;
        this.markDirty();
        this.updateClient();
    }
    
    @SuppressWarnings("unchecked")
    public <T extends AbstractControl> Optional<T> getControl(Class<T> clazz){
        for(ControlEntity control : controls) {
            if(control.getControl() == null) {
                control.remove();
                continue;
            }
            if(control.getControl().getClass() == clazz)
                return Optional.of((T)control.getControl());
        }
        return Optional.empty();
    }
    
    public ArrayList<ControlEntity> getControlList(){
        return this.controls;
    }
    
    public EmotionHandler getEmotionHandler() {
        return this.emotionHandler;
    }
    
    
    public InteriorManager getInteriorManager() {
        return this.interiorManager;
    }
    
    public ExteriorPropertyManager getExteriorManager() {
        return this.exteriorProps;
    }

    public UnlockManager getUnlockManager() {
        return this.unlockManager;
    }
    
    /*
     * This gets the first door in this dimension
     */
    public LazyOptional<DoorEntity> getDoor() {
        if(world instanceof ServerWorld) {
            Iterator<Entity> it = ((ServerWorld)world).getEntities().iterator();
            while(it.hasNext()) {
                Entity e = it.next();
                if(e instanceof DoorEntity) 
                    return LazyOptional.of(() -> (DoorEntity) e);
            }
        }
        else {
            for(DoorEntity ent : world.getEntitiesWithinAABB(DoorEntity.class, new AxisAlignedBB(this.getPos()).grow(30))) {
                return LazyOptional.of(() -> ent);
            }
        }
        return LazyOptional.empty();
    }
    
    public LazyOptional<ExteriorTile> getOrFindExteriorTile(){
        if(this.exteriorHolder.isPresent())
            return this.exteriorHolder;
        
        this.exteriorHolder.invalidate();
        
        ExteriorTile tile = this.getExteriorType().getExteriorTile(this);
        if(tile != null) {
            return this.exteriorHolder = LazyOptional.of(() -> tile);
        }
        return this.exteriorHolder = LazyOptional.empty();
        
    }
    
    /**
     * Gets the true facing of the Exterior
     * <br> Do NOT call this in models or renderers as this can be performance intensive
     * @return
     */
    public Direction getTrueExteriorFacingDirection() {
        if(!world.isRemote) {
            ServerWorld other = world.getServer().getWorld(this.dimension);
            if(other.isAreaLoaded(this.getCurrentLocation(), 1)) {
                ExteriorTile ext = this.getExteriorType().getExteriorTile(this);
                if(ext != null) {
                    BlockState state = ext.getBlockState();
                    if(state.hasProperty(BlockStateProperties.HORIZONTAL_FACING))
                        return state.get(BlockStateProperties.HORIZONTAL_FACING);
                }
            }
        }
        return this.facing;
    }
    
    public ConsoleRoom getConsoleRoom() {
        return this.consoleRoom;
    }
    
    /**
     * If {@link #flightTicks} >= this, then the actual journey part is over, it has reached it's destination.
     * This does not mean that it's started landing or will soon
     * 
     */
    public int getReachDestinationTick() {
        return this.reachDestinationTick;
    }
    
    public BlockPos getCurrentLocation() {
        return this.location;
    }
    
    public BlockPos getDestinationPosition() {
        return this.destination;
    }
    
    public RegistryKey<World> getCurrentDimension() {
        return this.dimension;
    }
    
    public RegistryKey<World> getDestinationDimension() {
        return this.destinationDimension;
    }
    /**
     * Gets the Exterior facing direction that the exterior is told to be, but not necessarily applied to the exterior block
     * @return
     */
    public Direction getExteriorFacingDirection() {
        return this.facing == Direction.DOWN || this.facing == Direction.UP ? facing = Direction.NORTH : facing;
    }
    
    public SpaceTimeCoord getPositionInFlight() {

        BlockPos diff = WorldHelper.scaleBlockPos(destination.subtract(this.takeoffLocation), this.getPercentageJourney());

        return new SpaceTimeCoord(this.getPercentageJourney() < 0.5 ? this.dimension : this.destinationDimension, takeoffLocation.add(diff), this.facing);
    }
    
    /**
     * If the Tardis is able to start flight, or is being towed by another Tardis
     * @return
     */
    public boolean canFly() {
        
        if(this.isBeingTowed)
            return true;
        
        for(Subsystem s : this.subsystems) {
            if(s.stopsFlight()) {
                return false;
            }
        }
        if (this.shouldStartChangingInterior && this.interiorManager.isInteriorStillRegenerating()) {
            return false;
        }
        return this.artron > 0;
    }
    
    
    /**
     * If the Tardis can send new flight events
     * <br> True if not landing and not stabilized
     * @return
     */
    public boolean canGiveNewEvent() {
        
        if(this.isBeingTowed)
            return false;
        
        StabilizerSubsystem sys = this.getSubsystem(StabilizerSubsystem.class).orElse(null);
        
        if(sys == null || sys.isControlActivated())
            return false;
        
        return this.landTime <= 0 &&
                this.flightTicks < this.reachDestinationTick;
        
    }
    /**
     * Set the Exterior facing direction, which is applied on landing
     * @param dir
     */
    public void setExteriorFacingDirection(Direction dir) {
        if(dir != Direction.DOWN && dir != Direction.UP) {
            this.facing = dir;
            this.markDirty();
            this.updateClient();
        }
    }
    
    /**
     * Update the Tardis' current position and dimension
     * @param worldKey
     * @param location
     */
    public void setCurrentLocation(RegistryKey<World> worldKey, BlockPos location) {
        this.dimension = worldKey;
        this.location = location.toImmutable();
        this.markDirty();
        this.updateClient();
    }
    
    /**
     * Gets the percentage of the Journey
     * @return double between 0 and 1
     */
    public double getPercentageJourney() {
        return this.reachDestinationTick == 0 ? 0 : MathHelper.clamp(this.flightTicks / (double)this.reachDestinationTick, 0.0, 1.0);
    }
    
    
    /*
     * To be used by packets *ONLY*, so help me God...
     */
    public void setFlightTicks(int ticks) {
        this.flightTicks = ticks;
        this.updateClient();
    }
    
    public AbstractSoundScheme getSoundScheme() {
        return this.scheme;
    }
    
    public void setSonicItem(ItemStack sonic) {
        this.sonic = sonic;
        this.markDirty();
        this.updateClient();
    }
    
    public ItemStack getSonicItem() {
        return this.sonic;
    }
    
    public void setCoordIncr(int incr) {
        this.coordIncr = incr;
        this.markDirty();
        this.updateClient();
    }
    
    public int getCoordIncr() {
        return this.coordIncr;
    }
    
    public float getArtron() {
        return this.artron;
    }
    
    public float getMaxArtron(){
        return this.max_artron;
    }
    
    public void setArtron(float artron) {
        if (artron < this.max_artron) {
            if(artron > 0)
                this.artron = artron;
            else this.artron = 0;
        }
        else {
            this.artron = this.max_artron;
        }
        this.markDirty();
        this.updateClient();
    }
    
    public void setMaxArtron(float maxArtron) {
        this.max_artron = maxArtron;
    }
    
    public BlockPos randomizeCoords(BlockPos pos, int radius) {
        int x = -radius + (rand.nextInt(radius * 2));
        int y = -radius + (rand.nextInt(radius * 2));
        int z = -radius + (rand.nextInt(radius * 2));
        return pos.add(x, y < TardisConstants.WORLD_MIN_HEIGHT ? 5 : y, z).toImmutable();
    }
    
    public void scaleDestination() {
        
        //Demat if landing
        if(this.isInFlight() && this.landTime > 0) {
            ExteriorTile ext = this.exterior.getExteriorTile(this);
            if(ext != null)
                ext.demat(this.getSoundScheme().getTakeoffTime());
        }
        
        double per = this.getPercentageJourney();
        if(per < 0)
            this.destination = this.getCurrentLocation();
        
        //Reset dimension if less than half way there
        if(per < 0.5)
            this.destinationDimension = this.dimension;
        
        BlockPos diff = this.destination.subtract(this.getCurrentLocation());
        this.destination = this.getCurrentLocation().add(new BlockPos(diff.getX() * per, diff.getY() * per, diff.getZ() * per)).toImmutable();
    }
    
    /**
     * Get map of systems that are using Artron in the Tardis
     * @return
     */
    public Map<IArtronType, ArtronUse> getArtronUses(){
        return this.artronUses;
    }
    
    public void setSoundScheme(AbstractSoundScheme scheme) {
        this.scheme = scheme;
    }
    
    public ArtronUse getOrCreateArtronUse(IArtronType type) {
        if(this.artronUses.containsKey(type)) {
            return this.artronUses.get(type);
        }
        else {
            ArtronUse use = new ArtronUse(type);
            this.artronUses.put(type, use);
            return use;
        }
    }
    
    public float calcFuelUse() {
        
        float use = BASIC_FUEL_USEAGE;
        
        ThrottleControl throttle = this.getControl(ThrottleControl.class).orElse(null);
        if(throttle != null)
            use *= throttle.getAmount() * 0.025F;
        
        StabilizerSubsystem sys = this.getSubsystem(StabilizerSubsystem.class).orElse(null);
        if(sys != null)
            use *= sys.isControlActivated() ? 1 : 0.5F;
        return use;
    }
    
    /**
     * Calculate flight speed for the Tardis
     * @return Speed in blocks a tick B/T
     */
    public float calcSpeed() {
        ObjectWrapper<Float> throttle = new ObjectWrapper<>(0.0F);
        this.getControl(ThrottleControl.class).ifPresent(throt -> throttle.setValue(throt.getAmount()));
        return ConsoleTile.TARDIS_MAX_SPEED * MathHelper.clamp(throttle.getValue(), 0.1F, 1.0F);
    }
    
    public void updateArtronValues() {
        
        this.world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            
            float newMax = 0;
            float rate = 0;
            
            int numCap = 0;
            
            PanelInventory inv = cap.getEngineInventoryForSide(Direction.WEST);
            for(int i = 0; i < inv.getSlots(); ++i) {
                ItemStack stack = inv.getStackInSlot(i);
                if(stack.getItem() instanceof ArtronCapacitorItem) {
                    ArtronCapacitorItem item = (ArtronCapacitorItem)stack.getItem();
                    newMax += item.getMaxStorage();
                    rate += item.getRechargeModifier();
                    ++numCap;
                }
            }
            
            this.max_artron = newMax;
            
            this.rechargeMod = (rate / (float)numCap);
            
            if(artron > this.max_artron)
                this.artron = this.max_artron;
            
        });
    }
    /**
     * Determine if administrative functions of the Tardis are able to be used by any of its pilots
     * <br> If the Tardis is not loyal enough to a pilot, return false
     * @return
     */
    public boolean areAdminFunctionsLocked() {
        
        for(UUID id : this.getEmotionHandler().getLoyaltyTrackingCrew()) {
            if(this.getEmotionHandler().getLoyalty(id) > 50)
                return true;
        }
        
        return false;
    }
    
    public boolean canDoAdminFunction(PlayerEntity entity) {
        if(!this.areAdminFunctionsLocked())
            return true;
        return this.getEmotionHandler().getLoyalty(entity.getUniqueID()) > 50;
    }
    
    //Doesn't include landing or take off
    public int calcFlightTicks(boolean usePositionInFlight) {


        float dist = (float) Math.sqrt(destination.distanceSq(location));
        float mod = this.calcSpeed();
        int time = (int)(dist / mod);
        
        if(this.destinationDimension != this.dimension)
            time += (30 * 20);

        int offset = 0; //Tick offset to prevent TARDIS from landing on top of itself and getting redirected

        return time <= 0 ? offset : (time + offset);
    }
    
    /** Only to be used in flight */
    public void updateFlightTime() {
        this.reachDestinationTick = 0 + this.flightTicks + this.calcFlightTicks(true);
        this.landTime = 0;
        this.markDirty();
        this.updateClient();
    }
    
    public void setCustomName(String name) {
        this.customName = name;
        this.markDirty();
    }
    
    public String getCustomName() {
        return this.customName;
    }

    public void addDistressSignal(DistressSignal coord) {
        this.getSubsystem(AntennaSubsystem.class).ifPresent(sys -> {
            if(sys.canBeUsed()) {
                this.distressSignal.add(coord);
                this.markDirty();
                this.updateClient();
            }
        });
    }
    /**
     * Gets the previous location for the Fast Return control
     * @return
     */
    public SpaceTimeCoord getReturnLocation() {
        return this.returnLocation;
    }
    public FlightEvent getFlightEvent() {
        return this.currentEvent;
    }
    
    public void setFlightEvent(@Nullable FlightEvent event) {
        this.currentEvent = this.isBeingTowed ? event = null : event;
        if(!world.isRemote) {
            if(event != null)
                event.warnPlayers(world, this.getPos());
            this.updateClient();
        }
        this.markDirty();
    }
    
    public List<DistressSignal> getDistressSignals(){
        return this.distressSignal;
    }
    /** Sets the current pilot id to that of the specifided player*/
    public void setPilot(PlayerEntity player) {
        this.pilot = player;
    }
    /**
     * Get the current player piloting the Tardis
     * <br> There can be many players who can pilot one Tardis
     * @return
     */
    public PlayerEntity getPilot() {
        return this.pilot;
    }
    
    /**
     * Get texture variants for Exterior/Consoles
     * @return
     */
    public TexVariant[] getTextureVariants() {
        return this.variants;
    }
    
    @Nullable
    public TexVariant getVariant() {
        if(this.variant < this.variants.length)
            return this.variants[this.variant];
        return null;
    }
    
    public void setVariant(int index) {
        if(index < this.variants.length)
            this.variant = index;
        this.markDirty();
    }

    private void findNewMission() {
        if(!world.isRemote) {
        	ServerWorld otherWorld = world.getServer().getWorld(dimension);
        	if (otherWorld.getCapability(Capabilities.MISSION).isPresent())
                this.startMission(MissionRegistry.getRandomMissionType(this.world.rand));
        }
    }
    
    public void startMission(MiniMissionType type) {
        if(!world.isRemote()) {
        	if (type != null) {
        		int searchRadiusBlocks = 48; //TODO: 50ap5ud5: Maybe make this as a field inside the MiniMission so we call it and make the search radius different for each mission?
        		int searchRadiusChunks = (int)(((double)searchRadiusBlocks / (double)16)); //Search Radius in Chunks. This must not be a very high number because this gets multiplied with the Structure Spacing values.
        		ServerWorld sWorld = world.getServer().getWorld(dimension);
                //Get the instance of the Structure on the Server via DynamicRegistries.
                Structure<?> missionStructure = sWorld.getServer().getDynamicRegistries().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getOrThrow(type.getStructureKey()).field_236268_b_;
                BlockPos mission = type.spawnAndFindNearestMission(sWorld, sWorld.getStructureManager(), getCurrentLocation(), searchRadiusChunks, false, sWorld.getSeed(), sWorld.getChunkProvider().getChunkGenerator().func_235957_b_().func_236197_a_(missionStructure));
                if (mission != null) {
                	List<BlockPos> possibleLandSpots = BlockPosHelper.getFilteredBlockPositionsInStructure(mission, sWorld, sWorld.getStructureManager(), missionStructure, TBlocks.landing_pad.get());
    	            BlockPos landSpot;
    	            if (!possibleLandSpots.isEmpty()) {
    	                int index = sWorld.rand.nextInt(possibleLandSpots.size());
    	                landSpot = possibleLandSpots.get(index);//Get a random possible landing spot
    	            }
    	            else {
    	                landSpot = mission;
    	            }
                	sWorld.getCapability(Capabilities.MISSION).ifPresent(mCap -> {
                	    boolean isNewMission = false;
                	    BlockPos possibleMission = landSpot;
                	    if (possibleMission.getY() == 0) {
                	        //Extra processing to try to get the top most solid block in this dimension
                    	    possibleMission = LandingSystem.getLandSpotUp(sWorld, mission, this.exterior, this);
                	    }
                		if(mCap.getMissionForPos(possibleMission) != null) { //If we can find a mission at this position, check if it's complete
                	        if (!mCap.getMissionForPos(possibleMission).isMissionComplete()) {
                	            isNewMission = true;
                	        }
                	    }
                		else {
                		    isNewMission = true; //If we still can't find a mission at this position, but the structure for the mission has spawned (perhaps it spawned at y level 0), allow the distress signal to be sent
                		}
                		if (isNewMission) {
                		    if (!possibleMission.equals(BlockPos.ZERO))
            	                this.addDistressSignal(new DistressSignal(new TranslationTextComponent(Translations.MISSION_DISTRESS_SIGNAL).getString(), new SpaceTimeCoord(this.dimension, possibleMission), type));
                		}
                	});
                }
        	}
        }
    }
    
    public List<Subsystem> getSubSystems(){
        return this.subsystems;
    }
    
    public List<Upgrade> getUpgrades(){
        return this.upgrades;
    }
    

    public Optional<Subsystem> getSubsystem(ResourceLocation key) {
        for(Subsystem s : this.getSubSystems()) {
            if(s.getEntry().getRegistryName().equals(key))
                return Optional.of(s);
        }
        return Optional.empty();
    }
    

    @SuppressWarnings("unchecked")
    public <T extends Subsystem> LazyOptional<T> getSubsystem(Class<T> clazz) {
        for(Subsystem sys : this.getSubSystems()) {
            if(sys.getClass() == clazz)
                return LazyOptional.of(() -> (T)sys);
        }
        return LazyOptional.empty();
    }
    
    
    @SuppressWarnings("unchecked")
    public <T extends Upgrade> LazyOptional<T> getUpgrade(Class<T> clazz) {
        for(Upgrade upgrade : upgrades) {
            if(upgrade.getClass() == clazz)
                return (LazyOptional<T>)LazyOptional.of(() -> upgrade);
        }
        return LazyOptional.empty();
    }

    public Optional<Upgrade> getUpgrade(ResourceLocation loc){
        for(Upgrade upgrade : this.upgrades){
            if(upgrade.getEntry().getRegistryName().equals(loc))
                return Optional.of(upgrade);
        }
        return Optional.empty();
    }
    
    public void setLandingCode(String code) {
        this.landingCode = code;
        this.markDirty();
    }
    
    public String getLandingCode() {
        return this.landingCode;
    }

    public boolean getAntiGrav() {
        return this.antiGravs;
    }

    public void setAntiGrav(boolean enabled) {
        this.antiGravs = enabled;
        if(!world.isRemote) {
            ExteriorTile tile = this.getExteriorType().getExteriorTile(this);
            if(tile != null) {
                tile.setAntiGravs(enabled);
            }
            TardisEntity ent = this.getEntity();
            if(ent != null)
                ent.remove();
        }
        this.markDirty();
    }
    /**
     * Gets the entity version of the Tardis
     * @return
     */
    @Nullable
    public TardisEntity getEntity() {
        if(this.tardisEntityID == null)
            return this.tardisEntity;
        
        if(this.tardisEntity != null && !this.tardisEntity.isAlive())
            return this.tardisEntity;
        
        if(!world.isRemote) {
            ServerWorld sw = world.getServer().getWorld(getCurrentDimension());
            return this.tardisEntity = (TardisEntity)sw.getEntityByUuid(tardisEntityID);
        }
        return null;
    }
    
    public void setEntity(@Nullable TardisEntity ent) {
        this.tardisEntity = ent;
        if(ent != null)
            this.tardisEntityID = ent.getUniqueID();
        else this.tardisEntityID = null;
    }

    /**
     * Logic for when the Tardis' light level goes down.
     */
    public void onPowerDown(boolean shutDown) {
    	this.hasPoweredDown = shutDown;
    	if(!world.isRemote) {
        	this.getInteriorManager().setLight(shutDown ? 0 : 15);
        	if (shutDown)
                world.playSound(null, this.getPos(), TSounds.POWER_DOWN.get(), SoundCategory.BLOCKS, 20F, 1F);
        }
    }
    /**
     * Determine if the Tardis has a Navcom, and thus, able to control its coordinates
     * @return
     */
    public boolean hasNavCom() {
        return this.hasNavCom;
    }
    
    public void setNavCom(boolean hasCom) {
        this.hasNavCom = hasCom;
        this.markDirty();
    }
    
    public boolean isBeingTowed() {
        return this.isBeingTowed;
    }
    /**
     * If the interior dimension does not contain any players inside.
     * @return
     */
    public boolean doesConsoleWorldHaveNoPlayers(){
        if(world.isRemote)
            return false;
        return world.getPlayers().isEmpty();
    }
    
    /** If we should start changing the interior
     * <p> This does not actually change the interior, we must detect that there are no players inside the tardis before we start doing so
     * <br> If you want to check if the console can actually change interior, use {@link ConsoleTile#canStartToChangeInterior()}*/
    public boolean shouldStartChangingInterior() {
        return this.shouldStartChangingInterior;
    }

    /** Tell the console that we want to start changing interior
     * <br> This doesn't actually start changing the interior, we will check if there are no players inside the Tardis*/
    public void setStartChangingInterior(boolean startChangingInterior) {
        this.shouldStartChangingInterior = startChangingInterior;
        this.markDirty();
        this.updateClient();
    }
    
    /** Check if we can actually start changing interiors. 
     * If there are no players inside and we are told to start changing interiors, go ahead and change interiors
     * <br> Called from {@link InteriorManager#tick(ConsoleTile)}*/
    public boolean canStartToChangeInterior() {
        return this.shouldStartChangingInterior && this.doesConsoleWorldHaveNoPlayers();
    }
    
    /** Get the next console room which the Tardis is about to change into
     * <br> Must not be null
     * */
    public ConsoleRoom getNextConsoleRoom() {
        return this.nextRoomToChange;
    }
    
    public void setNextConsoleRoomToChange(ConsoleRoom room) {
        this.nextRoomToChange = room;
        this.markDirty();
        this.updateClient();
    }
    
    /**
     * Set and prepare the properties for the interior change
     * @param roomToSpawn
     * @param cancelProcess - if we should be cancelling the interior change process
     * @param isInstantChange - if the room should instantly change when the players exit the Tardis 
     */
    public void setupInteriorChangeProperties(ConsoleRoom roomToSpawn, boolean cancelProcess, boolean isInstantChange) {
        int processingTicks = TConfig.SERVER.interiorChangeProcessTime.get() * 20;
        //If we want "instant" change, minimum ticks should be no lower than 20 ticks (1 second). 
        //If the time is lower than this, the console will not be able to serialise things that quickly, and the deadlock kicking mechanic will loop forever.
        this.getInteriorManager().setInteriorProcessingTime(cancelProcess ?  InteriorManager.resetInteriorChangeProcessTime :  (isInstantChange ? 60 : processingTicks)); //Set how much time to elapse before the console can fully finish its interior change
        this.setNextConsoleRoomToChange(cancelProcess ? this.consoleRoom : roomToSpawn); //Set the next room to be the one inputted
        this.setStartChangingInterior(cancelProcess ? false : true);
        this.onPowerDown(!cancelProcess);
        //Handle edge case of fuel where player somehow gets inside and cancels the interior change process
        //We want to stop the Tardis from continuing to use fuel if the change process is already underway
        if (cancelProcess) {
            //Reset the interior change fuel usage
            ArtronUse use = this.getOrCreateArtronUse(ArtronType.INTERIOR_CHANGE);
            use.setArtronUsePerTick(0);
            use.setTicksToDrain(0);
            this.getDoor().ifPresent(door -> {
                door.setAdditionalLockLevel(0);
                door.setLocked(false);
            });
            this.getOrFindExteriorTile().ifPresent(ext -> {
                ext.setAdditionalLockLevel(0);
                ext.setLocked(false);
                ext.setInteriorRegenerating(false);
            });
        }
        else {
        	//Setup the Artron Use before the player exits the doors, but don't let it start yet by not setting the ticks
            int fuelUsage = TConfig.SERVER.interiorChangeArtronUse.get();
            ArtronUse artronUse = this.getOrCreateArtronUse(ArtronType.INTERIOR_CHANGE);
            artronUse.setArtronUsePerTick((float)((float)fuelUsage/(float)processingTicks));
        }
        AxisAlignedBB radius = new AxisAlignedBB(this.getPos()).grow(30);
        for (PlayerEntity player : this.getWorld().getEntitiesWithinAABB(PlayerEntity.class, radius)){
            if (player != null) {
                if (cancelProcess) {
                    player.sendStatusMessage(Translations.CANCEL_INTERIOR_CHANGE, false);
                }
                else {
                   player.sendStatusMessage(new TranslationTextComponent(Translations.START_INTERIOR_CHANGE, this.nextRoomToChange.getRegistryName().toString()), false);
                   player.playSound(TSounds.TARDIS_SHUT_DOWN.get(), SoundCategory.PLAYERS, 0.6F, 1F);
                }
            }
        }//Warn each player within Tardis interior to exit the Tardis to start the interior change
    }
    
    /**
     * Actually start the back end interior change process
     * <br> Called when the player is exiting the Tardis
     * @param destWorld - the world we will be teleporting players to if they are still inside the interior
     */
    public void startInteriorChangeProcess(ServerWorld destWorld) {
        destWorld.getServer().enqueue(new TickDelayedTask(20, () -> {
            if (!WorldHelper.areDimensionTypesSame(destWorld, TDimensions.DimensionTypes.TARDIS_TYPE)) {
                if (this.getInteriorManager().isInteriorStillRegenerating()) {
                	
                	//Start draining fuel from the Tardis
                    int processingTime = getInteriorManager().getInteriorProcessingTime();
                    ArtronUse use = this.getOrCreateArtronUse(ArtronType.INTERIOR_CHANGE);
                    use.setTicksToDrain(processingTime);
                    this.artronUses.put(ArtronType.INTERIOR_CHANGE, use);

                	//Teleport any remaining players who are still inside, to the exterior
                    List<? extends PlayerEntity> players = this.getWorld().getPlayers();
                    for (PlayerEntity player : players) {
                        this.relocatePlayerToExterior(player, destWorld);
                    }
                    this.onPowerDown(false); //Set the lights back to full before we change the actually interior so that this new value is transferred to the new console tile
                    if (this.nextRoomToChange != null) {
                        ServerWorld consoleWorld = this.getWorld().getServer().getWorld(this.getWorld().getDimensionKey());
                        this.setConsoleRoom(this.nextRoomToChange);
                        //Spawn the room after no players are inside, the remaining time is just to let the player wait
                        this.consoleRoom.spawnConsoleRoom(consoleWorld, false);
                    }
                    this.getOrFindExteriorTile().ifPresent(ext -> {
                        ext.setInteriorRegenerating(true);
                        ext.setDoorState(EnumDoorState.CLOSED);
                        ext.setLocked(true);
                        ext.setAdditionalLockLevel(1);
                        ext.copyDoorStateToInteriorDoor();
                        ext.updateSpecific(DoorData.create(ext));
                        ext.getWorld().playSound(null, getPos(), this.exterior.getDoorSounds().getClosedSound(), SoundCategory.BLOCKS, 0.5F, 1F);
                        ext.getWorld().playSound(null, getPos(), TSounds.DOOR_LOCK.get(), SoundCategory.BLOCKS, 0.5F, 1F);
                    });
                }
            }
        }));
    }
    /** Handle logic for when the interior change has completed*/
    public void handleInteriorChangeComplete() {
        this.setStartChangingInterior(false); //Tell the console it is no longer changing interior
        this.getOrFindExteriorTile().ifPresent(ext -> {
            ext.setInteriorRegenerating(false);
            ext.setLocked(false);
            ext.setAdditionalLockLevel(0);
            ext.copyDoorStateToInteriorDoor();
            ext.updateSpecific(DoorData.create(ext));
            ext.getWorld().playSound(null, getPos(), TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 0.5F, 1F);
        });
        ServerWorld consoleWorld = this.getWorld().getServer().getWorld(this.getWorld().getDimensionKey());
        TextComponent tardisDim = TextHelper.getTardisDimObject(consoleWorld);
        for (UUID id : this.getEmotionHandler().getLoyaltyTrackingCrew()) {
            ServerPlayerEntity player = this.getWorld().getServer().getPlayerList().getPlayerByUUID(id);
            if (player != null) { //Make sure the player is online in the server before trying to contact them
                player.sendStatusMessage(new TranslationTextComponent(Translations.INTERIOR_CHANGE_COMPLETE, tardisDim), false);
                player.playSound(TSounds.TARDIS_LAND_NOTIFICATION.get(), SoundCategory.PLAYERS, 0.6F, 1F);
            }
        }
        //Reset interior change progress and start the cooldown
        int ticks = TConfig.SERVER.interiorChangeCooldownTime.get() * 20;
        this.getInteriorManager().setInteriorCooldownTime(ticks);
        this.getInteriorManager().setInteriorProcessingTime(InteriorManager.resetInteriorChangeProcessTime);
        
        ArtronUse use = this.getOrCreateArtronUse(ArtronType.INTERIOR_CHANGE);
        use.setArtronUsePerTick(0F);
        use.setTicksToDrain(0);
        this.artronUses.put(ArtronType.INTERIOR_CHANGE, use);
    }
    /**
     * Teleports the player to the exterior position. This is 0 128 0 by default if the tardis has not been flown and updated its location
     * <br> Make sure to add a tick delayed task for this to prevent a NPE that can occur
     * @param player
     * @param destWorld
     * @return
     */
    public boolean relocatePlayerToExterior(PlayerEntity player, ServerWorld destWorld) {
        if (player != null && destWorld != null) { //Handle if player got disconnected or something
            Direction dir = this.getTrueExteriorFacingDirection();
            float realFacing = WorldHelper.getAngleFromFacing(dir.getOpposite()) + player.rotationYaw;
            BlockPos pos = this.getCurrentLocation().offset(dir).offset(dir); //Move two blocks away from exterior in case the door is open. This prevents an infinite teleport loop if some logic screws up
            player.rotationYaw = WorldHelper.getAngleFromFacing(dir.getOpposite());
            WorldHelper.teleportEntities(player, destWorld, pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, realFacing, player.rotationPitch);
            return true;
        }
        return false;
    }
    
    public boolean relocatePlayerIfExteriorDeadlocked(ServerPlayerEntity player, World world) {
        ExteriorTile ext = this.getExteriorType().getExteriorTile(this);
        ServerWorld destWorld = world.getServer().getWorld(this.getCurrentDimension());
        boolean shouldEjectPlayer = true;
        if (ext != null && ext.isExteriorDeadLocked()) {
            //Kick if not creative and not spectator
            if (player.isCreative() || player.isSpectator()) {
                shouldEjectPlayer = false;
            }
            if (shouldEjectPlayer) {
                world.getServer().enqueue(new TickDelayedTask(1, () -> {
                    this.relocatePlayerToExterior(player, destWorld);
                    player.sendStatusMessage(ExteriorBlock.DEADLOCKED, false);
                }));
            }
        }
        return shouldEjectPlayer;
    }

    public void updateTrackingClients(ConsoleData data) {
        //if On the server
        if(!world.isRemote){
            Network.sendToTrackingTE(new ConsoleUpdateMessage(data.getDataType(), data), this);
        }
    }
    
    /**
     *  Force loads the chunk which the console sits in.
     * <br> Only call on logical Server
     * @param forceLoad - true if we want to forceload
     * @param clearForcedChunksBeforeReforce - True if we want to clear all other interior chunks before only forceloading the console chunk
     */
    public boolean forceLoadInteriorChunk(boolean forceLoad, boolean clearForcedChunksBeforeReforce) {
        if (!world.isRemote()) {
            ServerWorld sWorld = world.getServer().getWorld(world.getDimensionKey());
            BlockPos tardisPos = new BlockPos(TardisHelper.TARDIS_POS);
            ChunkPos cPos = new ChunkPos(tardisPos);
            if (clearForcedChunksBeforeReforce)
                WorldHelper.preLoadTardisInteriorChunks(sWorld, false); //remove the chunks that were loaded during teleportation/exterior door opening
            if(forceLoad)
                return WorldHelper.forceChunkIfNotLoaded(sWorld, cPos, tardisPos);
            else
                return WorldHelper.unForceChunkIfLoaded(sWorld, cPos, tardisPos);
        }
        return false;
    }
    
    /** Handle automatic loading or unloading of interior and exterior world chunks if there are no players inside it, if the interior is not changing and if we are not in flight*/
    private void handleAutoLoadOrUnloadChunks() {
        if(!world.isRemote() && world.getGameTime() % 60 == 0) {
//            if (this.doesConsoleWorldHaveNoPlayers() && !this.isInFlight() && this.hasForcedChunksToRemove && !this.interiorManager.isInteriorStillRegenerating()) {
//            	this.forceLoadInteriorChunk(false, false);
//                this.hasForcedChunksToRemove = false;
//            }
//            else if (!this.doesConsoleWorldHaveNoPlayers()) {
//                if (!this.hasForcedChunksToRemove) {
//                    this.forceLoadInteriorChunk(true, true);
//                    this.hasForcedChunksToRemove = true;
//                }
//            }
        	//New Logic: Always load the chunk the console sits in, but remove all other force-loaded chunks used to find the interior door for performance reasons.
        	if (this.doesConsoleWorldHaveNoPlayers() && !this.isInFlight() && this.hasForcedChunksToRemove && !this.interiorManager.isInteriorStillRegenerating()) {
        		this.forceLoadInteriorChunk(true, true);
                this.hasForcedChunksToRemove = false;
            }
            else if (!this.doesConsoleWorldHaveNoPlayers()) {
                if (!this.hasForcedChunksToRemove) {
                    this.hasForcedChunksToRemove = true; //If we do have players inside the Tardis, we need to remove the excess force-loaded chunks later on when there are no players inside.
                }
            }
//            Tardis.LOGGER.debug("Attempting to remove chunk ticket after no players found in Tardis Interior. Pos: {} World: {}", pos, world.getDimensionKey().getLocation());
//          Tardis.LOGGER.debug("Does {} have forced chunks after being empty? Counted: {}", sWorld.getDimensionKey().getLocation(), WorldHelper.getTickingBlockForcedChunks(sWorld) != null ? WorldHelper.getTickingBlockForcedChunks(sWorld).size() : "None");
        }
    }

    /**
     *  Force loads the chunk which the exterior sits in.
     * <br> Only call on logical Server
     * @param forceLoad - true if we want to forceload
     */
    public boolean forceLoadExteriorChunk(boolean forceLoad) {
        if (!world.isRemote()) {
            if (this.getOrFindExteriorTile().isPresent()) {
            	ExteriorTile exterior = this.getOrFindExteriorTile().orElseThrow(() -> new IllegalStateException("Cannot find exterior to chunkload for " + this.world.getDimensionKey().getLocation().toString()));
            	ServerWorld sWorld = world.getServer().getWorld(exterior.getWorld().getDimensionKey());
            	BlockPos extPos = exterior.getPos();
                ChunkPos cPos = new ChunkPos(extPos);
                if(forceLoad)
                    return WorldHelper.forceChunkIfNotLoaded(sWorld, cPos, extPos);
                else
                    return WorldHelper.unForceChunkIfLoaded(sWorld, cPos, extPos);
            }
            else {
            	return false;
            }
        }
        return false;
    }
    
    private void debugForcedChunksInDim(RegistryKey<World> worldKey) {
    	ServerWorld otherWorld = world.getServer().getWorld(worldKey);
        Tardis.LOGGER.debug("Does {} have forced chunks? Counted: {}", otherWorld.getDimensionKey().getLocation(), WorldHelper.getTickingBlockForcedChunks(otherWorld) != null ? WorldHelper.getTickingBlockForcedChunks(otherWorld).size() : "None");
    }
}