package net.tardis.mod.tileentities.machines;


import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.recipe.AlembicRecipe;
import net.tardis.mod.recipe.AlembicRecipe.ResultType;
import net.tardis.mod.tileentities.TTiles;

public class AlembicTile extends TileEntity implements ITickableTileEntity{

    public static final int MAX_WATER = 1000;
    public static final int MAX_NON_WATER = 1000;
    private ItemStackHandler handler = new ItemStackHandler(6);
    private FluidTank waterTank = new FluidTank(MAX_WATER, stack -> stack.getFluid() == Fluids.WATER);
    private int progress = 0;
    private int maxProgress = 200;
    private int burnTime = 0;
    private int maxBurnTime = 1;
    private int nonWaterFluidAmount = 0;
    private int maxNonWaterFluidCapacity = MAX_NON_WATER;
    
    private AlembicRecipe recipe;

    private AlembicRecipe fluidCollectionRecipe;
    private int fluidCollectionProgress = 0;
    private int fluidCollectionMaxProgress = 200;
    
    public AlembicTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }
    
    public AlembicTile() {
        super(TTiles.ALEMBIC.get());
    }

    @Override
    public void tick() {
        if(this.burnTime > 0) {

            //Reset progress if recipe has changed
            AlembicRecipe newRecipe = this.getRecipe();
            if(newRecipe != recipe) {
                if (newRecipe != null) {
                    if (newRecipe.getResultType() != ResultType.FLUID_COLLECTION) {
                    	this.recipe = newRecipe; //Only assign it the new recipe if it's not a fluid collection. Stops the input stack being instantly used up
                        this.progress = 0;
                    }
                }
            }   
            
            //If can process this recipe
            if(this.shouldAdvanceProgress()) {
                --this.burnTime;
                //Recipe work
                ++this.progress;
                world.setBlockState(pos, this.getBlockState().with(BlockStateProperties.ENABLED, true), 2);
                this.maxProgress = recipe.getRequiredProgressTicks();
                if(this.progress >= maxProgress) {
                    this.handleCraftComplete();
                }
            }
            else {
                this.progress = 0;
            }
        }
        this.handleFuelSlot();
        this.handleWaterSlots();
        this.handleFluidCollectionBottleSlots();
    }
    
    private void handleFuelSlot() {
        //Handle fuel items
        int fuelBurnTime = ForgeHooks.getBurnTime(this.handler.getStackInSlot(3), null);
        if(this.burnTime <= 0 && fuelBurnTime > 0 && !this.handler.getStackInSlot(2).isEmpty()) {
            this.burnTime = this.maxBurnTime = fuelBurnTime;
            this.handler.extractItem(3, 1, false);
            this.markDirty();
        }
    }
    
    private void handleWaterSlots() {
        //Handle tank filling
        this.handler.getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).ifPresent(cap -> {
            //If a water holder is in the top tank slot
            if(cap.getFluidInTank(0).getFluid().isIn(FluidTags.WATER)) {
                //If it can drain from it
                FluidStack drainResult = cap.drain(new FluidStack(Fluids.WATER, this.waterTank.getSpace()), FluidAction.EXECUTE); 
                if(!drainResult.isEmpty()) {
                    this.waterTank.fill(drainResult, FluidAction.EXECUTE);
                    //Move the container to the bottom slot and decrement the top one
                    this.handler.insertItem(1, cap.getContainer().copy(), false);
                    this.handler.getStackInSlot(0).shrink(1);
                    this.markDirty();
                }
            }
        });
    }
    
    private void handleFluidCollectionBottleSlots() {
        ItemStack fluidCollectionStack = this.handler.getStackInSlot(4);
        if (!fluidCollectionStack.isEmpty() && this.handler.getStackInSlot(5).isEmpty()) { //Make sure the output slot is empty and there is an item in the input slot first
            AlembicRecipe newRecipe = this.getRecipe();
            if (newRecipe != fluidCollectionRecipe) {
                if (newRecipe != null) {
                    //Only change the current recipe if we've found a valid fluid collection type
                    if (newRecipe.getResultType() == ResultType.FLUID_COLLECTION) {
                        this.fluidCollectionRecipe = newRecipe;
                    }
                }
            }
            if (fluidCollectionRecipe != null) {
                //Explicity check if it's our fluid collection result type.
                //Otherwise don't mess with the progress
                if (fluidCollectionRecipe.getResultType() == ResultType.FLUID_COLLECTION) {
                    this.fluidCollectionMaxProgress = fluidCollectionRecipe.getRequiredProgressTicks();
                    ++this.fluidCollectionProgress;
                    if (fluidCollectionProgress >= this.fluidCollectionMaxProgress) {
                        this.fluidCollectionProgress = 0;
                        if(fluidCollectionRecipe.getFluidCollectionIngredient().test(fluidCollectionStack)) {
                            if(this.nonWaterFluidAmount >= this.fluidCollectionRecipe.getRequiredNonWaterFluidAmount()) {
                                if (!fluidCollectionRecipe.getResult().isEmpty() && this.nonWaterFluidAmount >= fluidCollectionRecipe.getRequiredNonWaterFluidAmount()) {
                                    if(this.handler.insertItem(5, fluidCollectionRecipe.getResult(), false).isEmpty()) {
                                        this.handler.getStackInSlot(4).shrink(fluidCollectionRecipe.getRequiredIngredientCount());
                                        this.nonWaterFluidAmount -= fluidCollectionRecipe.getRequiredNonWaterFluidAmount();
                                        fluidCollectionRecipe = null;
                                    }
                                }
                            }
                        }
                    }
                    this.markDirty();
                }
            }
        }
    }
    
    private void handleCraftComplete() {
        //Finish
        this.progress = 0;
        //Remove the ingredients from the slot
        this.handler.getStackInSlot(2).shrink(recipe.getRequiredIngredientCount());
        this.waterTank.drain(recipe.getRequiredWater(), FluidAction.EXECUTE);
        recipe.onCraft(this);
        world.playSound(null, getPos(), SoundEvents.BLOCK_BREWING_STAND_BREW, SoundCategory.AMBIENT, 0.25F, 1F);
        world.setBlockState(pos, this.getBlockState().with(BlockStateProperties.ENABLED, false), 2);
        this.markDirty();
    }
    
    @Nullable
    private AlembicRecipe getRecipe() {
        for(AlembicRecipe rec : AlembicRecipe.getAllRecipes(world)) {
            if(rec.matches(this))
                return rec;
        }
        return null;
    }
    
    public boolean shouldAdvanceProgress() {
        //If there is no recipe, cannot continue
        if(recipe == null)
            return false;
        //If we are looking at a special case recipe, ignore this because we will handle this seperately.
        if (recipe.getResultType() == ResultType.FLUID_COLLECTION)
            return false;

        //Don't start to add more mercury/non water fluid if we are already at max capacity
        if (recipe.getResultType() == ResultType.FLUID) {
            if (this.nonWaterFluidAmount >= this.maxNonWaterFluidCapacity) {
                return false;
            }
        }

        //if output can't be stacked, cannot continue
        if(!this.handler.insertItem(5, recipe.getRecipeOutput(), true).isEmpty())
            return false;
        
        //If recipe doesn't match
        if(!recipe.matches(this))
            return false;
        
        return true;
    }
    
    public FluidTank getWaterTank() {
        return this.waterTank;
    }
    
    public float getWaterPercentage() {
        return (float)this.waterTank.getFluidAmount() / this.waterTank.getCapacity();
    }

    /** Get amount of mercury in the alembic. 
     * <br> Because we haven't registered Mercury as a real fluid, we cannot have use a fluid to get this value
     * <br> This is generically named in case we add allow fluids to be added to the "mercury" tank*/
    public int getNonWaterFluidAmount() {
        return this.nonWaterFluidAmount;
    }
    
    public void setNonWaterFluidAmount(int fluidAmount) {
        this.nonWaterFluidAmount = MathHelper.clamp(fluidAmount, 0, this.maxNonWaterFluidCapacity);
    }
    
    public int getMaxNonWaterFluidCapacity() {
        return this.maxNonWaterFluidCapacity;
    }
    
    public float getNonWaterPercentage() {
        return (float) this.nonWaterFluidAmount / this.maxNonWaterFluidCapacity;
    }
    
    public int getBurnTime() {
        return this.burnTime;
    }
    
    public int getMaxBurnTime() {
        return this.maxBurnTime;
    }
    
    public float getBurnTimePercent() {
        return (float) this.burnTime / this.maxBurnTime;
    }
    
    public float getProgressPercent() {
        return this.progress / (float)maxProgress;
    }
    
    public int getProgress() {
        return this.progress;
    }
    
    public int getMaxProgress() {
        return maxProgress;
    }
    
    public static boolean isFuel(ItemStack stack) {
        return ForgeHooks.getBurnTime(stack) > 0;
    }
    
    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        this.burnTime = compound.getInt("burn_time");
        this.maxBurnTime = compound.getInt("max_burn_time");
        this.progress = compound.getInt("progress");
        this.nonWaterFluidAmount = compound.getInt("mercury");
        this.waterTank.readFromNBT(compound.getCompound("water_tank"));
        this.handler.deserializeNBT(compound.getCompound("inv_handler"));
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt("burn_time", this.burnTime);
        compound.putInt("max_burn_time", this.maxBurnTime);
        compound.putInt("progress", this.progress);
        compound.putInt("mercury", this.nonWaterFluidAmount);
        compound.put("water_tank", this.waterTank.writeToNBT(new CompoundNBT()));
        compound.put("inv_handler", this.handler.serializeNBT());
        return super.write(compound);
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        return new SUpdateTileEntityPacket(this.getPos(), -1, this.serializeNBT());
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        this.deserializeNBT(pkt.getNbtCompound());
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        super.handleUpdateTag(state, tag);

        this.deserializeNBT(tag);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        return this.serializeNBT();
    }
    
    public ItemStackHandler getItemStackHandler() {
        return this.handler;
    }

}
