package net.tardis.mod.tileentities.machines;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RecipeWrapper;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.network.Network;
import net.tardis.mod.recipe.SpectrometerRecipe;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.tileentities.TTiles;

public class NeutronicSpectrometerTile extends TileEntity implements ITickableTileEntity {

    public static final int INPUT_SLOT = 0;
    public static final int SONIC_SLOT = 1;

    private int progressTicks;
    private int maxTicks = 0;
    private int downloadTicks;
    private int maxDownloadTicks = 200;
    private ItemStackHandler inventory = new ItemStackHandler(2);
    private LazyOptional<ItemStackHandler> itemHolder = LazyOptional.of(() -> inventory);
    private List<Schematic> schematics = Lists.newArrayList();
    private SpectrometerRecipe currentRecipe;

    private RecipeWrapper recipeWrapper;

    public NeutronicSpectrometerTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
        this.recipeWrapper = new RecipeWrapper(this.inventory);
    }

    public NeutronicSpectrometerTile(){
        this(TTiles.SPECTROMETER.get());
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        this.schematics.clear();
        this.progressTicks = compound.getInt("progress_ticks");
        this.downloadTicks = compound.getInt("download_ticks");
        ListNBT schematicList = compound.getList("schematics", Constants.NBT.TAG_COMPOUND);
        for (INBT base : schematicList) {
            CompoundNBT nbt = (CompoundNBT)base;
            ResourceLocation key = new ResourceLocation(nbt.getString("schematic_id"));
        	Schematic schematic = Schematics.SCHEMATIC_REGISTRY.get(key);
        	if (schematic != null)
        	    this.schematics.add(schematic);
        }
        this.inventory.deserializeNBT(compound.getCompound("inv_handler"));
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt("progress_ticks", this.progressTicks);
        compound.putInt("download_ticks", this.downloadTicks);
        ListNBT schematicList = new ListNBT();
        for (Schematic schematic : this.schematics) {
        	CompoundNBT schematicLoc = new CompoundNBT();
        	schematicLoc.putString("schematic_id", schematic.getId().toString());
            schematicList.add(schematicLoc);
        }
        compound.put("schematics", schematicList);
        compound.put("inv_handler", this.inventory.serializeNBT());
        return super.write(compound);
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        return Network.createTEUpdatePacket(this);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        this.deserializeNBT(pkt.getNbtCompound());
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        super.handleUpdateTag(state, tag);
        this.deserializeNBT(tag);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        return this.serializeNBT();
    }
    
    @Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? itemHolder.cast() : super.getCapability(cap, side);
	}

	@Override
    public void tick() {

        SpectrometerRecipe newRecipe = this.getRecipe();
        if (newRecipe != null) {
        	if (currentRecipe != newRecipe)
        		currentRecipe = newRecipe;
        }

        if(currentRecipe != null && this.canAdvanceProgress(currentRecipe)){
            this.maxTicks = currentRecipe.getTicks();
            this.maxDownloadTicks = currentRecipe.getDownloadTicks();
            ++this.progressTicks;
            
            if(this.progressTicks >= currentRecipe.getTicks()){
                this.finish(currentRecipe);
            }

        }
        else this.progressTicks = 0;

        this.downloadToSonic();

    }

    public void downloadToSonic(){
        ItemStack sonicStack = this.inventory.getStackInSlot(SONIC_SLOT);
        if(sonicStack.getItem() instanceof SonicItem && !this.schematics.isEmpty()){
            
            ++this.downloadTicks;

            if(this.downloadTicks > this.maxDownloadTicks){

                sonicStack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
                    this.downloadTicks = 0;
                    for(Schematic s : this.schematics){
                        cap.addSchematic(s);
                    }
                    this.schematics.clear();
                    this.markDirty();
                });

            }

        }
        else this.downloadTicks = 0;
    }

    public boolean canAdvanceProgress(SpectrometerRecipe rec){
        return rec.matches(this.recipeWrapper, world);
    }

    public void finish(SpectrometerRecipe recipe){
        this.inventory.extractItem(INPUT_SLOT, 1, false);
        this.schematics.add(recipe.getSchematicObject());
        this.markDirty();
    }

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    @Nullable
    public SpectrometerRecipe getRecipe(){

        for(SpectrometerRecipe rec : world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.SPECTROMETER_TYPE)){
            if(rec != null && rec.matches(this.recipeWrapper, world)){
                return rec;
            }
        }

        return null;

    }

    public float getProgress() {
        return this.maxTicks == 0  ? 0 : this.progressTicks / (float)maxTicks;
    }

    public float getDownloadProgress(){
        return this.downloadTicks / (float)maxDownloadTicks;
    }
    
    public int getProgressTicks() {
    	return this.progressTicks;
    }
    
    public int getMaxTicks() {
    	return this.maxTicks;
    }
    
    public int getDownloadTicks() {
    	return this.downloadTicks;
    }
    
    public int getMaxDownloadTicks() {
    	return this.maxDownloadTicks;
    }
    
    public List<Schematic> getSchematics(){
    	return this.schematics;
    }

    /** Check if the this tile has any schematics stored
     * <br> For client side use in the container gui ONLY */
    public boolean hasSchematics() {
        return !this.schematics.isEmpty();
    }
}
