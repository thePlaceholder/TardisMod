package net.tardis.mod.world.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.tardis.mod.world.structures.TStructures;

public class CratorFeature extends Feature<ProbabilityConfig> {

	public CratorFeature(Codec<ProbabilityConfig> codec) {
        super(codec);
    }

    @Override
    public boolean generate(ISeedReader iSeedReader, ChunkGenerator chunkGenerator, Random rand, BlockPos pos, ProbabilityConfig config) {
        int radius = 6 + rand.nextInt(10);
        pos = iSeedReader.getHeight(Type.WORLD_SURFACE_WG, pos).up(radius / 3);
        if (!iSeedReader.isRemote()) {
            for (BlockPos p : BlockPos.getAllInBoxMutable(pos.add(-radius, -radius, -radius), pos.add(radius, radius, radius))) {
                if (p.withinDistance(pos, radius - 1)) {
            	    if (!iSeedReader.getBlockState(p).isAir(iSeedReader, p))
            	        iSeedReader.setBlockState(p, Blocks.AIR.getDefaultState(), 2);
                }
            }
            return true;
        }
        return false;
    }

}
