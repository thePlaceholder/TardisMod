package net.tardis.mod.world.data;

import net.minecraft.nbt.*;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.storage.WorldSavedData;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.world.WorldGen;
import net.tardis.mod.world.dimensions.TDimensions;

public class TardisWorldData extends WorldSavedData {

    public TardisWorldData(String name) {
        super(name);
        this.markDirty();
    }

    public TardisWorldData() {
        this("tardis");
    }

    @Override
    public void read(CompoundNBT nbt) {
        ListNBT list = nbt.getList("dimensions", NBT.TAG_STRING);
        if (list == null) return;
        TDimensions.TARDIS_DIMENSIONS.clear();
        for (INBT base : list) {
            TDimensions.TARDIS_DIMENSIONS.add(RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(((StringNBT) base).getString())));
        }

        ListNBT brokenExtList = nbt.getList("broken_exteriors", Constants.NBT.TAG_LONG);
        WorldGen.BROKEN_EXTERIORS.clear();
        for (INBT ext : brokenExtList) {
            WorldGen.BROKEN_EXTERIORS.add(BlockPos.fromLong(((LongNBT) ext).getLong()));
        }

    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        ListNBT list = new ListNBT();
        for (RegistryKey<World> loc : TDimensions.TARDIS_DIMENSIONS) {
            list.add(StringNBT.valueOf(loc.getLocation().toString()));
        }
        compound.put("dimensions", list);

        ListNBT exteriors = new ListNBT();
        for (BlockPos pos : WorldGen.BROKEN_EXTERIORS) {
            exteriors.add(LongNBT.valueOf(pos.toLong()));
        }
        compound.put("broken_exteriors", exteriors);

        return compound;
    }

}
