package net.tardis.mod.containers.slot;

import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class RunnableSlot extends SlotItemHandler{
	private Runnable onSlotChanged;
	
	public RunnableSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
	}

	@Override
	public void onSlotChanged() {
		super.onSlotChanged();
		if(onSlotChanged != null)
			onSlotChanged.run();
	}
	
	public void setOnSlotChanged(Runnable run) {
		this.onSlotChanged = run;
	}
}
