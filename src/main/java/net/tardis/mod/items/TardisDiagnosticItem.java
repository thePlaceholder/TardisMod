package net.tardis.mod.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IDiagnostic;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.misc.ConsoleBoundItem;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.DiagnosticMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisDiagnosticItem extends ConsoleBoundItem{
    
	private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.tardis.diagnostic.use"));
	private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.tardis.diagnostic.use2"));
	
	
    public TardisDiagnosticItem(Properties properties) {
        super(properties);
    }

    public static void syncCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.getOrCreateTag().merge(stack.getShareTag());
        }
    }

    public static void readCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.readShareTag(stack.getOrCreateTag());
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (!worldIn.isRemote) {
            playerIn.getHeldItem(handIn).getCapability(Capabilities.DIAGNOSTIC).ifPresent(loc -> loc.onRightClick(worldIn, playerIn, handIn));
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {

        if(!context.getWorld().isRemote()){
            TardisHelper.getConsoleInWorld(context.getWorld()).ifPresent(console -> {
                Network.sendTo(DiagnosticMessage.createFromConsole(console), ((ServerPlayerEntity) context.getPlayer()));
            });
        }

        return super.onItemUse(context);
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (!worldIn.isRemote) {
            if (entityIn instanceof LivingEntity) {
                LivingEntity ent = (LivingEntity) entityIn;
                if (PlayerHelper.isInEitherHand(ent, this))
                    stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(loc -> loc.tick((LivingEntity) entityIn));
            }
        }

    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return oldStack.getItem() != newStack.getItem();
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        if (getTardisName(stack) != null)
        	tooltip.add(new TranslationTextComponent("tooltip.tardis.diagnostic.owner").appendSibling(new StringTextComponent(getTardisName(stack)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        else if (getTardisName(stack) == null)
        	tooltip.add(TardisConstants.Translations.TOOLTIP_NO_ATTUNED);
        tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
        if (Screen.hasControlDown()) {
            tooltip.clear();
            tooltip.add(0, this.getDisplayName(stack));
            tooltip.add(descriptionTooltip);
            tooltip.add(descriptionTooltipTwo);
        }
    }

    @Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) { //sync capability to client
        CompoundNBT tag = stack.getOrCreateTag();
        stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(cap -> tag.put("cap_sync", cap.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if (nbt != null) {
            if (nbt.contains("cap_sync")) {
                stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(cap -> cap.deserializeNBT(nbt.getCompound("cap_sync")));
            }
        }
    }

	@Override
	public ItemStack onAttuned(ItemStack stack, ConsoleTile tile) {
		setTardis(stack, tile.getWorld().getDimensionKey().getLocation());
		tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> setTardisName(stack, data.getTARDISName()));
		return stack;
	}

	@Override
	public int getAttunementTime() {
		return 200;
	}

	@Override
	public ResourceLocation getTardis(ItemStack stack) {
		IDiagnostic cap = stack.getCapability(Capabilities.DIAGNOSTIC).orElse(null);
		
		if(cap == null)
			return null;
		
		return cap.getTardis();
		
	}

	@Override
	public void setTardis(ItemStack stack, ResourceLocation world) {
		IDiagnostic diag = stack.getCapability(Capabilities.DIAGNOSTIC).orElse(null);
		if(diag != null)
			diag.setTardis(world);
	}
	
	@Override
	public String getTardisName(ItemStack stack) {
		if(stack.getTag() != null && stack.getTag().contains("tardis_name")) {
			return stack.getTag().getString("tardis_name");
		}
		return null;
	}

	@Override
	public void setTardisName(ItemStack stack, String name) {
		stack.getOrCreateTag().putString("tardis_name", name);
	}
}
