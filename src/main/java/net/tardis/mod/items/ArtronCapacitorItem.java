package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;

public class ArtronCapacitorItem extends Item {

    private float storage = 32F;
    private float rechargeModifier = 1F;
    
    protected final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.artron_capacitor.info"));
    protected final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.artron_capacitor.howto"));
    
    public ArtronCapacitorItem(float storage, float rechargeMod) {
        super(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE));
        this.storage = storage;
        this.rechargeModifier = rechargeMod;
    }

    public float getRechargeModifier() {
        return this.rechargeModifier;
    }

    public float getMaxStorage() {
        return this.storage;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
        tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
        if (Screen.hasShiftDown()) {
        	tooltip.clear();
        	tooltip.add(0, this.getDisplayName(stack));
        	tooltip.add(new TranslationTextComponent("tooltip.artron_capacitor.max_charge").appendSibling(new StringTextComponent(String.valueOf(this.storage)).mergeStyle(TextFormatting.LIGHT_PURPLE).appendSibling(TardisConstants.Suffix.ARTRON_UNITS)));
            tooltip.add(new TranslationTextComponent("tooltip.artron_capacitor.recharge_multiplier").appendSibling(new StringTextComponent(String.valueOf(this.rechargeModifier)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        }
        if (Screen.hasControlDown()) {
            tooltip.clear();
            tooltip.add(0, this.getDisplayName(stack));
            tooltip.add(descriptionTooltip);
            tooltip.add(descriptionTooltipTwo);
        }
    }

}
