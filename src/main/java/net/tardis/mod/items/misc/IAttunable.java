package net.tardis.mod.items.misc;

import net.minecraft.item.ItemStack;
import net.tardis.mod.tileentities.ConsoleTile;
/** Implement this class on an item class to allow that item to be attunable to a Tardis
 * <br> Only implement if you want the have custom logic on your item after attunement*/
public interface IAttunable {
    
	/** Defines behavior for what happens when the {@linkplain ItemStack} is attuned*/
	ItemStack onAttuned(ItemStack stack, ConsoleTile tile);

	/** Get the number of ticks that's needed to allow this object to be attuned*/
	int getAttunementTime();
}

