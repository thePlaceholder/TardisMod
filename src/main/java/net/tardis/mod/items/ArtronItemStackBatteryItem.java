package net.tardis.mod.items;

import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.artron.IArtronItemStackBattery;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.TooltipProviderItem;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Created by 50ap5ud5
 * on 24 Mar 2020 @ 9:29:34 am
 */

/** Used for providing charge to items
  *Also used to power machines
  **/
public class ArtronItemStackBatteryItem extends TooltipProviderItem implements IArtronItemStackBattery {

    private static final String CHARGE = "artron";
    private final float chargeRateMultiplier;
    private final float dischargeRateMultiplier;
    private final float maxChargeCapacity;
    private final boolean isCreative;
    
    protected final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.artron_battery.howto"));
    

    public ArtronItemStackBatteryItem(float chargeRateMultiplier, float dischargeRateMultiplier, float maxChargeCapacity, boolean isCreative) {
        super(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE));
        this.chargeRateMultiplier = chargeRateMultiplier;
        this.dischargeRateMultiplier = dischargeRateMultiplier;
        this.maxChargeCapacity = maxChargeCapacity;
        this.isCreative = isCreative;
        this.setShowTooltips(true);
        this.setHasStatisticsTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    //Returns how much was charged
    //Also writes the changes in data to nbt
    @Override
    public float charge(ItemStack stack, float amount, boolean simulate) {
        amount = this.getChargeRate(amount);
        float charge = stack.getOrCreateTag().getFloat(CHARGE);
        float maxCharge = this.getMaxCharge(stack);
        //If adding more will go over the max charge, return however much is needed to reach max.
        if (charge + amount >= maxCharge) {
            float chargeToAdd = maxCharge - charge;
            if (!simulate)
                this.writeCharge(stack, charge + chargeToAdd); //We need to write to nbt everytime we want to save data
            return chargeToAdd;
        } else {
        	if (!simulate)
                this.writeCharge(stack, charge + amount);
            return amount;
        }
    }


    @Override
    public float discharge(ItemStack stack, float amount, boolean simulate) {
        float current = stack.getOrCreateTag().getFloat(CHARGE);
        float takenAmount = this.getDischargeRate(amount);
        float updatedCharge = current - takenAmount;
        if (takenAmount <= current && updatedCharge > 0) {
        	if (!simulate)
               writeCharge(stack, updatedCharge);
            return takenAmount;
        }
        if (!simulate)
            writeCharge(stack, 0);
        return current;
    }


    @Override
    public float getMaxCharge(ItemStack stack) {
        return this.maxChargeCapacity;
    }

    @Override
    public float getCharge(ItemStack stack) {
        return readCharge(stack);
    }

    public void writeCharge(ItemStack stack, float charge) {
        stack.getOrCreateTag().putFloat(CHARGE, charge);
    }

    public float readCharge(ItemStack stack) {
        return stack.getOrCreateTag().getFloat(CHARGE);
    }

    public float getDischargeRate(float amount) {
        return amount * this.dischargeRateMultiplier;
    }

    public float getChargeRate(float amount) {
        return amount * this.chargeRateMultiplier;
    }


    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {

        //Fill battery in creative
        if (this.isCreative) {
            ItemStack stack = playerIn.getHeldItem(handIn);
            if (this.getCharge(stack) == 0) {
                this.charge(stack, Float.MAX_VALUE);
                return new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);
            }
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        BlockState state = context.getWorld().getBlockState(context.getPos());
        if(state.getBlock() == TBlocks.engine.get()){
            ConsoleTile console = TardisHelper.getConsoleInWorld(context.getWorld()).orElse(null);
            if(console != null){
                float room = console.getMaxArtron() - console.getArtron();
                float accepted = this.discharge(context.getItem(), room);
                console.setArtron(console.getArtron() + accepted);
                return ActionResultType.SUCCESS;
            }
        }
        return super.onItemUse(context);
    }


    @Override
	public void createStatisticTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
    	tooltip.add(new TranslationTextComponent("tooltip.artron_battery.charge").appendSibling(new StringTextComponent(String.valueOf(this.getCharge(stack))).mergeStyle(TextFormatting.LIGHT_PURPLE).appendSibling(TardisConstants.Suffix.ARTRON_UNITS)));
        tooltip.add(new TranslationTextComponent("tooltip.artron_battery.max_charge").appendSibling(new StringTextComponent(String.valueOf(this.getMaxCharge(stack))).mergeStyle(TextFormatting.LIGHT_PURPLE).appendSibling(TardisConstants.Suffix.ARTRON_UNITS)));
        tooltip.add(new TranslationTextComponent("tooltip.artron_battery.discharge_multiplier").appendSibling(new StringTextComponent(String.valueOf(this.dischargeRateMultiplier)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        tooltip.add(new TranslationTextComponent("tooltip.artron_battery.charge_multiplier").appendSibling(new StringTextComponent(String.valueOf(this.chargeRateMultiplier)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        if (this.isCreative && this.getCharge(stack) == 0)
            tooltip.add(new TranslationTextComponent("tooltip.artron_battery.creative_setup"));
	}

	@Override
	public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		tooltip.add(descriptionTooltip);
	}

    public boolean isCreative() {
        return this.isCreative;
    }

	@Override
	public float charge(ItemStack stack, float amount) {
		return charge(stack, amount, false);
	}

	@Override
	public float discharge(ItemStack stack, float amount) {
		return discharge(stack, amount, false);
	}


}
