package net.tardis.mod.network;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.mojang.serialization.Codec;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.tardis.mod.Tardis;
import net.tardis.mod.network.packets.ARSDeleteMessage;
import net.tardis.mod.network.packets.ARSPieceSyncMessage;
import net.tardis.mod.network.packets.ARSSpawnMessage;
import net.tardis.mod.network.packets.AttunementProgressMessage;
import net.tardis.mod.network.packets.BOTIChunkMessage;
import net.tardis.mod.network.packets.BOTIEntityMessage;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.network.packets.BOTITileMessage;
import net.tardis.mod.network.packets.BessieHornMessage;
import net.tardis.mod.network.packets.BrokenTardisParticleSpawn;
import net.tardis.mod.network.packets.ChangeExtAnimationMessage;
import net.tardis.mod.network.packets.ChangeExtVarMessage;
import net.tardis.mod.network.packets.ChangeHumMessage;
import net.tardis.mod.network.packets.ChangeInteriorMessage;
import net.tardis.mod.network.packets.CommunicatorMessage;
import net.tardis.mod.network.packets.CompanionActionMessage;
import net.tardis.mod.network.packets.CompleteMissionMessage;
import net.tardis.mod.network.packets.ConsoleChangeMessage;
import net.tardis.mod.network.packets.ConsoleRoomSyncMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleVariantMessage;
import net.tardis.mod.network.packets.DiagnosticMessage;
import net.tardis.mod.network.packets.EngineSliderSyncMessage;
import net.tardis.mod.network.packets.ExteriorChangeMessage;
import net.tardis.mod.network.packets.ExteriorDataMessage;
import net.tardis.mod.network.packets.FailEngineMessage;
import net.tardis.mod.network.packets.InteriorEffectsMessage;
import net.tardis.mod.network.packets.LaserDamageSyncMessage;
import net.tardis.mod.network.packets.LightUpdateMessage;
import net.tardis.mod.network.packets.MaterializationMessage;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.network.packets.MonitorRemoteMessage;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.network.packets.RequestTileDataMessage;
import net.tardis.mod.network.packets.SchematicSyncMessage;
import net.tardis.mod.network.packets.SendTardisDistressSignal;
import net.tardis.mod.network.packets.SetMissionObjectiveMessage;
import net.tardis.mod.network.packets.SnapMessage;
import net.tardis.mod.network.packets.SonicModeChangeMessage;
import net.tardis.mod.network.packets.SonicPartChangeMessage;
import net.tardis.mod.network.packets.StopHumMessage;
import net.tardis.mod.network.packets.SubsystemToggleMessage;
import net.tardis.mod.network.packets.SyncDimensionListMessage;
import net.tardis.mod.network.packets.SyncPlayerMessage;
import net.tardis.mod.network.packets.SyncSonicMessage;
import net.tardis.mod.network.packets.TardisLikeSyncMessage;
import net.tardis.mod.network.packets.TardisNameGuiMessage;
import net.tardis.mod.network.packets.TelepathicGetStructuresMessage;
import net.tardis.mod.network.packets.TelepathicMessage;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.network.packets.UpdateControlMessage;
import net.tardis.mod.network.packets.UpdateLaserGun;
import net.tardis.mod.network.packets.UpdateManualPageMessage;
import net.tardis.mod.network.packets.UpdateTransductionMessage;
import net.tardis.mod.network.packets.VMDistressMessage;
import net.tardis.mod.network.packets.VMFunctionMessage;
import net.tardis.mod.network.packets.VMGetWorldsMessage;
import net.tardis.mod.network.packets.VMTeleportMessage;
import net.tardis.mod.network.packets.WatchTimeUpdate;
import net.tardis.mod.network.packets.WaypointDeleteMessage;
import net.tardis.mod.network.packets.WaypointLoadMessage;
import net.tardis.mod.network.packets.WaypointOpenMessage;
import net.tardis.mod.network.packets.WaypointSaveMessage;
import net.tardis.mod.network.packets.WaypointScreenMessage;
import net.tardis.mod.network.packets.data.Requestors.BotiRequestor;
import net.tardis.mod.network.packets.data.Requestors.MaterializationRequestor;

public class Network {

	private static int ID = 0;
    private static final String PROTOCOL_VERSION = Integer.toString(1);
    private static final SimpleChannel NETWORK_CHANNEL = NetworkRegistry.newSimpleChannel(new ResourceLocation(Tardis.MODID, "main_channel"), () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals);

    
    public static void init() {
        NETWORK_CHANNEL.registerMessage(nextId(), UpdateControlMessage.class, UpdateControlMessage::encode, UpdateControlMessage::decode, UpdateControlMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ProtocolMessage.class, ProtocolMessage::encode, ProtocolMessage::decode, ProtocolMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), LightUpdateMessage.class, LightUpdateMessage::encode, LightUpdateMessage::decode, LightUpdateMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), VMTeleportMessage.class, VMTeleportMessage::encode, VMTeleportMessage::decode, VMTeleportMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ChangeInteriorMessage.class, ChangeInteriorMessage::encode, ChangeInteriorMessage::decode, ChangeInteriorMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ConsoleChangeMessage.class, ConsoleChangeMessage::encode, ConsoleChangeMessage::decode, ConsoleChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SyncSonicMessage.class, SyncSonicMessage::encode, SyncSonicMessage::decode, SyncSonicMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ARSSpawnMessage.class, ARSSpawnMessage::encode, ARSSpawnMessage::decode, ARSSpawnMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SonicPartChangeMessage.class, SonicPartChangeMessage::encode, SonicPartChangeMessage::decode, SonicPartChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), UpdateManualPageMessage.class, UpdateManualPageMessage::encode, UpdateManualPageMessage::decode, UpdateManualPageMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), QuantiscopeTabMessage.class, QuantiscopeTabMessage::encode, QuantiscopeTabMessage::decode, QuantiscopeTabMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), StopHumMessage.class, StopHumMessage::encode, StopHumMessage::decode, StopHumMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ChangeHumMessage.class, ChangeHumMessage::encode, ChangeHumMessage::decode, ChangeHumMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), BessieHornMessage.class, BessieHornMessage::encode, BessieHornMessage::decode, BessieHornMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ExteriorChangeMessage.class, ExteriorChangeMessage::encode, ExteriorChangeMessage::decode, ExteriorChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), WaypointSaveMessage.class, WaypointSaveMessage::encode, WaypointSaveMessage::decode, WaypointSaveMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), WaypointLoadMessage.class, WaypointLoadMessage::encode, WaypointLoadMessage::decode, WaypointLoadMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), WaypointDeleteMessage.class, WaypointDeleteMessage::encode, WaypointDeleteMessage::decode, WaypointDeleteMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ChangeExtAnimationMessage.class, ChangeExtAnimationMessage::encode, ChangeExtAnimationMessage::decode, ChangeExtAnimationMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), TelepathicMessage.class, TelepathicMessage::encode, TelepathicMessage::decode, TelepathicMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), UpdateARSTablet.class, UpdateARSTablet::encode, UpdateARSTablet::decode, UpdateARSTablet::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), CommunicatorMessage.class, CommunicatorMessage::encode, CommunicatorMessage::decode, CommunicatorMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), VMFunctionMessage.class, VMFunctionMessage::encode, VMFunctionMessage::decode, VMFunctionMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ChangeExtVarMessage.class, ChangeExtVarMessage::encode, ChangeExtVarMessage::decode, ChangeExtVarMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ConsoleVariantMessage.class, ConsoleVariantMessage::encode, ConsoleVariantMessage::decode, ConsoleVariantMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), VMDistressMessage.class, VMDistressMessage::encode, VMDistressMessage::decode, VMDistressMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ConsoleUpdateMessage.class, ConsoleUpdateMessage::encode, ConsoleUpdateMessage::decode, ConsoleUpdateMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), UpdateTransductionMessage.class, UpdateTransductionMessage::encode, UpdateTransductionMessage::decode, UpdateTransductionMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ARSDeleteMessage.class, ARSDeleteMessage::encode, ARSDeleteMessage::decode, ARSDeleteMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SnapMessage.class, SnapMessage::encode, SnapMessage::decode, SnapMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), MonitorRemoteMessage.class, MonitorRemoteMessage::encode, MonitorRemoteMessage::decode, MonitorRemoteMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), WaypointOpenMessage.class, WaypointOpenMessage::encode, WaypointOpenMessage::decode, WaypointOpenMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SendTardisDistressSignal.class, SendTardisDistressSignal::encode, SendTardisDistressSignal::decode, SendTardisDistressSignal::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), FailEngineMessage.class, FailEngineMessage::encode, FailEngineMessage::decode, FailEngineMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), CompleteMissionMessage.class, CompleteMissionMessage::encode, CompleteMissionMessage::decode, CompleteMissionMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SetMissionObjectiveMessage.class, SetMissionObjectiveMessage::encode, SetMissionObjectiveMessage::decode, SetMissionObjectiveMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), WaypointScreenMessage.class, WaypointScreenMessage::encode, WaypointScreenMessage::decode, WaypointScreenMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SubsystemToggleMessage.class, SubsystemToggleMessage::encode, SubsystemToggleMessage::decode, SubsystemToggleMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), BOTITileMessage.class, BOTITileMessage::encode, BOTITileMessage::decode, BOTITileMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SonicModeChangeMessage.class, SonicModeChangeMessage::encode, SonicModeChangeMessage::decode, SonicModeChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), MaterializationMessage.class, MaterializationMessage::encode, MaterializationMessage::decode, MaterializationMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ExteriorDataMessage.class, ExteriorDataMessage::encode, ExteriorDataMessage::decode, ExteriorDataMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), RequestTileDataMessage.class, RequestTileDataMessage::encode, RequestTileDataMessage::decode, RequestTileDataMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), CompanionActionMessage.class, CompanionActionMessage::encode, CompanionActionMessage::decode, CompanionActionMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), UpdateLaserGun.class, UpdateLaserGun::encode, UpdateLaserGun::decode, UpdateLaserGun::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), InteriorEffectsMessage.class, InteriorEffectsMessage::encode, InteriorEffectsMessage::decode, InteriorEffectsMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), BOTIChunkMessage.class, BOTIChunkMessage::encode,BOTIChunkMessage::decode, BOTIChunkMessage::handle);

        //To Client
        NETWORK_CHANNEL.registerMessage(nextId(), MissControlMessage.class, MissControlMessage::encode, MissControlMessage::decode, MissControlMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), BrokenTardisParticleSpawn.class, BrokenTardisParticleSpawn::encode, BrokenTardisParticleSpawn::decode, BrokenTardisParticleSpawn::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), BOTIMessage.class, BOTIMessage::encode, BOTIMessage::decode, BOTIMessage::handle);
//        NETWORK_CHANNEL.registerMessage(nextId(), AlembicRecipeSyncMessage.class, AlembicRecipeSyncMessage::encode, AlembicRecipeSyncMessage::decode, AlembicRecipeSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ConsoleRoomSyncMessage.class, ConsoleRoomSyncMessage::encode, ConsoleRoomSyncMessage::decode, ConsoleRoomSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), ARSPieceSyncMessage.class, ARSPieceSyncMessage::encode, ARSPieceSyncMessage::decode, ARSPieceSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SchematicSyncMessage.class, SchematicSyncMessage::encode, SchematicSyncMessage::decode, SchematicSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), TardisLikeSyncMessage.class, TardisLikeSyncMessage::encode, TardisLikeSyncMessage::decode, TardisLikeSyncMessage::handle);
//        NETWORK_CHANNEL.registerMessage(nextId(), WeldRecipeSyncMessage.class, WeldRecipeSyncMessage::encode, WeldRecipeSyncMessage::decode, WeldRecipeSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), DiagnosticMessage.class, DiagnosticMessage::encode, DiagnosticMessage::decode, DiagnosticMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), BOTIEntityMessage.class, BOTIEntityMessage::encode, BOTIEntityMessage::decode, BOTIEntityMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), MissionUpdateMessage.class, MissionUpdateMessage::encode, MissionUpdateMessage::decode, MissionUpdateMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), WatchTimeUpdate.class, WatchTimeUpdate::encode, WatchTimeUpdate::decode, WatchTimeUpdate::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), SyncPlayerMessage.class, SyncPlayerMessage::encode, SyncPlayerMessage::decode, SyncPlayerMessage::handle);
        registerCodecPacket(nextId(), NETWORK_CHANNEL, SyncDimensionListMessage.CODEC, SyncDimensionListMessage.INVALID);
        NETWORK_CHANNEL.registerMessage(nextId(), AttunementProgressMessage.class, AttunementProgressMessage::encode, AttunementProgressMessage::decode, AttunementProgressMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), TardisNameGuiMessage.class, TardisNameGuiMessage::encode, TardisNameGuiMessage::decode, TardisNameGuiMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), TelepathicGetStructuresMessage.class, TelepathicGetStructuresMessage::encode, TelepathicGetStructuresMessage::decode, TelepathicGetStructuresMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), EngineSliderSyncMessage.class, EngineSliderSyncMessage::encode, EngineSliderSyncMessage::decode, EngineSliderSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), VMGetWorldsMessage.class, VMGetWorldsMessage::encode, VMGetWorldsMessage::decode, VMGetWorldsMessage::handle);
        NETWORK_CHANNEL.registerMessage(nextId(), LaserDamageSyncMessage.class, LaserDamageSyncMessage::encode, LaserDamageSyncMessage::decode, LaserDamageSyncMessage::handle);
        registerReqestors();
    }
    
    public static <PACKET extends Consumer<NetworkEvent.Context>> void registerCodecPacket(int id, SimpleChannel channel, Codec<PACKET> codec, PACKET defaultPacket){

		final BiConsumer<PACKET,PacketBuffer> encoder = (packet,buffer) -> codec.encodeStart(NBTDynamicOps.INSTANCE, packet)
			.result()
			.ifPresent(nbt -> buffer.writeCompoundTag((CompoundNBT)nbt));
		final Function<PacketBuffer,PACKET> decoder = buffer -> codec.parse(NBTDynamicOps.INSTANCE, buffer.readCompoundTag())
			.result()
			.orElse(defaultPacket);	// we should return a packet without throwing an error here if we don't want logspam
		final BiConsumer<PACKET,Supplier<Context>> handler = (packet,context) -> {
			packet.accept(context.get());
			context.get().setPacketHandled(true);
		};
		
		final Class<PACKET> packetClass = (Class<PACKET>) (defaultPacket.getClass());
		
		channel.registerMessage(id, packetClass, encoder, decoder, handler);
	}
    
    public static SimpleChannel getNetworkChannel() {
    	return NETWORK_CHANNEL;
    }

    /**
     * Sends a packet to the server.<br>
     * Must be called Client side.
     */
    public static void sendToServer(Object msg) {
        NETWORK_CHANNEL.sendToServer(msg);
    }

    /**
     * Send a packet to a specific player.<br>
     * Must be called Server side.
     */
    public static void sendTo(Object msg, ServerPlayerEntity player) {
        if (!(player instanceof FakePlayer)) {
            NETWORK_CHANNEL.send(PacketDistributor.PLAYER.with(() -> player), msg);
        }
    }

    public static void sendPacketToAll(Object packet) {
    	NETWORK_CHANNEL.send(PacketDistributor.ALL.noArg(), packet);
    }

	public static SUpdateTileEntityPacket createTEUpdatePacket(TileEntity tile) {
		return new SUpdateTileEntityPacket(tile.getPos(), -1, tile.getUpdateTag());
	}

	public static void sendToAllAround(Object mes, RegistryKey<World> dim, BlockPos pos, int radius) {
        NETWORK_CHANNEL.send(PacketDistributor.NEAR.with(() -> new PacketDistributor.TargetPoint(pos.getX(), pos.getY(), pos.getZ(), radius, dim)), mes);
	}

	public static void sendToAllInWorld(Object mes, ServerWorld world) {
        NETWORK_CHANNEL.send(PacketDistributor.DIMENSION.with(world::getDimensionKey), mes);
	}
	
	public static void sendToTrackingTE(Object mes, TileEntity te) {
		if(te != null && !te.getWorld().isRemote)
			NETWORK_CHANNEL.send(PacketDistributor.TRACKING_CHUNK.with(() -> te.getWorld().getChunkAt(te.getPos())), mes);
	}

	public static int nextId(){
        return ++ID;
    }

    public static BotiRequestor BOTI_REQUESTOR;
    public static MaterializationRequestor MATERIALIZARION_REQUESTOR;

    public static void registerReqestors(){
        BOTI_REQUESTOR = RequestTileDataMessage.registerRequestor(new BotiRequestor());
        MATERIALIZARION_REQUESTOR = RequestTileDataMessage.registerRequestor(new MaterializationRequestor());
    }

}
