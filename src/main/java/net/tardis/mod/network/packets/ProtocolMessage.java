package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.ProtocolRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class ProtocolMessage {
	
	public ResourceLocation protocol;
	
	public ProtocolMessage(ResourceLocation protocol) {
		this.protocol = protocol;
	}
	
	public static ProtocolMessage decode(PacketBuffer buf) {
		return new ProtocolMessage(buf.readResourceLocation());
	}
	
	public static void encode(ProtocolMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.protocol);
	}

	public static void handle(ProtocolMessage mes, Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			
			ServerWorld world = ctx.get().getSender().getServerWorld();
			TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				Protocol prot = ProtocolRegistry.PROTOCOL_REGISTRY.get().getValue(mes.protocol);
				if(prot != null) {
					prot.call(world, ctx.get().getSender(), (ConsoleTile)te);
				}
			}
		});
	}
}
