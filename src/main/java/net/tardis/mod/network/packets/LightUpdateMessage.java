package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class LightUpdateMessage {
	
	public double light = 0;
	
	public LightUpdateMessage(double light) {
		this.light = light;
	}
	
	public static void encode(LightUpdateMessage mes, PacketBuffer buffer) {
		buffer.writeDouble(mes.light);
	}
	
	public static LightUpdateMessage decode(PacketBuffer buffer) {
		return new LightUpdateMessage(buffer.readDouble());
	}
	
	public static void handle(LightUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> {
			ServerWorld world = cont.get().getSender().getServerWorld();
			TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				((ConsoleTile)te).getInteriorManager().setLight((int)(mes.light * 15.0));
			}
		});
		
		cont.get().setPacketHandled(true);
	}

}
