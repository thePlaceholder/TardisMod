package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.TelepathicUtils.Search;
import net.tardis.mod.misc.TelepathicUtils.SearchType;

public class TelepathicGetStructuresMessage {

	private static final int STRING_SIZE = 32767;
	private List<Search> names = new ArrayList<>();
	
	public TelepathicGetStructuresMessage(List<Search> names) {
		this.names.clear();
		this.names.addAll(names);
	}
	
	public static TelepathicGetStructuresMessage create(MinecraftServer server, World world) {
		List<Search> list = new ArrayList<>();
		server.getDynamicRegistries().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getEntries().stream().forEach(entry -> {
			list.add(new Search(new StringTextComponent(WorldHelper.formatStructureKey(entry.getKey())), entry.getKey().getLocation().toString(), SearchType.STRUCTURE));
		});
		return new TelepathicGetStructuresMessage(list);
	}
	
	public static void encode(TelepathicGetStructuresMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.names.size());
		for(Search e : mes.names) {
			buf.writeString(e.key, STRING_SIZE);
			buf.writeInt(e.type.ordinal());
		}
	}
	
	public static TelepathicGetStructuresMessage decode(PacketBuffer buf) {
		int size = buf.readInt();
		List<Search> names = new ArrayList<>();
	    for(int i = 0; i < size; ++i) {
	    	String key = buf.readString(STRING_SIZE);
			SearchType type = SearchType.values()[buf.readInt()];
		    names.add(new Search(new StringTextComponent(WorldHelper.formatStructureKey(key)), key, type));
        }
		
		return new TelepathicGetStructuresMessage(names);
	}
	
	public static void handle(TelepathicGetStructuresMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> ClientPacketHandler.handleTelepathicStructureNames(mes));
		context.get().setPacketHandled(true);
	}
	
	public List<Search> getSearchNames(){
		return names;
	}
}
